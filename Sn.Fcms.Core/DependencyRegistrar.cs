﻿using Autofac;
using Sn.Fcms.Infrastructure;

namespace Sn.Fcms.Core
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            builder.RegisterType<UserService>().As<IUserService>();


            builder.RegisterType<ListDataService>().As<IListDataService>();
            builder.RegisterType<FinanceListService>().As<IFinanceDataService>();

            #region 04

            //builder.RegisterType<FormDataService>()
            //   .As<IFormDataService>();

            //builder.RegisterType<DataConfigService>()
            //   .As<IDataConfigService>();

            //builder.RegisterType<SingleDataService>()
            //   .As<ISingleDataService>();

            //builder.RegisterType<SingleDataResolver>()
            //   .As<ISingleDataResolver>();

            //builder.RegisterType<DhbDataService>()
            //  .As<IDhbDataService>();

            //builder.RegisterType<ArAgeListDateResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ArAgingSheetId);

            //builder.RegisterType<OtherArAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OtherArAgingSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ArPrReclassifySheetId);

            //builder.RegisterType<OarAccountListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OarAccountSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.AccountBalanceSheetId);

            //builder.RegisterType<PrAccountAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.PrAccountAgeSheetId);

            //builder.RegisterType<ApAccountAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ApAccountAgeSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ArPr2rdReclassifySheetId);

            //builder.RegisterType<Ar2rdReclassifyAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.Ar2rdReclassifyAgeSheetId);

            //builder.RegisterType<Pr2rdReclassifyAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.Pr2rdReclassifyAgeSheetId);

            //builder.RegisterType<OarOapReclassifyListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OarOapReclassifySheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.TimelessArAccountAgeSheetId);

            //builder.RegisterType<PpAccountAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.PpAccountAgeSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ApPpReclassifySheetId);

            //builder.RegisterType<OapAccountListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OapAccountSheetId);

            //builder.RegisterType<OapAccountAgeListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OapAccountAgeSheetId);

            //builder.RegisterType<WorkInProcessListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.WorkInProcessSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ProjectCostSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.DiffCheckSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OtherProcessAssetDetailSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.ProjectMaterialSheetId);

            //builder.RegisterType<AssetDecreaseListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.AssetDecreaseSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.OtherProcessAssetSummarySheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //   .Keyed<IListDataResolver>(ListConst.AssetDeprRelationSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //  .Keyed<IListDataResolver>(ListConst.NonBusinessIncomeSheetId);

            //builder.RegisterType<DefaultListDataResolver>()
            //  .Keyed<IListDataResolver>(ListConst.NonBusinessExpenseSheetId);

            //builder.RegisterType<InternalCurrentAccountResolver>()
            //  .Keyed<ISingleListDataResolver>(ListConst.SInternalCurrentAccount);

            //builder.RegisterType<InternalCurrentAccountResolver>()
            //  .Keyed<ISingleListDataResolver>(ListConst.SharedCostNotesSheetId);

            //builder.RegisterType<AdjustingEntryResolver>()
            //  .Keyed<ISingleListDataResolver>(ListConst.SAdjustingEntriesSheetId);

            //builder.RegisterType<CombinedAdjustEntryResolver>()
            //  .Keyed<ISingleListDataResolver>(ListConst.SCombinedAdjustingEntriesSheetId);

            //builder.RegisterType<InternalCurrentAccountResolver>()
            // .Keyed<ISingleListDataResolver>(ListConst.AccountBalanceSheetId);

            //builder.RegisterType<CombinedAdjustEntryResolver>()
            // .Keyed<ISingleListDataResolver>(ListConst.SCashFlowAdjustingEntriesSheetId);



            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FManageFee);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FSaleFee);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FIntanAssetAmortize);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FDevelopPayChangeDetail);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FBankBalanceChange);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FBankBalanceSummary);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FBankBalanceSheet);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FArBillStandBook);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FArBillDetail);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FApBillStandBook);

            //builder.RegisterType<DefaultListDataResolver>()
            //.Keyed<IListDataResolver>(ListConst.FApBillDetail);

            #endregion
        }

        public int Order => 25;
    }
}