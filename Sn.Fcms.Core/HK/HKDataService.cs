﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public class HKDataService : IHKDataService
    {
        public Task<QueryListDataResponse<T>> Query<T>(QueryParam request)
        {
            var resource = "cfss-server/reportOrderQuery.htm";
            var baseUrl = ConfigConst.GetBaseUrl();

            //return GetMockData<T>(request);

            return RestfulApiClient.ExcutePostTask<QueryListDataResponse<T>>(baseUrl, resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error("user:{0} RefreshListData list:{1} error, ReponseStatus:{2}, HttpStatus:{3}",
                                t.Result.ErrorException, request.UserId, request.ReportId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new QueryListDataResponse<T>()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} RefreshListData {1} success, result count:{2}",
                            request.UserId, request.ReportId, t.Result.Data.Data == null ? 0 : t.Result.Data.Data.Count());

                        return t.Result.Data;
                    }

                    return null;
                });
        }

        #region mock data                 

        private async Task<QueryListDataResponse<T>> GetMockData<T>(QueryParam request)
        {
            return await Task.Run(() =>
            {
                QueryListDataResponse<T> response = new QueryListDataResponse<T>();

                List<BillInfo> data = new List<BillInfo>();    

                // 1001,7611                
                data.AddRange(GetData("1001", "7611", "1001", "现金", "58", "ZOR", 10));
                data.AddRange(GetData("1001", "7611", "1001", "现金", "58", "ZC", 10));
                data.AddRange(GetData("1001", "7611", "1001", "现金", "60", "ZOR", 10));
                data.AddRange(GetData("1001", "7611", "1001", "现金", "60", "ZC", 10));

                data.AddRange(GetData("1001", "7611", "6090", "南京市民卡", "58", "ZOR", 10));
                data.AddRange(GetData("1001", "7611", "6090", "南京市民卡", "58", "ZC", 10));
                data.AddRange(GetData("1001", "7611", "6903", "易付宝促销折扣", "60", "ZOR", 10));
                data.AddRange(GetData("1001", "7611", "6903", "易付宝促销折扣", "60", "ZC", 10));
                    
                data.AddRange(GetData("1001", "7612", "1001", "现金", "58", "ZOR", 10));
                data.AddRange(GetData("1001", "7612", "1001", "现金", "58", "ZC", 10));
                data.AddRange(GetData("1001", "7612", "1001", "现金", "60", "ZOR", 10));
                data.AddRange(GetData("1001", "7612", "1001", "现金", "60", "ZC", 10));

                data.AddRange(GetData("1001", "7612", "6090", "南京市民卡", "58", "ZOR", 10));
                data.AddRange(GetData("1001", "7612", "6090", "南京市民卡", "58", "ZC", 10));
                data.AddRange(GetData("1001", "7612", "6903", "易付宝促销折扣", "60", "ZOR", 10));
                data.AddRange(GetData("1001", "7612", "6903", "易付宝促销折扣", "60", "ZC", 10));

                // 1002,7611                
                //data.AddRange(GetData("1002", "7611", "1001", "现金", "58", "ZOR", 10));
                //data.AddRange(GetData("1002", "7611", "1001", "现金", "58", "ZC", 10));
                //data.AddRange(GetData("1002", "7611", "1001", "现金", "60", "ZOR", 10));
                //data.AddRange(GetData("1002", "7611", "1001", "现金", "60", "ZC", 10));

                //data.AddRange(GetData("1002", "7611", "6090", "南京市民卡", "58", "ZOR", 10));
                //data.AddRange(GetData("1002", "7611", "6090", "南京市民卡", "58", "ZC", 10));
                //data.AddRange(GetData("1002", "7611", "6903", "易付宝促销折扣", "60", "ZOR", 10));
                //data.AddRange(GetData("1002", "7611", "6903", "易付宝促销折扣", "60", "ZC", 10));

                //data.AddRange(GetData("1002", "7612", "1001", "现金", "58", "ZOR", 10));
                //data.AddRange(GetData("1002", "7612", "1001", "现金", "58", "ZC", 10));
                //data.AddRange(GetData("1002", "7612", "1001", "现金", "60", "ZOR", 10));
                //data.AddRange(GetData("1002", "7612", "1001", "现金", "60", "ZC", 10));

                //data.AddRange(GetData("1002", "7612", "6090", "南京市民卡", "58", "ZOR", 10));
                //data.AddRange(GetData("1002", "7612", "6090", "南京市民卡", "58", "ZC", 10));
                //data.AddRange(GetData("1002", "7612", "6903", "易付宝促销折扣", "60", "ZOR", 10));
                //data.AddRange(GetData("1002", "7612", "6903", "易付宝促销折扣", "60", "ZC", 10));


                response.Data = data as IEnumerable<T>;
                response.Amount = 2001;
                response.MsgCode = "S0000";
                response.MsgDesc = "操作成功";

                return response;
            });
        }

        private List<BillInfo> GetData(string payOrg,string payStore,string payCode,string payName,string dealType,string sapOrderType,int len)
        {
            List<BillInfo> data = new List<BillInfo>();
            Random rand = new Random();
            string day = DateTime.Now.ToString("yyyyMMdd");

            for (int i = 0; i < len; i++)
            {
                BillInfo info = new BillInfo();
                info.BillType = i % 2 == 0 ? 1 : -1;
                info.PayAmount = 100; //rand.Next(1000, 5000);
                info.PayOrg = payOrg;
                info.PayStore = payStore;
                info.PayCode = payCode;
                info.PayName = payName;
                info.DealType = dealType;
                info.DealTypeFlag = dealType;
                info.SapOrderType = sapOrderType;
                info.EffectiveTime = day;
                data.Add(info);
            }

            return data;
        }

        #endregion
    }
}
