﻿using Sn.Fcms.Infrastructure.Entity;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public interface IListDataService
    {
        Task<QueryListDataResponse<T>> Query<T>(QueryListDataRequest request);
    }
}
