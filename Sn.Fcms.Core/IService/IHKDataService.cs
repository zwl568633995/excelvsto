﻿using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public interface IHKDataService
    {
        Task<QueryListDataResponse<T>> Query<T>(QueryParam request);
    }
}
