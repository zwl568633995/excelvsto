﻿using Sn.Fcms.Infrastructure.Entity;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public interface IFinanceDataService
    {
        Task<QueryListDataResponse<T>> Query<T>(QueryListDataRequest request);
    }
}
