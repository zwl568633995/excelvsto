﻿using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public interface IUserService
    {
        //Task<ClientUpgradeResponse> CheckClient(ClientUpgradeRequest request);

        Task<AuthUserResponse> AuthUser(AuthUserRequest request);

        Task<DataResponse<Report>> QueryReports(AuthReportRequest request);

        Task<DataResponse<Organization>> QueryOrganizations(BaseRequest request);
    }
}
