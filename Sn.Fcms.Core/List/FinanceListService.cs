﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public class FinanceListService: IFinanceDataService
    {
        public Task<QueryListDataResponse<T>> Query<T>(QueryListDataRequest request)
        {
            var resource = "cfss-server/reportDataQuery.htm";

            return RestfulApiClient.ExcutePostTask<QueryListDataResponse<T>>(ConfigConst.ServerUrl, resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error("user:{0} RefreshListData list:{1} error, ReponseStatus:{2}, HttpStatus:{3}",
                                t.Result.ErrorException, request.UserId, request.ReportId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new QueryListDataResponse<T>()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} RefreshListData {1} success, result count:{2}",
                            request.UserId, request.ReportId, t.Result.Data.Data == null ? 0 : t.Result.Data.Data.Count());

                        return t.Result.Data;
                    }

                    return null;
                });
        }
    }
}
