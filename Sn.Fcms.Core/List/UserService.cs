﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Core
{
    public class UserService
    {
        #region 4 fcms 

        public Task<AuthUserResponse> AuthUser(AuthUserRequest request)
        {
            var resource = "cfss-server/clientLogin.htm";

            return RestfulApiClient.ExcutePostTask<AuthUserResponse>(ConfigConst.ServerUrl,resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error("user:{0} logon error, ReponseStatus:{1}, HttpStatus{2}:",
                                t.Result.ErrorException, request.UserId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new AuthUserResponse()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} log on success", request.UserId);
                        return t.Result.Data;
                    }

                    return null;
                });
        }

        public Task<DataResponse<Report>> QueryReports(AuthReportRequest request)
        {
            var resource = "cfss-server/reportListReceive.htm";          

            return RestfulApiClient.ExcutePostTask<DataResponse<Report>>(ConfigConst.ServerUrl,resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error(
                                "user:{0} query authoried reports error, ReponseStatus:{1}, HttpStatus{2}:",
                                t.Result.ErrorException, request.UserId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new DataResponse<Report>()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} query authoried reports success", request.UserId);

                        return t.Result.Data;
                    }

                    return null;
                });
        }

        public Task<DataResponse<Organization>> QueryOrganizations(BaseRequest request)
        {
            var resource = "cfss-server/orgListReceive.htm";           

            return RestfulApiClient.ExcutePostTask<DataResponse<Organization>>(ConfigConst.ServerUrl,resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error(
                                "user:{0} query authoried organs error, ReponseStatus:{1}, HttpStatus{2}:",
                                t.Result.ErrorException, request.UserId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new DataResponse<Organization>()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} query authoried organs success", request.UserId);

                        return t.Result.Data;
                    }

                    return null;
                });
        }

        #endregion

        #region 4 hk 

        public Task<AuthUserResponse> AuthHkUser(AuthUserRequest request)
        {
            var resource = "cfss-server/clientLogin.htm";
            var baseUrl = ConfigConst.GetBaseUrl();

            return RestfulApiClient.ExcutePostTask<AuthUserResponse>(baseUrl, resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccessStatusCode())
                        {
                            NLogger.Error.Error("user:{0} logon error, ReponseStatus:{1}, HttpStatus{2}:",
                                t.Result.ErrorException, request.UserId, t.Result.ResponseStatus,
                                t.Result.StatusDescription);

                            return new AuthUserResponse()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("user:{0} log on success", request.UserId);
                        return t.Result.Data;
                    }

                    return null;
                });
        }

        #endregion
    }
}
