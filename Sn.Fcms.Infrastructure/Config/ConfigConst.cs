﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure
{
    public class ConfigConst
    {   
        public static string ServerUrl = ConfigurationManager.AppSettings["serverUrl"];
        public static int Count = int.Parse(ConfigurationManager.AppSettings["PageCount"]);
        private static string env = ConfigurationManager.AppSettings["env"];

        private static Dictionary<string, string> router = null;

        public static void InitEnv()
        {
            router = new Dictionary<string, string>();
            router.Add("sit", "http://cfsssit.cnsuning.com");
            router.Add("pre", "http://cfsspre.cnsuning.com");
            router.Add("prd", "http://cfss.cnsuning.com");
            router.Add("dev", "http://10.24.5.159:8080");
        }

        public static string GetBaseUrl()
        {            
            return router[env];
        }
    }
}
