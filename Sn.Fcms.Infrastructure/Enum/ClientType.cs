﻿
namespace Sn.Fcms.Infrastructure
{
    public enum ClientType
    {        
        ManagementDocument = 10,
        DhbDocument = 12,
        FcmsDocument = 15,
        FinanceListDocument = 20,
        HKOrdersDocument = 21,
        SingleCompanyDocument = 30,
        TemData = 40,
        XhbDocument = 60,
        AppListDocument = 70,
        AppSingleDocument =80,
        AppXhbDocument = 90,
        AppDhbDocument = 13,
        NonSap=81
    }

    public enum ProductType
    {
        List = 1,
        SingleReport = 2
    }

}