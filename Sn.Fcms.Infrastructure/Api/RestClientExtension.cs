﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace Sn.Fcms.Infrastructure
{
    public static class RestClientExtension
    {
        public static bool IsSuccessStatusCode(this IRestResponse response)
        {
            return response.ResponseStatus == ResponseStatus.Completed && (int)response.StatusCode / 100 == 2;
        }

        public static void UseJsonDotNetSerializerWrapper(this IRestRequest request)
        {
            request.RequestFormat = DataFormat.Json;
            request.JsonSerializer = new JsonDotNetSerializerWrapper();
        }
        public static void UseJsonDotNetDeserializerWrapper(this IRestClient client)
        {
            client.RemoveHandler("application/json");
            client.AddHandler("application/json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/json");
            client.AddHandler("text/json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/x-json");
            client.AddHandler("text/x-json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/javascript");
            client.AddHandler("text/javascript", new JsonDotNetDeserializerWrapper());
        }

        internal class JsonDotNetSerializerWrapper : ISerializer
        {
            public JsonDotNetSerializerWrapper()
            {
                this.ContentType = "application/json";
            }

            public string Serialize(object obj)
            {
                return JsonConvert.SerializeObject(obj);
            }

            public string DateFormat { get; set; }
            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string ContentType { get; set; }
        }

        internal class JsonDotNetDeserializerWrapper : IDeserializer
        {
            public T Deserialize<T>(IRestResponse response)
            {
                JsonSerializerSettings setting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                return JsonConvert.DeserializeObject<T>(response.Content, setting);
            }
            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string DateFormat { get; set; }
        } 
    }
}