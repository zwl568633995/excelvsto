﻿using System.Threading.Tasks;
using RestSharp;

namespace Sn.Fcms.Infrastructure
{
    public class RestfulApiClient
    {      
        public static IRestResponse<T> ExcuteAsPost<T>
            (string serverUrl, string resource, object requestBody) where T : new()
        {
            RestClient client = new RestClient(serverUrl);
            client.UseJsonDotNetDeserializerWrapper();

            var restRequest = new RestRequest(resource);
            restRequest.UseJsonDotNetSerializerWrapper();
            restRequest.Method = Method.POST;

            restRequest.AddBody(requestBody);

            return client.Execute<T>(restRequest);
        }

        public static Task<IRestResponse<T>> ExcutePostTask<T>(string serverUrl, string resource, object requestBody)
        {
            RestClient client = new RestClient(serverUrl);
            client.UseJsonDotNetDeserializerWrapper();

            var restRequest = new RestRequest(resource);
            restRequest.UseJsonDotNetSerializerWrapper();

            restRequest.AddBody(requestBody);

            Task<IRestResponse<T>> iResult = client.ExecutePostTaskAsync<T>(restRequest);
            string strSaveRquest = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);
            string strSaveResponse = Newtonsoft.Json.JsonConvert.SerializeObject(iResult.Result.Data);           
            return iResult;
        }

        public static Task<IRestResponse> ExcutePostTask(string serverUrl, string resource, object requestBody)
        {
            RestClient client = new RestClient(serverUrl);

            var restRequest = new RestRequest(resource);
            restRequest.UseJsonDotNetSerializerWrapper();

            restRequest.AddBody(requestBody);

            return client.ExecutePostTaskAsync(restRequest);
        }

        public static Task<IRestResponse<T>> ExcuteGetTask<T>(string serverUrl, string resource, object requestBody)
        {
            RestClient client = new RestClient(serverUrl);
            client.UseJsonDotNetDeserializerWrapper();

            var restRequest = new RestRequest(resource);

            if (requestBody != null)
            {
                restRequest.UseJsonDotNetSerializerWrapper();
                restRequest.AddBody(requestBody);
            }

            return client.ExecuteGetTaskAsync<T>(restRequest);
        }

        public static Task<IRestResponse<T>> ExcuteGetTask<T>(string serverUrl)
        {
            return ExcuteGetTask<T>(serverUrl, string.Empty, null);
        }

        public static Task<IRestResponse> ExcuteGetTask(string serverUrl)
        {
            RestClient client = new RestClient(serverUrl);
            var restRequest = new RestRequest();

            return client.ExecuteGetTaskAsync(restRequest);
        }

    }
}