﻿using System;
using System.Runtime.Caching;

namespace Sn.Fcms.Infrastructure
{
    public class MemoryCacheManager : ICacheManager
    {
        private readonly MemoryCache _cache = MemoryCache.Default;

        public bool AddItem(string key, object value, TimeSpan? activePeriod = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                AbsoluteExpiration =
                    !activePeriod.HasValue ? DateTimeOffset.MaxValue : DateTimeOffset.Now.Add(activePeriod.Value)
            };

            return _cache.Add(key, value, policy);
        }

        public void SetItem(string key, object value, TimeSpan? activePeriod = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                AbsoluteExpiration =
                    !activePeriod.HasValue ? DateTimeOffset.MaxValue : DateTimeOffset.Now.Add(activePeriod.Value)
            };

            _cache.Set(key, value, policy);
        }

        public T GetItem<T>(string key)
        {
            var item = _cache.Get(key);

            if (!(item is T))
                return default(T);

            return (T)_cache.Get(key);
        }

        public object RemoveItem(string key)
        {
            return _cache.Remove(key);
        }
    }
}