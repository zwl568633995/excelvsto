﻿using System;

namespace Sn.Fcms.Infrastructure
{
    public interface ICacheManager
    {
        bool AddItem(string key, object value, TimeSpan? activePeriod = null);

        void SetItem(string key, object value, TimeSpan? activePeriod = null);

        T GetItem<T>(string key);

        object RemoveItem(string key);
    }
}