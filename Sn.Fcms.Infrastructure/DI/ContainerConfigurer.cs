﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace Sn.Fcms.Infrastructure
{
    public class ContainerConfigurer
    {
        public static ContainerManager CurrentManager
        {
            get
            {
                if (Singleton<ContainerManager>.Instance == null)
                {
                    Singleton<ContainerManager>.Instance = InitialContainerManager();
                }

                return Singleton<ContainerManager>.Instance;
            }
        }

        public static ContainerManager InitialContainerManager()
        {
            if (Singleton<ContainerManager>.Instance == null)
            {
                var builder = new ContainerBuilder();
                var containerManager = new ContainerManager(builder.Build());

                Configure(containerManager);

                return containerManager;
            }

            return Singleton<ContainerManager>.Instance;
        }

        private static void Configure(ContainerManager containerManager)
        {
            //type finder
            containerManager.AddComponent<ITypeFinder, AppDomainTypeFinder>("typeFinder");

            //register dependencies provided by other assemblies
            var typeFinder = containerManager.Resolve<ITypeFinder>();

            containerManager.UpdateContainer(x =>
            {
                var drTypes = typeFinder.FindClassesOfType<IDependencyRegistrar>();
                var drInstances = new List<IDependencyRegistrar>();
                foreach (var drType in drTypes)
                    drInstances.Add((IDependencyRegistrar)Activator.CreateInstance(drType));
                //sort
                drInstances = drInstances.AsQueryable().OrderBy(t => t.Order).ToList();
                foreach (var dependencyRegistrar in drInstances)
                    dependencyRegistrar.Register(x, typeFinder);
            });
        }
 

    }
}