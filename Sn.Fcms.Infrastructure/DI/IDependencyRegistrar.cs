﻿using Autofac;

namespace Sn.Fcms.Infrastructure
{
    public interface IDependencyRegistrar
    {
        void Register(ContainerBuilder builder, ITypeFinder typeFinder);

        int Order { get; } 
    }
}