﻿using Autofac;

namespace Sn.Fcms.Infrastructure
{
    public class DependencyRegistrar:IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            builder.RegisterType<MemoryCacheManager>()
                .As<ICacheManager>()
                .SingleInstance();
        }

        public int Order => 10;
    }
}