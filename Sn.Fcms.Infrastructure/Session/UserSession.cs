﻿using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure
{
    public class UserSession
    {
        public static bool Auth { get; set; }

        public static string UserId { get; set; }

        public static string UserName { get; set; }

        public static ClientType ClientId { get; set; }

        public static string TokenId { get; set; }

        public static string OrgCode { get; set; }

        public static string Month { get; set; }

        public static string DataVersion { get; set; }

        public static List<string> ProcedureAuth { get; set; }

        public static List<Organization> Organizations { get; set; }

        public static List<Report> Reports { get; set; }

        public static Dictionary<string,SheetInfo> Map { get; set; }
    }
}
