﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure
{
    public static class Common
    {
        public static DataTable ConvertToDatatable<T>(List<T>data)
        {
            if (data == null)
            {
                return null;
            }
            DataTable dataTable = new DataTable();
            using (var reader = ObjectReader.Create(data))
            {
                dataTable.Load(reader);
            }

            return dataTable;
        }

        public static IEnumerable<T> ConvertToList<T>(this DataTable table,
           string categoryCode = null, string categoryValue = null) where T : class, new()
        {
            List<T> result = new List<T>();

            foreach (DataRow row in table.Rows)
            {
                T entity = new T();
                var accessor = ObjectAccessor.Create(entity, false);

                foreach (DataColumn column in table.Columns)
                {
                    if (row[column] == null || row[column] == DBNull.Value)
                        continue;

                    if (accessor[column.ColumnName] is decimal)
                        accessor[column.ColumnName] = Convert.ToDecimal(row[column]);
                    else
                        accessor[column.ColumnName] = row[column];
                }

                if (!string.IsNullOrEmpty(categoryCode))
                    accessor[categoryCode] = categoryValue;

                result.Add(entity);
            }

            return result;
        }

        public static IEnumerable<T> GetUpdatedDatas<T>(System.Data.DataTable dt) where T : class, new()
        {
            var upTable = dt?.GetChanges(DataRowState.Modified);

            return upTable?.ConvertToList<T>() ?? new List<T>();
        }
    }
}
