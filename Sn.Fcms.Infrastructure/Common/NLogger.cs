﻿using NLog;

namespace Sn.Fcms.Infrastructure
{
    public class NLogger
    {
        private static Logger _error = null;
        private static Logger _info = null;
        private static Logger _interface = null;

        public static Logger Error
        {
            get
            {
                if (_error == null)
                    _error = LogManager.GetLogger("error");

                return _error;
            }
        }

        public static Logger Info
        {
            get
            {
                if (_info == null)
                    _info = LogManager.GetLogger("info");

                return _info;
            }
        }        
    }
}
