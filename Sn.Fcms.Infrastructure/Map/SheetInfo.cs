﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure
{
    public class SheetInfo
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public List<RangeInfo> Ranges { get; set; }

        public List<TableRange> Tables { get; set; }
    }

    public class TableRange
    {
        public string Name { get; set; }

        public string Address { get; set; }

        /// <summary>
        /// 数据筛选标识
        /// </summary>
        public string Category { get; set; }

        public List<Column> Columns { get; set; }
    }

    public class Column
    {
        public int Index { get; set; }

        public string Name { get; set; }

        public string NumberFormat { get; set; }

        public int ColorIndex { get; set; }

        public string Validation { get; set; }

        /// <summary>
        /// 用于绑定Table 的 Expression
        /// </summary>
        public string Expression { get; set; }

        /// <summary>
        /// 表间公式
        /// </summary>
        public string Formular { get; set; }

        public string FormularR1C1 { get; set; }

        public ColumnMode Mode { get; set; }
    }

    public class RangeInfo
    {
        public int Index { get; set; }

        /// <summary>
        /// 数据筛选标识
        /// </summary>
        public string Category { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Range Address
        /// </summary>
        public string ReferTo { get; set; }

        /// <summary>
        /// 存储模版数据
        /// </summary>
        public string XmlData { get; set; }

        /// <summary>
        /// 公式
        /// </summary>
        public object[,] Formula { get; set; }

        /// <summary>
        /// 交互映射关系
        /// </summary>
        public Dictionary<string, Cell> Map { get; set; }

        /// <summary>
        /// 手工输入的cell集合
        /// </summary>
        public List<string> Entrys { get; set; }

        /// <summary>
        /// 查询前清除内容cell集合
        /// </summary>
        public List<string> Clears { get; set; }

        public List<Vh> BlueClear { get; set; }

        public List<Vh> VH { get; set; }
    }

    public class Cell
    {
        /// <summary>
        /// Cell Address eg: $A$1
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 备注 ： S01_C01_R01
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 大数据查询的值或手工填写的值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 数据来源
        /// </summary>
        public CellType CellType { get; set; }
    }

    public class Vh
    {
        public int V { get; set; }
        public int H { get; set; }

        public string Key { get; set; }
    }

    public enum CellType
    {
        /// <summary>
        /// 公式
        /// </summary>
        Formular = 0,

        /// <summary>
        /// 大数据
        /// </summary>
        BigData = 1,

        /// <summary>
        /// 手工录入
        /// </summary>
        Entry = 2
    }

    public enum ColumnMode
    {
        Normal = 0,

        Formular = 1,

        ReadOnly = 2,

        /// <summary>
        /// 用于绑定 Table 的 Expression
        /// </summary>
        Expression = 3,

        ServerSelection = 4
    }
}
