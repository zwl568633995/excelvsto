﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class BaseRequest
    {
        public string UserId { get; set; }

        public string TokenId { get; set; }

        public string ReportId { get; set; }

        public string Month { get; set; }

        public string OrgCode { get; set; }
    }
}
