﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class DataResponse<T> : BaseResponse
    {
        public IEnumerable<T> Data { get; set; }
    }
}
