﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class BaseMsg
    {
        public string MsgCode { get; set; }

        public string MsgDesc { get; set; }
    }

    public class BaseResponse:BaseMsg
    {     
        public string ReportId { get; set; }

        public string Month { get; set; }

        public string OrgCode { get; set; }

        public string DataVersion { get; set; }
    }

    public static class Extension
    {
        public static bool Success(this BaseMsg response)
        {
            return !string.IsNullOrEmpty(response.MsgCode) && response.MsgCode.Equals("S0000", StringComparison.OrdinalIgnoreCase);
        }
    }
}
