﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class AuthReportRequest: BaseRequest
    {
        public string ParentId { get; set; }

        [JsonIgnore]
        public ProductType ProductType { get; set; }

        public string Type => ((int)ProductType).ToString().PadLeft(2, '0');
    }
}
