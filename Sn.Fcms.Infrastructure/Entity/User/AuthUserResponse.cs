﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class AuthUserResponse:BaseMsg
    {
        public string TokenId { get; set; }

        public string UserName { get; set; }

        public List<string> ProcedureAuth { get; set; }
    }
}
