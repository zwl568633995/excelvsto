﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class AuthUserRequest
    {
        public string UserId { get; set; }

        public string Password { get; set; }

        public ClientType ClientId { get; set; }

        public string ClientVersion { get; set; }
    }
}
