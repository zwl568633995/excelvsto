﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class Report
    {
        public string ReportId { get; set; }

        public string ReportName { get; set; }

        public string ShortName { get; set; }

        public string Auth { get; set; }

        /// <summary>
        /// Auth=='W' &
        /// 0:编写权限
        /// 1:审核人1
        /// 2:审核人2
        /// 3:定版不可修改
        /// </summary>
        public string LevelAuth { get; set; }

        public string Type { get; set; }
    }
}
