﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class Organization
    {
        public string OrgCode { get; set; }

        public string OrgName { get; set; }

        /// <summary>
        /// 是否是特殊公司 T：是
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// YS 云商
        /// </summary>
        public string CompanyProperty { get; set; }

        /// <summary>
        /// 是否合并公司 T：是 
        /// </summary>
        public string MergedOrg { get; set; }

        /// <summary>
        /// 小程序导出时报表名称
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// 运算层级
        /// </summary>
        public int Level { get; set; }

        [JsonIgnore]
        public string CombineName => OrgCode + ":" + OrgName;
    }
}
