﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class BaseType
    {
        public string MainDataCodeType { get; set; }
    }

    public class ArAgeBase : BaseType
    {
        public decimal ReclassifyBlance { get; set; }

        public decimal Agel1 { get; set; }

        public decimal Age12 { get; set; }

        public decimal Age23 { get; set; }

        public decimal Age34 { get; set; }

        public decimal Age45 { get; set; }

        public decimal Ageg5 { get; set; }
    }

    public class ArAgeItem : ArAgeBase
    {
        public string Id { get; set; }

        public string MainDataCode { get; set; }

        public string MainDataCodeName { get; set; }
    }
}
