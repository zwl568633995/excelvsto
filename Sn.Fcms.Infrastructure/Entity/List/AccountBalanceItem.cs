﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class AccountBalanceItem
    {
        public string SubjectNumber { get; set; }//科目编号

        public string GeneralRemark { get; set; } //总帐科目详细注释

        public string CurrencyCode { get; set; } //货币代码

        public string TransferBalance { get; set; }//用本币计算的结转余额

        public string BeforeBalance { get; set; } //前一期间的余额

        public string DebitBalance { get; set; }//在制表期间的借方余额

        public string CreditBalance { get; set; }//报表期间的贷方余额

        public string AccumulatedBalance { get; set; }//累计余额
    }
}
