﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class QueryParam
    {        
        public string UserId { get; set; }

        public string TokenId { get; set; }

        public string ReportId { get; set; }

        public string SaleOrg { get; set; }

        public string StoreCode { get; set; }

        //public string PayTime { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string PayCode { get; set; }

        public string DealType { get; set; }

        public int StartIndex { get; set; }

        public int CountPerPage { get; set; }
    }
}
