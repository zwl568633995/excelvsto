﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class BillInfo
    {      
        public int BillType { get; set; }

        public string B2cOrderId { get; set; }

        public string EffectiveTime { get; set; }

        public string ClientId { get; set; }

        public string OrderItemId { get; set; }

        public string PosOrderId { get; set; }

        public string SapOrderType { get; set; }

        public string OrderItemType { get; set; }

        public string DistChannel { get; set; }

        public string SaleOrg { get; set; }

        public string StoreCode { get; set; }

        public string CurrencyType { get; set; }

        public string MerchantCode { get; set; }

        public string PayOrg { get; set; }

        public string PayStore { get; set; }

        public string DebtId { get; set; }

        public string PreOrderItemId { get; set; }

        public string SpecialReturnFlag { get; set; }

        public string DealType { get; set; }

        public string DealTypeFlag { get; set; }

        public string PayCode { get; set; }

        public string PayName { get; set; }

        public decimal Amount { get; set; }

        public decimal PayAmount { get; set; }

        public string PayTime { get; set; }

        public string FlowId { get; set; }

        public string BillNo { get; set; }

        public string AlipayPayNo { get; set; }

        public string OrderItemStatus { get; set; }

        public string OtherOrder { get; set; }

        public string OtherCode { get; set; }

        public decimal OtherAmount { get; set; }

        public string RefundTime { get; set; }
    }
}
