﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class DealType
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
