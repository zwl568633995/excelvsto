﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class QueryListDataResponse<T> : BaseResponse
    {
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// HK 总记录数
        /// </summary>
        public int Amount { get; set; }
    }
}
