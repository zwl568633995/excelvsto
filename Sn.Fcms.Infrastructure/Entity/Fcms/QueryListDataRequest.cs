﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Infrastructure.Entity
{
    public class QueryListDataRequest:BaseRequest
    {
        public string DataVersion { get; set; }

        public int StartIndex { get; set; }

        public int CountPerPage { get; set; }
    }
}
