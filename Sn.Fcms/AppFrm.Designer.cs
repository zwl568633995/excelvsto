﻿namespace Sn.Fcms
{
    partial class AppFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_List = new System.Windows.Forms.Button();
            this.btn_fcms = new System.Windows.Forms.Button();
            this.btn_single = new System.Windows.Forms.Button();
            this.pnlBottomLine = new System.Windows.Forms.Panel();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.topPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btn_List
            // 
            this.btn_List.Location = new System.Drawing.Point(82, 121);
            this.btn_List.Name = "btn_List";
            this.btn_List.Size = new System.Drawing.Size(106, 76);
            this.btn_List.TabIndex = 0;
            this.btn_List.Text = "清单报表";
            this.btn_List.UseVisualStyleBackColor = true;
            this.btn_List.Click += new System.EventHandler(this.btn_List_Click);
            // 
            // btn_fcms
            // 
            this.btn_fcms.Location = new System.Drawing.Point(229, 121);
            this.btn_fcms.Name = "btn_fcms";
            this.btn_fcms.Size = new System.Drawing.Size(106, 76);
            this.btn_fcms.TabIndex = 0;
            this.btn_fcms.Text = "财务往来";
            this.btn_fcms.UseVisualStyleBackColor = true;
            this.btn_fcms.Click += new System.EventHandler(this.btn_fcms_Click);
            // 
            // btn_single
            // 
            this.btn_single.Location = new System.Drawing.Point(376, 121);
            this.btn_single.Name = "btn_single";
            this.btn_single.Size = new System.Drawing.Size(106, 76);
            this.btn_single.TabIndex = 0;
            this.btn_single.Text = "单体公司";
            this.btn_single.UseVisualStyleBackColor = true;
            this.btn_single.Click += new System.EventHandler(this.btn_single_Click);
            // 
            // pnlBottomLine
            // 
            this.pnlBottomLine.BackColor = System.Drawing.Color.ForestGreen;
            this.pnlBottomLine.Cursor = System.Windows.Forms.Cursors.No;
            this.pnlBottomLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomLine.Location = new System.Drawing.Point(0, 339);
            this.pnlBottomLine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlBottomLine.Name = "pnlBottomLine";
            this.pnlBottomLine.Size = new System.Drawing.Size(625, 1);
            this.pnlBottomLine.TabIndex = 34;
            // 
            // rightPanel
            // 
            this.rightPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.rightPanel.Cursor = System.Windows.Forms.Cursors.No;
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(624, 0);
            this.rightPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(1, 339);
            this.rightPanel.TabIndex = 35;
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.leftPanel.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftPanel.Location = new System.Drawing.Point(0, 0);
            this.leftPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(1, 339);
            this.leftPanel.TabIndex = 36;
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.topPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(1, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(623, 1);
            this.topPanel.TabIndex = 37;
            // 
            // AppFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 340);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.leftPanel);
            this.Controls.Add(this.rightPanel);
            this.Controls.Add(this.pnlBottomLine);
            this.Controls.Add(this.btn_single);
            this.Controls.Add(this.btn_fcms);
            this.Controls.Add(this.btn_List);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AppFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "财务管理平台";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitle_MouseDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_List;
        private System.Windows.Forms.Button btn_fcms;
        private System.Windows.Forms.Button btn_single;
        private System.Windows.Forms.Panel pnlBottomLine;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.Panel topPanel;
    }
}