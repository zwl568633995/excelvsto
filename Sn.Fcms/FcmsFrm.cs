﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace Sn.Fcms
{
    public partial class FcmsFrm : Form
    {       
        private IniFiles config = null;
        private string appNewVersion = string.Empty;
        private string appName = string.Empty;

        public FcmsFrm()
        {
            InitializeComponent();
        }

        private void FcmsFrm_Load(object sender, EventArgs e)
        {
            CheckClient();
        }

        private void CheckClient()
        {            
            try
            {
                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "app.ini");
                config = new IniFiles(path);

                string appVersion = config.ReadValue("version", "appVersion");               
                appName = config.ReadValue("startup", "appName");
                var appResponse = AutoHelper.CheckClient(new ClientUpgradeRequest() { ClientId = AutoHelper.ClientId, ClientVersion = appVersion }).Result;
                if (appResponse.IsSuccess())
                {
                    // 应用程序升级完成后不需要升级模版每次应用程序升级包含最新的模版数据
                    if (AutoHelper.CompareVersion(appVersion, appResponse.ClientVersion))
                    {
                        //ShowApp();
                        appNewVersion = appResponse.ClientVersion;
                        Download(appResponse.ClientUrl);
                    }
                    else
                    {
                        StartupApp(appName);
                    }
                }
                else
                {
                    StartupApp(appName);
                }
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("try upgrade app error,Message:{0},StackTrace:{1}", ex.Message, ex.StackTrace);
                StartupApp(appName);
            }
        }       

        private void ShowApp()
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private string savePath = string.Empty;

        private void Download(string url)
        {
            try
            {
                savePath = Path.Combine(Application.StartupPath, Path.GetFileName(url));

                using(WebClient client = new WebClient())
                {
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                    client.DownloadProgressChanged += Client_DownloadProgressChanged;
                    this.lbProcessBarInfo.Text = "下载文件中...";
                    client.DownloadFileAsync(new Uri(url), savePath);                    
                    client.Dispose();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("下载升级信息失败！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NLogger.Error.Error("try download zip file failure,url:{0},message:{1},stackTrace:{2}", url, ex.Message, ex.StackTrace);
            }
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.progressBar.Value = e.ProgressPercentage;
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                this.lbProcessBarInfo.Text = "正在解压缩文件...";
                Decompression();

                if (!string.IsNullOrEmpty(appNewVersion))
                    config.WriteValue("version", "appVersion", appNewVersion);

                if (File.Exists(savePath))
                    File.Delete(savePath);

                StartupApp(appName);
            }
            catch (Exception ex)
            {                
                MessageBox.Show("解压缩文件失败！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NLogger.Error.Error("try decompression zip file fail,message:{0},stackTrace:{1}", ex.Message, ex.StackTrace);
            }
        }

        private void Decompression()
        {
            if (!File.Exists(savePath))
                return;

            ResetBar();
            ZipHelper.UnZip(savePath, Application.StartupPath, null, true);
        }

        private void ResetBar()
        {
            this.progressBar.Minimum = 0;
            this.progressBar.Style = ProgressBarStyle.Marquee;
        }

        private void StartupApp(string appName)
        {
            try
            {
                if (string.IsNullOrEmpty(appName))
                    return;

                this.Hide();

                Process.Start(Path.Combine(Application.StartupPath, appName));
                var proc = Process.GetCurrentProcess();
                proc.Kill();
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("try startup app：{0} error，Message:{0},StackTrace:{1}", appName, ex.Message, ex.StackTrace);
            }
        }
    }
}
