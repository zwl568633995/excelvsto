﻿using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Serializers;
using RestSharp.Deserializers;
using System.Configuration;
using System;

namespace Sn.Fcms
{
    public class AutoHelper
    {
        private static string serverUrl = ConfigurationManager.AppSettings["serverUrl"];
        public static int ClientId = int.Parse(ConfigurationManager.AppSettings["clientId"]);

        public const string SuccessCode = "S0000";

        #region 02 check client

        public static Task<ClientUpgradeResponse> CheckClient(ClientUpgradeRequest request)
        {
            var resource = "cfss-server/checkClient.htm";

            return ExcutePostTask<ClientUpgradeResponse>(serverUrl, resource, request)
                .ContinueWith(t =>
                {
                    if (t.Result != null)
                    {
                        if (!t.Result.IsSuccess())
                        {
                            NLogger.Error.Error("check client error, ReponseStatus:{0}, HttpStatus{1}:{2}",
                                 t.Result.ResponseStatus, t.Result.StatusDescription, t.Result.ErrorException.StackTrace);

                            return new ClientUpgradeResponse()
                            {
                                MsgCode = t.Result.StatusCode.ToString(),
                                MsgDesc = t.Result.ErrorMessage
                            };
                        }

                        NLogger.Info.Info("check client success, http result:{0}", t.Result.Content);
                        return t.Result.Data;
                    }

                    return null;
                });
        }

        public static bool CompareVersion(string localVersion, string newVersion)
        {
            Version lv, nv;

            if (!Version.TryParse(localVersion, out lv))
                return false;

            if (!Version.TryParse(newVersion, out nv))
                return false;

            return nv > lv;
        }

        #endregion

        #region RestSharp

        public static Task<IRestResponse<T>> ExcutePostTask<T>(string serverUrl, string resource, object requestBody)
        {
            RestClient client = new RestClient(serverUrl);
            client.UseJsonDotNetDeserializerWrapper();

            var restRequest = new RestRequest(resource);
            restRequest.UseJsonDotNetSerializerWrapper();

            restRequest.AddBody(requestBody);

            return client.ExecutePostTaskAsync<T>(restRequest);
        }

        #endregion
    }

    public class ClientUpgradeRequest
    {
        public int ClientId { get; set; }

        public string ClientVersion { get; set; }
    }

    public class ClientUpgradeResponse
    {
        public string MsgCode { get; set; }

        public string MsgDesc { get; set; }

        public string ClientUrl { get; set; }

        public string ClientVersion { get; set; }

        public string Description { get; set; }
    }


    public static class RestClientExtension
    {
        public static bool IsSuccess(this ClientUpgradeResponse response)
        {
            return response.MsgCode.Equals(AutoHelper.SuccessCode, StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsSuccess(this IRestResponse response)
        {
            return response.ResponseStatus == ResponseStatus.Completed && (int)response.StatusCode / 100 == 2;
        }

        public static void UseJsonDotNetSerializerWrapper(this IRestRequest request)
        {
            request.RequestFormat = DataFormat.Json;
            request.JsonSerializer = new JsonDotNetSerializerWrapper();
        }
        public static void UseJsonDotNetDeserializerWrapper(this IRestClient client)
        {
            client.RemoveHandler("application/json");
            client.AddHandler("application/json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/json");
            client.AddHandler("text/json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/x-json");
            client.AddHandler("text/x-json", new JsonDotNetDeserializerWrapper());
            client.RemoveHandler("text/javascript");
            client.AddHandler("text/javascript", new JsonDotNetDeserializerWrapper());
        }

        internal class JsonDotNetSerializerWrapper : ISerializer
        {
            public JsonDotNetSerializerWrapper()
            {
                this.ContentType = "application/json";
            }

            public string Serialize(object obj)
            {
                return JsonConvert.SerializeObject(obj);
            }

            public string DateFormat { get; set; }
            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string ContentType { get; set; }
        }

        internal class JsonDotNetDeserializerWrapper : IDeserializer
        {
            public T Deserialize<T>(IRestResponse response)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string DateFormat { get; set; }
        }
    }
}
