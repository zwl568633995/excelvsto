﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Sn.Fcms
{
    public partial class AppFrm : Form
    {
        #region API Import引用

        /// <summary>
        /// 移动无边框窗体声明引用
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        //移动窗体
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;

        //改变窗体大小
        public const int WMSZ_LEFT = 0xF001;
        public const int WMSZ_RIGHT = 0xF002;
        public const int WMSZ_TOP = 0xF003;
        public const int WMSZ_TOPLEFT = 0xF004;
        public const int WMSZ_TOPRIGHT = 0xF005;
        public const int WMSZ_BOTTOM = 0xF006;
        public const int WMSZ_BOTTOMLEFT = 0xF007;
        public const int WMSZ_BOTTOMRIGHT = 0xF008;

        #endregion

        public AppFrm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 移动窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlTitle_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);
        }

        private void btn_List_Click(object sender, EventArgs e)
        {
            StartupApp("FinanceList.xlsx");
        }

        private void btn_fcms_Click(object sender, EventArgs e)
        {
            StartupApp("fcms.xlsx");
        }

        private void btn_single_Click(object sender, EventArgs e)
        {
            StartupApp("singleCompany.xlsx");
        }

        private void StartupApp(string appName)
        {
            try
            {
                if (string.IsNullOrEmpty(appName))
                    return;

                this.Hide();

                Process.Start(Path.Combine(Application.StartupPath, appName));
                var proc = Process.GetCurrentProcess();
                proc.Kill();
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("try startup app：{0} error，Message:{0},StackTrace:{1}", appName, ex.Message, ex.StackTrace);
            }
        }
    }
}
