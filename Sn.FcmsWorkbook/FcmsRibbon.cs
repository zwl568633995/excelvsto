﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Microsoft.Office.Interop.Excel;
using Sn.Fcms.Infrastructure;
using RestSharp;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sn.Fcms.Service;
using System.Text.RegularExpressions;
using System.Threading;

namespace Sn.FcmsWorkbook
{
    public partial class FcmsRibbon
    {
        #region 自定义的成员变量
        private static Form m_CurrentForm;
        public static Form CurrentForm
        {
            get
            {
                return m_CurrentForm;
            }

            set
            {
                m_CurrentForm = value;
            }
        }

        private int m_PageIndex = 1;
        /// <summary>
        /// 当前页面
        /// </summary>
        public int PageIndex
        {
            get { return m_PageIndex; }
            set { m_PageIndex = value; }
        }

        public int m_PageSize = 100;
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }

        public int m_RecordCount = 0;
        /// <summary>
        /// 总记录数
        /// </summary>
        public int RecordCount
        {
            get { return m_RecordCount; }
            set { m_RecordCount = value; }
        }
        public int m_PageCount = 0;
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount
        {
            get
            {
                if (m_PageSize != 0)
                {
                    m_PageCount = GetPageCount();
                }

                return m_PageCount;
            }
            set
            {
                m_PageCount = value;
            }
        }

        public DataVerificationInfo DataVerificationInfo;
        public PayDataVerSubjectInfo PayDataVerificationInfo;
        public PayDataVerMainDataInfo PayDataVerMainDataInfo;
        public SubDimensionInfo SubDimensionInfo;
        public PaySubDimensionInfo PaySubDimensionInfo;
        public DebtInfo DebtInfo;
        public PayDebtInfo PayDebtInfo;
        public AccountInfo AccountInfo;
        public PayAccountInfo PayAccountInfo;
        public OtherDataVerSubjectInfo OtherDataVerSubjectInfo;
        public OtherDataVerMainDataInfo OtherDataVerMainDataInfo;
        public OtherAccountInfo OtherAccountInfo;
        public OtherSubDimensionInfo OtherSubDimensionInfo;
        public OtherDebtInfo OtherDebtInfo;
        /// <summary>
        /// 页码变化触发事件
        /// </summary>
        public event EventHandler OnPageChanged;
        #endregion

        #region 控件加载和查询事件
        /// <summary>
        /// 加载
        /// </summary>
        private void FcmsRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            SetFormCtrEnabled(false);
        }

        /// <summary>
        /// 窗体关闭
        /// </summary>
        private void FcmsRibbon_Close(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /// <summary>
        /// 查询
        /// </summary>
        private void btnQuery_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Globals.Ribbons.FcmsRibbon.PageIndex = 1;
                Globals.Ribbons.FcmsRibbon.PageCount = 0;
                Globals.Ribbons.FcmsRibbon.RecordCount = 0;
                Globals.Ribbons.FcmsRibbon.PageSize = 2000;
                Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
                if (worksheet.ListObjects.Count == 0)
                    return;

                if (worksheet.Name == ConstStr.No1SheetName)
                {
                    m_CurrentForm = new frmRecDataVerification() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No2SheetName)
                {
                    m_CurrentForm = new frmRecSubDimension() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No3SheetName)
                {
                    m_CurrentForm = new frmRecDebt() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No4SheetName)
                {
                    m_CurrentForm = new frmRecAccount() { Tag = worksheet.Name };
                }
                if (worksheet.Name == ConstStr.No5SheetName)
                {
                    m_CurrentForm = new frmPayDataVerification() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No6SheetName)
                {
                    m_CurrentForm = new frmPaySubDimension() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No7SheetName)
                {
                    m_CurrentForm = new frmPayDebt() { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No8SheetName)
                {
                    m_CurrentForm = new frmPayAccount() { Tag = worksheet.Name };
                }
                else if (worksheet.Name==ConstStr.No9SheetName)
                {
                    m_CurrentForm = new frmPayDataVerMainData() { Tag = worksheet.Name };
                }
                else if (worksheet.Name==ConstStr.No10SheetName)
                {
                    m_CurrentForm = new frmOtherDataVerSubject { Tag=worksheet.Name};
                }
                else if (worksheet.Name == ConstStr.No11SheetName)
                {
                    m_CurrentForm = new frmOtherSubDimension {Tag=worksheet.Name }; 
                }
                else if (worksheet.Name == ConstStr.No12SheetName)
                {
                    m_CurrentForm = new frmOtherDebt { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No13SheetName)
                {
                    m_CurrentForm = new frmOtherAccount { Tag = worksheet.Name };
                }
                else if (worksheet.Name == ConstStr.No14SheetName)
                {
                    m_CurrentForm = new frmOtherDataVerMainData { Tag = worksheet.Name };
                }

                if (m_CurrentForm != null)
                    m_CurrentForm.Show(new WindowHand { Handle = new IntPtr(Globals.ThisWorkbook.ThisApplication.Hwnd) });
            }
            catch { }
        }

        /// <summary>
        /// 另存为
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveAs_Click(object sender, RibbonControlEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Excel File(*.xlsx)|*.xlsx";
            if (dialog.ShowDialog() == DialogResult.No)
                return;

            try
            {
                string filename = dialog.FileName;
                if (string.IsNullOrEmpty(filename))
                    return;

                Globals.ThisWorkbook.RemoveCustomization();
                Globals.ThisWorkbook.SaveCopyAs(filename);
                MessageBox.Show("另存副本成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show("另存副本失败:" + ex.Message);
            }
        }
        #endregion

        #region 首页,上一页,下一页,末页以及跳转事件
        /// <summary>
        /// 首页
        /// </summary>
        private async void lnFirst_Click(object sender, RibbonControlEventArgs e)
        {
            PageIndex = 1;
            DrawControl(true);

            Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
            if (worksheet.ListObjects.Count == 0)
                return;

            ////StartIndex设置值
            await SetRequestIndex(worksheet, MathMethod.Equals, 1);
        }

        /// <summary>
        /// 上一页
        /// </summary>
        private async void preLink_Click(object sender, RibbonControlEventArgs e)
        {
            PageIndex = Math.Max(1, PageIndex - 1);
            DrawControl(true);
            Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
            if (worksheet.ListObjects.Count == 0)
                return;

            ////StartIndex设置值
            int iIndex = Convert.ToInt32(cbPerCount.Text) * (PageIndex-1) + 1;
            await SetRequestIndex(worksheet, MathMethod.Equals, iIndex);
        }

        /// <summary>
        /// 下一页
        /// </summary>
        private async void NextLink_Click(object sender, RibbonControlEventArgs e)
        {
            PageIndex = Math.Min(PageCount, PageIndex + 1);
            DrawControl(true);
            Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
            if (worksheet.ListObjects.Count == 0)
                return;

            ////StartIndex设置值
            int iIndex = Convert.ToInt32(cbPerCount.Text)*(PageIndex-1)+1;
            await SetRequestIndex(worksheet, MathMethod.Equals, iIndex);
        }

        /// <summary>
        /// 最后一页
        /// </summary>
        private async void lnkLast_Click(object sender, RibbonControlEventArgs e)
        {
            PageIndex = PageCount;
            DrawControl(true);
            Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
            if (worksheet.ListObjects.Count == 0)
                return;

            ////StartIndex设置值
            int iIndex = Convert.ToInt32(lblRecordCount.Label) - (Convert.ToInt32(lblRecordCount.Label) % Convert.ToInt32(cbPerCount.Text)) + 1;
            await SetRequestIndex(worksheet, MathMethod.Equals, iIndex);
        }

        /// <summary>
        /// 跳转
        /// </summary>
        private async void btnGoTo_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                if (m_PageCount == 0)
                {
                    MessageBox.Show("当前数据为空。");
                    return;
                }

                if (string.IsNullOrEmpty(ebGoTo.Text))
                {
                    MessageBox.Show("跳转的页码为空。");
                    return;
                }

                if (!Regex.IsMatch(ebGoTo.Text, "^[0-9]+"))
                {
                    MessageBox.Show("输入的页码数必须为有效数字。");
                    return;
                }

                int temp = Convert.ToInt32(ebGoTo.Text);
                if (temp > m_PageCount || temp < 1)
                {
                    MessageBox.Show("所选页数不能大于总页数,并且所选页数不能小于1");
                    return;
                }

                PageIndex = Convert.ToInt32(ebGoTo.Text);
                DrawControl(true);
                Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet;
                if (worksheet.ListObjects.Count == 0)
                    return;

                ////StartIndex设置值
                int iIndex = (Convert.ToInt32(ebGoTo.Text) - 1) * Convert.ToInt32(cbPerCount.Text) + 1;
                await SetRequestIndex(worksheet, MathMethod.Equals, iIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show("错误消息");
                NLogger.Error.Error("跳转错误消息 {0} \r\n {1}", ex.Message, ex.StackTrace);
                return;
            }
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 设置按钮的显示
        /// </summary>
        public void SetFormCtrEnabled(bool bEnable)
        {
            BtnNext.Enabled = bEnable;
            BtnPre.Enabled = bEnable;
            BtnLast.Enabled = bEnable;
            BtnFirst.Enabled = bEnable;
        }

        /// <summary>
        /// 计算总页数
        /// </summary>
        /// <returns></returns>
        public int GetPageCount()
        {
            try
            {
                if (PageSize == 0)
                    return 0;

                m_PageSize = Convert.ToInt32(cbPerCount.Text);
                int pageCount = RecordCount / PageSize;
                return RecordCount % PageSize == 0 ? RecordCount / PageSize : RecordCount / PageSize + 1;
            }
            catch { }

            return 0;
        }

        /// <summary>
        /// 页面控件呈现
        /// </summary>
        public void DrawControl(bool callEvent)
        {
            try
            {
                lblPgeInfo.Label = PageCount == 0 ? "0/" + PageCount.ToString() : PageIndex.ToString() + "/" + PageCount.ToString();
                lblRecordCount.Label = RecordCount.ToString();

                if (callEvent && OnPageChanged != null)
                {
                    OnPageChanged(this, null);//当前分页数字改变时，触发委托事件
                }

                bool btnShow = PageCount == 1 ? false : true;
                SetFormCtrEnabled(btnShow);
                if (PageCount == 1)
                {
                    BtnNext.Enabled = false;
                    BtnPre.Enabled = false;
                }
                else if (PageIndex == 1)//第一页
                {
                    BtnFirst.Enabled = false;
                    BtnPre.Enabled = false;
                }
                else if (PageIndex == PageCount)//最后一页
                {
                    BtnNext.Enabled = false;
                    BtnLast.Enabled = false;
                }
            }
            catch { }
        }

        /// <summary>
        /// 设置查询的开始页码
        /// </summary>
        private async Task SetRequestIndex(Worksheet worksheet, MathMethod method, int index)
        {
            try
            {
                if (worksheet.Name == ConstStr.No1SheetName)
                {
                    frmRecDataVerification frm = UIFunction<frmRecDataVerification>.GetInstance();
                    frm.DataVerificationRequest = Globals.Ribbons.FcmsRibbon.DataVerificationInfo.DataVerificationRequest;
                    if (frm.DataVerificationRequest == null)
                        return;

                    frm.DataVerificationRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                              frm.DataVerificationRequest.StartIndex + index : frm.DataVerificationRequest.StartIndex - index);
                    await QueryData.BinderData<DataVerificationItem, DataVerificationRequest, DataVerificationSumItem>(frm.DataVerificationRequest, false);
                }
                else if (worksheet.Name == ConstStr.No2SheetName)
                {
                    frmRecSubDimension frm = UIFunction<frmRecSubDimension>.GetInstance();
                    frm.SubDimensionRequest = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.SubDimensionRequest;
                    if (frm.SubDimensionRequest == null)
                        return;

                    frm.SubDimensionRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                             frm.SubDimensionRequest.StartIndex + index : frm.SubDimensionRequest.StartIndex - index);
                    await QueryData.BinderData<SubDimensionItem, SubDimensionRequest, SubDimensionSumItem>(frm.SubDimensionRequest, false);
                }
                else if (worksheet.Name == ConstStr.No3SheetName)
                {
                    frmRecDebt frm = UIFunction<frmRecDebt>.GetInstance();
                    frm.DebtTableRequest = Globals.Ribbons.FcmsRibbon.DebtInfo.DebtTableRequest;
                    if (frm.DebtTableRequest == null)
                        return;

                    frm.DebtTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                            frm.DebtTableRequest.StartIndex + index : frm.DebtTableRequest.StartIndex - index);
                    await QueryData.BinderData<DebtTableItem, DebtTableRequest, DebtTableSumItem>(frm.DebtTableRequest, false);
                }
                else if (worksheet.Name == ConstStr.No4SheetName)
                {
                    frmRecAccount frm = UIFunction<frmRecAccount>.GetInstance();
                    frm.AccountTableRequest = Globals.Ribbons.FcmsRibbon.AccountInfo.AccountTableRequest;
                    if (frm.AccountTableRequest == null)
                        return;

                    frm.AccountTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.AccountTableRequest.StartIndex + index : frm.AccountTableRequest.StartIndex - index);
                    await QueryData.BinderData<AccountTableItem, AccountTableRequest, AccountTableSumItem>( frm.AccountTableRequest, false);
                }
                if (worksheet.Name == ConstStr.No5SheetName)
                {
                    frmPayDataVerification frm = UIFunction<frmPayDataVerification>.GetInstance();
                    frm.PayDataVerificationRequest = Globals.Ribbons.FcmsRibbon.PayDataVerificationInfo.PayDataVerificationRequest;
                    if (frm.PayDataVerificationRequest == null)
                        return;

                    frm.PayDataVerificationRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                              frm.PayDataVerificationRequest.StartIndex + index : frm.PayDataVerificationRequest.StartIndex - index);
                    await QueryData.BinderData<PayDataVerSubjectItem, PayDataVerificationRequest, PayDataVerSujectSumItem>(frm.PayDataVerificationRequest, false);
                }
                else if (worksheet.Name == ConstStr.No6SheetName)
                {
                    frmPaySubDimension frm = UIFunction<frmPaySubDimension>.GetInstance();
                    frm.PaySubDimensionRequest = Globals.Ribbons.FcmsRibbon.PaySubDimensionInfo.PaySubDimensionRequest;
                    if (frm.PaySubDimensionRequest == null)
                        return;

                    frm.PaySubDimensionRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                             frm.PaySubDimensionRequest.StartIndex + index : frm.PaySubDimensionRequest.StartIndex - index);
                    await QueryData.BinderData<PaySubDimensionItem, PaySubDimensionRequest, PaySubDimensionSumItem>(frm.PaySubDimensionRequest, false);
                }
                else if (worksheet.Name == ConstStr.No7SheetName)
                {
                    frmPayDebt frm = UIFunction<frmPayDebt>.GetInstance();
                    frm.PayDebtTableRequest = Globals.Ribbons.FcmsRibbon.PayDebtInfo.PayDebtTableRequest;
                    if (frm.PayDebtTableRequest == null)
                        return;

                    frm.PayDebtTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                            frm.PayDebtTableRequest.StartIndex + index : frm.PayDebtTableRequest.StartIndex - index);
                    await QueryData.BinderData<PayDebtTableItem, PayDebtTableRequest, PayDebtTableSumItem>(frm.PayDebtTableRequest, false);
                }
                else if (worksheet.Name == ConstStr.No8SheetName)
                {
                    frmPayAccount frm = UIFunction<frmPayAccount>.GetInstance();
                    frm.PayAccountTableRequest = Globals.Ribbons.FcmsRibbon.PayAccountInfo.PayAccountTableRequest;
                    if (frm.PayAccountTableRequest == null)
                        return;

                    frm.PayAccountTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PayAccountTableRequest.StartIndex + index : frm.PayAccountTableRequest.StartIndex - index);
                    await QueryData.BinderData<PayAccountTableItem, PayAccountTableRequest, PayAccountTableSumItem>(frm.PayAccountTableRequest, false);
                }
                else if (worksheet.Name == ConstStr.No9SheetName)
                {
                    frmPayDataVerMainData frm = UIFunction<frmPayDataVerMainData>.GetInstance();
                    frm.PayDataVerificationRequest = Globals.Ribbons.FcmsRibbon.PayDataVerMainDataInfo.PayDataVerificationRequest;
                    if (frm.PayDataVerificationRequest == null)
                        return;

                    frm.PayDataVerificationRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PayDataVerificationRequest.StartIndex + index : frm.PayDataVerificationRequest.StartIndex - index);
                    await QueryData.BinderData<PayDataVerMainDataItem, PayDataVerificationRequest, PayDataVerMainDataSumItem>(frm.PayDataVerificationRequest, false);
                }
                else if (worksheet.Name == ConstStr.No10SheetName)
                {
                    frmOtherDataVerSubject frm = UIFunction<frmOtherDataVerSubject>.GetInstance();
                    frm.PayDataVerificationRequest = Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo.PayDataVerificationRequest;
                    if (frm.PayDataVerificationRequest == null)
                        return;

                    frm.PayDataVerificationRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                              frm.PayDataVerificationRequest.StartIndex + index : frm.PayDataVerificationRequest.StartIndex - index);
                    await QueryData.BinderData<PayDataVerSubjectItem, PayDataVerificationRequest, PayDataVerSujectSumItem>(frm.PayDataVerificationRequest, false);
                }
                else if (worksheet.Name == ConstStr.No11SheetName)
                {
                    frmOtherSubDimension frm= UIFunction<frmOtherSubDimension>.GetInstance();
                    frm.PaySubDimensionRequest = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.PaySubDimensionRequest;
                    if (frm.PaySubDimensionRequest==null)
                    {
                        return;
                    }

                    frm.PaySubDimensionRequest.StartIndex= (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PaySubDimensionRequest.StartIndex + index : frm.PaySubDimensionRequest.StartIndex - index);
                    await QueryData.BinderData<OtherSubDimensionItem, PaySubDimensionRequest, OtherSubDimensionSumItem>(frm.PaySubDimensionRequest, false);
                }
                else if (worksheet.Name == ConstStr.No12SheetName)
                {
                    frmOtherDebt frm = UIFunction<frmOtherDebt>.GetInstance();
                    frm.PayDebtTableRequest = Globals.Ribbons.FcmsRibbon.OtherDebtInfo.PayDebtTableRequest;
                    if (frm.PayDebtTableRequest == null)
                    {
                        return;
                    }

                    frm.PayDebtTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PayDebtTableRequest.StartIndex + index : frm.PayDebtTableRequest.StartIndex - index);
                    await QueryData.BinderData<OtherDebtTableItem, PayDebtTableRequest, OtherDebtTableSumItem>(frm.PayDebtTableRequest, false);
                }
                else if (worksheet.Name == ConstStr.No13SheetName)
                {
                    frmOtherAccount frm = UIFunction<frmOtherAccount>.GetInstance();
                    frm.PayAccountTableRequest = Globals.Ribbons.FcmsRibbon.OtherAccountInfo.PayAccountTableRequest;
                    if (frm.PayAccountTableRequest == null)
                        return;

                    frm.PayAccountTableRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PayAccountTableRequest.StartIndex + index : frm.PayAccountTableRequest.StartIndex - index);
                    await QueryData.BinderData<PayAccountTableItem, PayAccountTableRequest, PayAccountTableSumItem>(frm.PayAccountTableRequest, false);
                }
                else if(worksheet.Name == ConstStr.No14SheetName)
                {
                    frmOtherDataVerMainData frm = UIFunction<frmOtherDataVerMainData>.GetInstance();
                    frm.PayDataVerificationRequest = Globals.Ribbons.FcmsRibbon.OtherDataVerMainDataInfo.PayDataVerificationRequest;
                    if (frm.PayDataVerificationRequest == null)
                        return;

                    frm.PayDataVerificationRequest.StartIndex = (method == MathMethod.Equals) ? index : (method == MathMethod.Add ?
                                                           frm.PayDataVerificationRequest.StartIndex + index : frm.PayDataVerificationRequest.StartIndex - index);
                    await QueryData.BinderData<PayDataVerMainDataItem, PayDataVerificationRequest, PayDataVerMainDataSumItem>(frm.PayDataVerificationRequest, false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                NLogger.Error.Error("错误消息 {0} \r\n {1}", ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// 设置列的数据格式
        /// </summary>
        /// <param name="worksheet"></param>
        public void SetFormat(Worksheet worksheet)
        {
            Range range = null;
            string formatStr = "_ * #,##0.00_ ;_ * -#,##0.00_ ;_ * \"-\"??_ ;_ @_ ";
            if (worksheet.Name == ConstStr.No1SheetName)
            {
                range = worksheet.Cells[2, 9];
                range.Formula = "=G2=H2";
                range = worksheet.Columns[8];
                range.NumberFormat = formatStr;
                range = worksheet.Columns[9];
                range.NumberFormat = formatStr;
                worksheet.Sort.SetRange(range);
                range = worksheet.Cells[4, 9];
                range.Formula = "=G4=H4";
            }
            if (worksheet.Name == ConstStr.No2SheetName)
            {
                for (int i = 12; i < 45; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No3SheetName)
            {
                for (int i = 9; i < 42; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No4SheetName)
            {
                for (int i = 24; i < 26; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No5SheetName)
            {
                range = worksheet.Cells[2, 10];
                range.Formula = "=H2=I2";
                range = worksheet.Cells[4, 10];
                range.Formula = "=H4=I4";
                for (int i = 8; i < 10; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No6SheetName)
            {
                for (int i = 12; i < 47; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No7SheetName)
            {
                for (int i = 9; i < 44; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No8SheetName)
            {
                for (int i = 24; i < 26; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No9SheetName)
            {
                range = worksheet.Cells[2, 10];
                range.Formula = "=H2=I2";
                range = worksheet.Cells[4, 10];
                range.Formula = "=H4=I4";
                for (int i = 8; i < 10; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No10SheetName)
            {
                range = worksheet.Cells[2, 10];
                range.Formula = "=H2=I2";
                range = worksheet.Cells[4, 10];
                range.Formula = "=H4=I4";
                for (int i = 8; i < 10; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No11SheetName)
            {
                for (int i = 12; i < 46; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No12SheetName)
            {
                for (int i = 9; i < 43; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No13SheetName)
            {
                for (int i = 24; i < 26; i++)
                {
                    range = worksheet.Columns[i];
                    range.NumberFormat = formatStr;
                }
            }
            if (worksheet.Name == ConstStr.No14SheetName)
            {
                range = worksheet.Cells[2, 10];
                range.Formula = "=H2=I2";
                range = worksheet.Cells[4, 10];
                range.Formula = "=H4=I4";
                range = worksheet.Columns[8];
                range.NumberFormat = formatStr;
                range = worksheet.Columns[9];
                range.NumberFormat = formatStr;
            }
        }
        #endregion
    }
}
