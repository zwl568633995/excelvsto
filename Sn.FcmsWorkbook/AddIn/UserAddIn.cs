﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sn.FcmsWorkbook
{
    public class UserAddIn
    {
        #region 01 ini



        #endregion

        #region 02 login 

        public static void Logon(ClientType clientType,ProductType product,string path)
        {
            try
            {
                var version = ConfigHelper.ReadValue(path, AddInConst.AppVersion);             
                LogonFrm form = new LogonFrm(version, clientType,product);
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    UserSession.Auth = true;
                }
                else
                {
                    UserSession.Auth = false;
                }
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("Logon workshee error: {0}", ex.StackTrace);
            }
        }
        #endregion
    }
}
