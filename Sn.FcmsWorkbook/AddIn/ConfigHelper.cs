﻿using Sn.Fcms.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.FcmsWorkbook
{
    public class ConfigHelper
    {
        private const string iniFile = "app.ini";

        public static string ReadValue(string path,string key)
        {
            string filePath = System.IO.Path.Combine(path, iniFile);
            IniFiles config = new IniFiles(filePath);
            return config.ReadValue("version", key);
        }
    }
}
