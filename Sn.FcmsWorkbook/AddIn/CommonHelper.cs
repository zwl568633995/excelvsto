﻿using Microsoft.Office.Interop.Excel;
using RestSharp;
using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sn.FcmsWorkbook
{
    public class WindowHand : IWin32Window
    {
        public IntPtr Handle { get; set; }
    }

    public class ConstStr
    {
        #region 相应表名称
        public static List<string> GetAllSheetNames()
        {
            List<string> AllSheetNames = new List<string>();
            AllSheetNames.Add(No1SheetName);
            AllSheetNames.Add(No2SheetName);
            AllSheetNames.Add(No3SheetName);
            AllSheetNames.Add(No4SheetName);
            AllSheetNames.Add(No5SheetName);
            AllSheetNames.Add(No6SheetName);
            AllSheetNames.Add(No7SheetName);
            AllSheetNames.Add(No8SheetName);
            AllSheetNames.Add(No9SheetName);
            AllSheetNames.Add(No10SheetName);
            AllSheetNames.Add(No11SheetName);
            AllSheetNames.Add(No12SheetName);
            AllSheetNames.Add(No13SheetName);
            AllSheetNames.Add(No14SheetName);
            return AllSheetNames;
        }

        /// <summary>
        /// No.1应收预收数据核对
        /// </summary>
        public static string No1SheetName = "应收预收数据核对";
        /// <summary>
        /// No.2应收预收余额监控表-科目维度
        /// </summary>
        public static string No2SheetName = "应收预收余额监控表-科目维度";
        /// <summary>
        /// No.3应收预收余额监控表-债务债券维度
        /// </summary>
        public static string No3SheetName = "应收预收余额监控表-债权债务维度";
        /// <summary>
        /// No.4应收预收账款明细报表
        /// </summary>
        public static string No4SheetName = "应收预收账款明细报表";

        /// <summary>
        /// No.5应付预付数据核对
        /// </summary>
        public static string No5SheetName = "应付预付数据核对-科目";
        /// <summary>
        /// No.6应付预付余额监控表-科目维度
        /// </summary>
        public static string No6SheetName = "应付预付余额监控表-科目维度";
        /// <summary>
        /// No.7应付预付余额监控表-债务债券维度
        /// </summary>
        public static string No7SheetName = "应付预付余额监控表-债权债务维度";
        /// <summary>
        /// No.8应付预付账款明细报表
        /// </summary>
        public static string No8SheetName = "应付预付账款明细报表";
        /// <summary>
        /// No.9应付预付数据核对
        /// </summary>
        public static string No9SheetName = "应付预付数据核对-债权债务";

        /// <summary>
        /// No.10应付预付数据核对
        /// </summary>
        public static string No10SheetName = "其他应收应付数据核对-科目";
        /// <summary>
        /// No.11应付预付余额监控表-科目维度
        /// </summary>
        public static string No11SheetName = "其他应收应付余额监控表-科目维度";
        /// <summary>
        /// No.12应付预付余额监控表-债务债券维度
        /// </summary>
        public static string No12SheetName = "其他应收应付余额监控表-债权债务维度";
        /// <summary>
        /// No.13应付预付账款明细报表
        /// </summary>
        public static string No13SheetName = "其他应收应付明细报表";
        /// <summary>
        /// No.14应付预付数据核对
        /// </summary>
        public static string No14SheetName = "其他应收应付数据核对-债权债务";
        #endregion

        #region 请求数据的资源
        public static string ServerURL = System.Configuration.ConfigurationManager.AppSettings["ServerUrl"];
        public static string QuerySource = System.Configuration.ConfigurationManager.AppSettings["QueryData"];
        public static string SumSource = System.Configuration.ConfigurationManager.AppSettings["SumData"];
        #endregion

        #region 各Sheet页绑定数据的起始行
        public static string No1RowStart = System.Configuration.ConfigurationManager.AppSettings["No1RowStart"];
        public static string No2RowStart = System.Configuration.ConfigurationManager.AppSettings["No2RowStart"];
        public static string No3RowStart = System.Configuration.ConfigurationManager.AppSettings["No3RowStart"];
        public static string No4RowStart = System.Configuration.ConfigurationManager.AppSettings["No4RowStart"];
        #endregion

        /// <summary>
        /// 保护视图密码
        /// </summary>
        public static string ProtectPassword = "ProtectPassword";
    }

    /// <summary>
    /// 状态栏状态记录
    /// </summary>
    public class StatusInfo
    {
        public bool BtnLnFirst { get; set; }
        public bool BtnPreLink { get; set; }
        public bool BtnNextLink { get; set; }
        public bool BtnLnkLast { get; set; }
        public string PageIndex { get; set; }
        public string PageCount { get; set; }
        public string PageSize { get; set; }
        public string RecordCount { get; set; }
        public string GoTo { get; set; }
        public string QueryData { get; set; }
    }

    /// <summary>
    /// 上一页下一页的标识
    /// </summary>
    public enum MathMethod
    {
        Equals = 0,
        Add = 1,
        Reduce = 2
    }

    public class CommonHelper
    {
        /// <summary>
        /// 获取列参数,过滤含有公式的列
        /// </summary>
        /// <param name="lstColumns">列</param>
        /// <returns></returns>
        public static string[] GetParams(ListColumns lstColumns, string sSheetName)
        {
            List<string> lstPaarams = new List<string>();
            foreach (ListColumn item in lstColumns)
            {
                lstPaarams.Add(item.Name);
            }

            return lstPaarams.ToArray();
        }

        /// <summary>
        /// 刷新信息
        /// </summary>
        /// <param name="worksheet"></param>
        public static void RefreshInfo(Worksheet worksheet)
        {
            try
            {
                Globals.Ribbons.FcmsRibbon.SetFormat(worksheet);
                Globals.Ribbons.FcmsRibbon.DrawControl(true);
            }
            catch { }
            finally
            {
                Globals.ThisWorkbook.PrecisionAsDisplayed = true;
                Globals.ThisWorkbook.ThisApplication.ScreenUpdating = true;
                Globals.ThisWorkbook.ThisApplication.Interactive = true;
            }
        }
    }

    public class QueryData
    {
        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <typeparam name="T">回复类</typeparam>
        /// <typeparam name="K">请求类</typeparam>
        /// <param name="worksheet">Sheet页</param>
        /// <param name="sSource">访问资源</param>
        /// <param name="request">请求</param>
        /// <returns></returns>
        public static async Task BinderData<T, R, S>(R request, bool ShowError) where T : new() where S : new()
        {
            Worksheet worksheet = (Worksheet)Globals.ThisWorkbook.ActiveSheet;
            Globals.ThisWorkbook.ThisApplication.ScreenUpdating = false;
            Globals.ThisWorkbook.ThisApplication.Interactive = false;
            Globals.Ribbons.FcmsRibbon.lblDate.Label = (request as DataVerificationRequest).Month;
            NativeWindow xlMainWindow = new NativeWindow();
            xlMainWindow.AssignHandle(new IntPtr(Globals.ThisWorkbook.ThisApplication.Hwnd));
            List<T> lstData = new List<T>();
            S SumData = default(S);
            int rowStart = 0;
            try
            {
                LoadingHelper.ShowLoading("正在查询......", xlMainWindow, async p =>
                 {
                     ////获取合计数据
                     var sumData = await GetResultData<T, S>(request, ConstStr.SumSource);
                     SumData = sumData.TotalMap;
                     if (sumData == null)
                         return;

                     if (Convert.ToInt32(sumData.TotalCount)>1000000)
                     {
                         MessageBox.Show("当前查询数据量大于1000000，请增加查询条件或联系开发人员。");
                         return;
                     }

                     ////获取明细数据
                     if ((request as DataVerificationRequest).CountPerPage > ConfigConst.Count)
                     {
                         await GetAllData<T, R, S>(request, lstData);
                     }
                     else
                     {
                         var data = await GetResultData<T, S>(request, ConstStr.QuerySource);
                         if (data == null)
                             return;

                         lstData.AddRange(data.FloatCols);
                     }
                 });

                ////绑定合计数值
                BindSumData(worksheet, SumData, ref rowStart);
                ////绑定数据
                System.Data.DataTable dtBinder = GetBinderTable<T>(lstData, worksheet);
                BinderLstData(worksheet, dtBinder, rowStart);
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("查询绑定数据失败:" + ex.Message);
            }
            finally
            {
                CommonHelper.RefreshInfo(worksheet);
                xlMainWindow.ReleaseHandle();
                GC.Collect();
            }
        }

        /// <summary>
        /// 获取数据表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lstData"></param>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        private static System.Data.DataTable GetBinderTable<T>(List<T> lstData, Worksheet worksheet)
        {
            Microsoft.Office.Tools.Excel.ListObject vstoList = null;
            System.Data.DataTable dt = Common.ConvertToDatatable<T>(lstData);
            try
            {
                foreach (Microsoft.Office.Interop.Excel.ListObject item in worksheet.ListObjects)
                {
                    vstoList = Globals.Factory.GetVstoObject(item);
                    string[] paramsStrs = CommonHelper.GetParams(vstoList.ListColumns, worksheet.Name);
                    if (paramsStrs == null || paramsStrs.Length == 0)
                    {
                        MessageBox.Show("模板为空。");
                        return null;
                    }

                    for (int i = 0; i < paramsStrs.Length; i++)
                    {
                        dt.Columns[paramsStrs[i]].SetOrdinal(i);
                    }
                }

            }
            catch { }

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="dt"></param>
        private static void BinderLstData(Worksheet worksheet, System.Data.DataTable dt, int rowStart)
        {
            Range rng;
            int rowCount = 0;
            int colCount = 0;
            ////删除以往数据
            if (worksheet.UsedRange.Rows.Count > dt.Rows.Count + rowStart - 1)
            {
                rowCount = worksheet.UsedRange.Rows.Count;
                colCount = worksheet.UsedRange.Columns.Count;
                rng = worksheet.Range[worksheet.Cells[rowStart, 1], worksheet.Cells[rowCount, colCount]];
                rng.EntireRow.Delete(XlDeleteShiftDirection.xlShiftUp);
            }

            //定义范围
            rowCount = dt.Rows.Count;
            colCount = dt.Columns.Count;
            object[,] objarr = new object[rowCount, colCount];
            try
            {
                for (int i = 0; i < rowCount; i++)
                {
                    for (int j = 0; j < colCount; j++)
                    {
                        objarr[i, j] = dt.Rows[i].ItemArray[j];
                    }
                }

                //定义范围
                rng = worksheet.Range[worksheet.Cells[rowStart, 1], worksheet.Cells[rowCount + rowStart - 1, colCount]];
                //数组赋值工作表范围
                rng.Value2 = objarr;
            }
            catch(Exception ex)
            {
            }
        }

        /// <summary>
        /// 查询绑定合计数据
        /// </summary>
        /// <typeparam name="Y"></typeparam>
        /// <param name="worksheet"></param>
        /// <param name="vstoList"></param>
        /// <param name="sumData"></param>
        private static void BindSumData<Y>(Worksheet worksheet, Y sumData, ref int rowStart) where Y : new()
        {
            try
            {
                if (worksheet.Name == ConstStr.No1SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No1RowStart);
                    for (int i = 7; i < 9; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as DataVerificationSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No2SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No2RowStart);
                    for (int i = 12; i < 45; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as SubDimensionSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No3SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No3RowStart);
                    for (int i = 9; i < 42; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as DebtTableSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No4SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No4RowStart);
                    for (int i = 24; i < 25; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as AccountTableSumItem).GetValue(sName);
                    }
                }
                if (worksheet.Name == ConstStr.No5SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No1RowStart);
                    for (int i = 8; i < 10; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayDataVerSujectSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No6SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No2RowStart);
                    for (int i = 12; i < 47; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as PaySubDimensionSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No7SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No3RowStart);
                    for (int i = 9; i < 44; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as PayDebtTableSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No8SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No4RowStart);
                    for (int i = 24; i < 25; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayAccountTableSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No9SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No1RowStart);
                    for (int i = 8; i < 10; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayDataVerMainDataSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No10SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No1RowStart);
                    for (int i = 8; i < 10; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayDataVerSujectSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No11SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No2RowStart);
                    for (int i = 12; i < 46; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as OtherSubDimensionSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No12SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No3RowStart);
                    for (int i = 9; i < 43; i++)
                    {
                        string sName = (worksheet.Cells[4, i] as Range).Value + "SUM";
                        worksheet.Cells[3, i] = (sumData as OtherDebtTableSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No13SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No4RowStart);
                    for (int i = 24; i < 26; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayAccountTableSumItem).GetValue(sName);
                    }
                }
                else if (worksheet.Name == ConstStr.No14SheetName)
                {
                    rowStart = Convert.ToInt32(ConstStr.No1RowStart);
                    for (int i = 8; i < 10; i++)
                    {
                        string sName = (worksheet.Cells[3, i] as Range).Value + "SUM";
                        worksheet.Cells[2, i] = (sumData as PayDataVerMainDataSumItem).GetValue(sName);
                    }
                }
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("绑定合计数据失败:" + ex.Message);
                return;
            }
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="sSource"></param>
        /// <param name="request"></param>
        /// <param name="lstData"></param>
        /// <returns></returns>
        private static async Task GetAllData<T, K, Y>(K request, List<T> lstData) where T : new()

        {
            try
            {
                int index = (request as DataVerificationRequest).StartIndex;
                int bk = 0;
                int totalCount = (request as DataVerificationRequest).CountPerPage;
                while (true)
                {
                    (request as DataVerificationRequest).StartIndex = index;
                    (request as DataVerificationRequest).CountPerPage = (totalCount - lstData.Count) < ConfigConst.Count ? totalCount - lstData.Count : ConfigConst.Count;
                    var data = await GetResultData<T, Y>(request, ConstStr.QuerySource);
                    lstData.AddRange(data.FloatCols);
                    index += ConfigConst.Count;
                    if (data.FloatCols.Length < ConfigConst.Count || lstData.Count >= totalCount)
                    {
                        break;
                    }

                    bk++;
                    // 防止死循环
                    if (bk >= 100)
                    {
                        NLogger.Error.Error("请求超过100次，可能存在死循环");
                        break;
                    }
                }

                (request as DataVerificationRequest).CountPerPage = Convert.ToInt32(Globals.Ribbons.FcmsRibbon.cbPerCount.Text);
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("获取数据失败:" + ex.Message);
                return;
            }
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="T">回复类</typeparam>
        /// <param name="request">请求</param>
        /// <param name="sResource">资源地址</param>
        /// <returns>数据</returns>
        private static async Task<ResponseModel<T, S>> GetResultData<T, S>(object request, string resource) where T : new()
        {
            try
            {
                if (string.IsNullOrEmpty(ConstStr.ServerURL))
                {
                    NLogger.Info.Info("【AppCongig】未配置ServerUrl地址。");
                    return null;
                }

                IRestResponse<ResponseModel<T, S>> result = await RestfulApiClient.ExcutePostTask<ResponseModel<T, S>>(ConstStr.ServerURL, resource, request);
                if (result != null)
                {
                    if (result.Data == null)
                    {
                        MessageBox.Show(result.ErrorMessage);
                    }

                    if (result.IsSuccessStatusCode())
                    {
                        if (result.Data.MsgCode == "E0000" | result.Data.MsgDesc == "服务端异常")
                            MessageBox.Show("服务端异常。");

                        if ((result.Data.FloatCols == null || result.Data.FloatCols.Length == 0) && (result.Data.TotalMap == null))
                            MessageBox.Show("未查询到相关数据。");

                        if (result.Data.TotalCount!=null)
                        {
                            Globals.Ribbons.FcmsRibbon.RecordCount = Convert.ToInt32(result.Data.TotalCount);
                            Globals.Ribbons.FcmsRibbon.GetPageCount();
                            Globals.Ribbons.FcmsRibbon.DrawControl(false);
                        }

                        return result.Data;
                    }
                }
            }
            catch (Exception ex)
            {
                NLogger.Error.Error("跳转错误消息 {0} \r\n {1}", ex.Message, ex.StackTrace);
                return null;
            }

            return null;
        }
    }
}
