﻿namespace Sn.FcmsWorkbook
{
    partial class FcmsRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public FcmsRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl1 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl2 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl3 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl4 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl5 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl6 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl7 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl8 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl9 = this.Factory.CreateRibbonDropDownItem();
            this.tabFcms = this.Factory.CreateRibbonTab();
            this.groupQuery = this.Factory.CreateRibbonGroup();
            this.btnQuery = this.Factory.CreateRibbonButton();
            this.separator7 = this.Factory.CreateRibbonSeparator();
            this.BtnFirst = this.Factory.CreateRibbonButton();
            this.BtnPre = this.Factory.CreateRibbonButton();
            this.BtnNext = this.Factory.CreateRibbonButton();
            this.BtnLast = this.Factory.CreateRibbonButton();
            this.btnSaveAs = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.label2 = this.Factory.CreateRibbonLabel();
            this.label3 = this.Factory.CreateRibbonLabel();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.lblRecordCount = this.Factory.CreateRibbonLabel();
            this.lblPgeInfo = this.Factory.CreateRibbonLabel();
            this.cbPerCount = this.Factory.CreateRibbonComboBox();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.ebGoTo = this.Factory.CreateRibbonEditBox();
            this.btnGoTo = this.Factory.CreateRibbonButton();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.label4 = this.Factory.CreateRibbonLabel();
            this.label5 = this.Factory.CreateRibbonLabel();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.lblUser = this.Factory.CreateRibbonLabel();
            this.lblDate = this.Factory.CreateRibbonLabel();
            this.tabFinanceList = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btn_QueryList = this.Factory.CreateRibbonButton();
            this.tabFcms.SuspendLayout();
            this.groupQuery.SuspendLayout();
            this.group2.SuspendLayout();
            this.group3.SuspendLayout();
            this.tabFinanceList.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabFcms
            // 
            this.tabFcms.Groups.Add(this.groupQuery);
            this.tabFcms.Groups.Add(this.group2);
            this.tabFcms.Groups.Add(this.group3);
            this.tabFcms.Label = "财务往来";
            this.tabFcms.Name = "tabFcms";
            // 
            // groupQuery
            // 
            this.groupQuery.Items.Add(this.btnQuery);
            this.groupQuery.Items.Add(this.separator7);
            this.groupQuery.Items.Add(this.BtnFirst);
            this.groupQuery.Items.Add(this.BtnPre);
            this.groupQuery.Items.Add(this.BtnNext);
            this.groupQuery.Items.Add(this.BtnLast);
            this.groupQuery.Items.Add(this.btnSaveAs);
            this.groupQuery.Label = "查询功能";
            this.groupQuery.Name = "groupQuery";
            // 
            // btnQuery
            // 
            this.btnQuery.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnQuery.Label = "查询数据";
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.OfficeImageId = "QueryShowTable";
            this.btnQuery.ShowImage = true;
            this.btnQuery.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnQuery_Click);
            // 
            // separator7
            // 
            this.separator7.Name = "separator7";
            // 
            // BtnFirst
            // 
            this.BtnFirst.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnFirst.Label = "首页";
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.OfficeImageId = "FirstPage";
            this.BtnFirst.ShowImage = true;
            this.BtnFirst.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.lnFirst_Click);
            // 
            // BtnPre
            // 
            this.BtnPre.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnPre.Label = "上页";
            this.BtnPre.Name = "BtnPre";
            this.BtnPre.OfficeImageId = "PreviousPage";
            this.BtnPre.ShowImage = true;
            this.BtnPre.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.preLink_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnNext.Label = "下页";
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.OfficeImageId = "NextPage";
            this.BtnNext.ShowImage = true;
            this.BtnNext.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.NextLink_Click);
            // 
            // BtnLast
            // 
            this.BtnLast.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnLast.Label = "末页";
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.OfficeImageId = "LastPage";
            this.BtnLast.ShowImage = true;
            this.BtnLast.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.lnkLast_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSaveAs.Label = "另存副本";
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.OfficeImageId = "SaveAndClose";
            this.btnSaveAs.ShowImage = true;
            this.btnSaveAs.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSaveAs_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.label1);
            this.group2.Items.Add(this.label2);
            this.group2.Items.Add(this.label3);
            this.group2.Items.Add(this.separator1);
            this.group2.Items.Add(this.lblRecordCount);
            this.group2.Items.Add(this.lblPgeInfo);
            this.group2.Items.Add(this.cbPerCount);
            this.group2.Items.Add(this.separator2);
            this.group2.Items.Add(this.ebGoTo);
            this.group2.Items.Add(this.btnGoTo);
            this.group2.Label = "页数信息";
            this.group2.Name = "group2";
            // 
            // label1
            // 
            this.label1.Label = "   总记录数：";
            this.label1.Name = "label1";
            // 
            // label2
            // 
            this.label2.Label = "   页码信息：";
            this.label2.Name = "label2";
            // 
            // label3
            // 
            this.label3.Label = "每页记录数：";
            this.label3.Name = "label3";
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Label = "0";
            this.lblRecordCount.Name = "lblRecordCount";
            // 
            // lblPgeInfo
            // 
            this.lblPgeInfo.Label = "0/0";
            this.lblPgeInfo.Name = "lblPgeInfo";
            // 
            // cbPerCount
            // 
            ribbonDropDownItemImpl1.Label = "100";
            ribbonDropDownItemImpl2.Label = "500";
            ribbonDropDownItemImpl3.Label = "1000";
            ribbonDropDownItemImpl4.Label = "2000";
            ribbonDropDownItemImpl5.Label = "5000";
            ribbonDropDownItemImpl6.Label = "10000";
            ribbonDropDownItemImpl7.Label = "20000";
            ribbonDropDownItemImpl8.Label = "50000";
            ribbonDropDownItemImpl9.Label = "100000";
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl1);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl2);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl3);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl4);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl5);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl6);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl7);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl8);
            this.cbPerCount.Items.Add(ribbonDropDownItemImpl9);
            this.cbPerCount.Label = " ";
            this.cbPerCount.Name = "cbPerCount";
            this.cbPerCount.ShowLabel = false;
            this.cbPerCount.SizeString = "80，1";
            this.cbPerCount.Text = "2000";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // ebGoTo
            // 
            this.ebGoTo.Label = "请输入页码";
            this.ebGoTo.Name = "ebGoTo";
            this.ebGoTo.SizeString = "100，1";
            this.ebGoTo.Text = null;
            // 
            // btnGoTo
            // 
            this.btnGoTo.Label = "点击页码跳转";
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.OfficeImageId = "ExportSharePointList";
            this.btnGoTo.ShowImage = true;
            this.btnGoTo.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnGoTo_Click);
            // 
            // group3
            // 
            this.group3.Items.Add(this.label4);
            this.group3.Items.Add(this.label5);
            this.group3.Items.Add(this.separator3);
            this.group3.Items.Add(this.lblUser);
            this.group3.Items.Add(this.lblDate);
            this.group3.Name = "group3";
            // 
            // label4
            // 
            this.label4.Label = "当前用户：";
            this.label4.Name = "label4";
            // 
            // label5
            // 
            this.label5.Label = "当前日期：";
            this.label5.Name = "label5";
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // lblUser
            // 
            this.lblUser.Label = "未选择";
            this.lblUser.Name = "lblUser";
            // 
            // lblDate
            // 
            this.lblDate.Label = "未选择";
            this.lblDate.Name = "lblDate";
            // 
            // tabFinanceList
            // 
            this.tabFinanceList.Groups.Add(this.group1);
            this.tabFinanceList.Label = "清单管理";
            this.tabFinanceList.Name = "tabFinanceList";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btn_QueryList);
            this.group1.Label = "功能";
            this.group1.Name = "group1";
            // 
            // btn_QueryList
            // 
            this.btn_QueryList.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_QueryList.Label = "查询数据";
            this.btn_QueryList.Name = "btn_QueryList";
            this.btn_QueryList.OfficeImageId = "QueryShowTable";
            this.btn_QueryList.ShowImage = true;
            // 
            // FcmsRibbon
            // 
            this.Name = "FcmsRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tabFcms);
            this.Tabs.Add(this.tabFinanceList);
            this.Close += new System.EventHandler(this.FcmsRibbon_Close);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.FcmsRibbon_Load);
            this.tabFcms.ResumeLayout(false);
            this.tabFcms.PerformLayout();
            this.groupQuery.ResumeLayout(false);
            this.groupQuery.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.tabFinanceList.ResumeLayout(false);
            this.tabFinanceList.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabFcms;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupQuery;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabFinanceList;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnQuery;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_QueryList;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator7;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnFirst;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnPre;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnNext;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnLast;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label2;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label3;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbPerCount;
        public Microsoft.Office.Tools.Ribbon.RibbonLabel lblRecordCount;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnGoTo;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox ebGoTo;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblPgeInfo;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label4;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label5;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblUser;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblDate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSaveAs;
    }

    partial class ThisRibbonCollection
    {
        public FcmsRibbon FcmsRibbon
        {
            get { return this.GetRibbon<FcmsRibbon>(); }
        }
    }
}
