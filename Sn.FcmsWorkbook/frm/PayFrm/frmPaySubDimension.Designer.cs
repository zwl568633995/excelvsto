﻿namespace Sn.FcmsWorkbook
{
    partial class frmPaySubDimension
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblYear = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbYear = new System.Windows.Forms.ComboBox();
            this.cbMonth = new System.Windows.Forms.ComboBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbMainDataType = new System.Windows.Forms.CheckBox();
            this.rtbMainDataType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbComapanys = new System.Windows.Forms.CheckBox();
            this.rtbCreditCode = new System.Windows.Forms.TextBox();
            this.rtbMainDataCode = new System.Windows.Forms.TextBox();
            this.rtbSubjects = new System.Windows.Forms.TextBox();
            this.rtbCompanys = new System.Windows.Forms.TextBox();
            this.cbCreditCode = new System.Windows.Forms.CheckBox();
            this.cbMainDataCode = new System.Windows.Forms.CheckBox();
            this.cbSubjectCode = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTitle.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitle
            // 
            this.pnlTitle.Location = new System.Drawing.Point(2, 0);
            this.pnlTitle.Size = new System.Drawing.Size(926, 35);
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(63, 14);
            this.lblTitle.Text = "查询数据";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblYear.Location = new System.Drawing.Point(27, 28);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(56, 17);
            this.lblYear.TabIndex = 1;
            this.lblYear.Text = "选择年份";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(478, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "选择月份";
            // 
            // cbYear
            // 
            this.cbYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYear.FormattingEnabled = true;
            this.cbYear.ItemHeight = 17;
            this.cbYear.Location = new System.Drawing.Point(89, 25);
            this.cbYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbYear.Name = "cbYear";
            this.cbYear.Size = new System.Drawing.Size(349, 25);
            this.cbYear.TabIndex = 0;
            // 
            // cbMonth
            // 
            this.cbMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMonth.FormattingEnabled = true;
            this.cbMonth.ItemHeight = 17;
            this.cbMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cbMonth.Location = new System.Drawing.Point(542, 25);
            this.cbMonth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMonth.Name = "cbMonth";
            this.cbMonth.Size = new System.Drawing.Size(342, 25);
            this.cbMonth.TabIndex = 1;
            // 
            // btnQuery
            // 
            this.btnQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuery.Location = new System.Drawing.Point(716, 608);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(87, 33);
            this.btnQuery.TabIndex = 9;
            this.btnQuery.Text = "查询(&Q)";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(809, 608);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 33);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Green;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(928, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1, 647);
            this.panel2.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(1, 646);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(927, 1);
            this.panel3.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Green;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(1, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 646);
            this.panel4.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbMainDataType);
            this.groupBox1.Controls.Add(this.rtbMainDataType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbComapanys);
            this.groupBox1.Controls.Add(this.rtbCreditCode);
            this.groupBox1.Controls.Add(this.rtbMainDataCode);
            this.groupBox1.Controls.Add(this.rtbSubjects);
            this.groupBox1.Controls.Add(this.rtbCompanys);
            this.groupBox1.Controls.Add(this.cbCreditCode);
            this.groupBox1.Controls.Add(this.cbMainDataCode);
            this.groupBox1.Controls.Add(this.cbSubjectCode);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblSubject);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbMonth);
            this.groupBox1.Controls.Add(this.lblYear);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbYear);
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(906, 562);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询维度";
            // 
            // cbMainDataType
            // 
            this.cbMainDataType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbMainDataType.AutoSize = true;
            this.cbMainDataType.Location = new System.Drawing.Point(32, 533);
            this.cbMainDataType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMainDataType.Name = "cbMainDataType";
            this.cbMainDataType.Size = new System.Drawing.Size(51, 21);
            this.cbMainDataType.TabIndex = 44;
            this.cbMainDataType.Text = "排除";
            this.cbMainDataType.UseVisualStyleBackColor = true;
            // 
            // rtbMainDataType
            // 
            this.rtbMainDataType.Location = new System.Drawing.Point(89, 466);
            this.rtbMainDataType.Multiline = true;
            this.rtbMainDataType.Name = "rtbMainDataType";
            this.rtbMainDataType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbMainDataType.Size = new System.Drawing.Size(795, 88);
            this.rtbMainDataType.TabIndex = 43;
            this.toolTip1.SetToolTip(this.rtbMainDataType, "多个社会信用证代码/税务登记证以“,”形式分隔开");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(3, 466);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 41;
            this.label1.Text = "债权债务品类";
            // 
            // cbComapanys
            // 
            this.cbComapanys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbComapanys.AutoSize = true;
            this.cbComapanys.Location = new System.Drawing.Point(32, 130);
            this.cbComapanys.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbComapanys.Name = "cbComapanys";
            this.cbComapanys.Size = new System.Drawing.Size(51, 21);
            this.cbComapanys.TabIndex = 40;
            this.cbComapanys.Text = "排除";
            this.cbComapanys.UseVisualStyleBackColor = true;
            // 
            // rtbCreditCode
            // 
            this.rtbCreditCode.Location = new System.Drawing.Point(89, 365);
            this.rtbCreditCode.Multiline = true;
            this.rtbCreditCode.Name = "rtbCreditCode";
            this.rtbCreditCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbCreditCode.Size = new System.Drawing.Size(795, 88);
            this.rtbCreditCode.TabIndex = 39;
            this.toolTip1.SetToolTip(this.rtbCreditCode, "多个社会信用证代码/税务登记证以“,”形式分隔开");
            this.rtbCreditCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbMainDataCode
            // 
            this.rtbMainDataCode.Location = new System.Drawing.Point(89, 265);
            this.rtbMainDataCode.Multiline = true;
            this.rtbMainDataCode.Name = "rtbMainDataCode";
            this.rtbMainDataCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbMainDataCode.Size = new System.Drawing.Size(795, 88);
            this.rtbMainDataCode.TabIndex = 38;
            this.toolTip1.SetToolTip(this.rtbMainDataCode, "多个债权债务方编码以“,”形式分隔开");
            this.rtbMainDataCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbSubjects
            // 
            this.rtbSubjects.Location = new System.Drawing.Point(89, 163);
            this.rtbSubjects.Multiline = true;
            this.rtbSubjects.Name = "rtbSubjects";
            this.rtbSubjects.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbSubjects.Size = new System.Drawing.Size(795, 88);
            this.rtbSubjects.TabIndex = 37;
            this.toolTip1.SetToolTip(this.rtbSubjects, "多个科目编码以“,”形式分隔开");
            this.rtbSubjects.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbCompanys
            // 
            this.rtbCompanys.Location = new System.Drawing.Point(89, 63);
            this.rtbCompanys.Multiline = true;
            this.rtbCompanys.Name = "rtbCompanys";
            this.rtbCompanys.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbCompanys.Size = new System.Drawing.Size(795, 88);
            this.rtbCompanys.TabIndex = 36;
            this.toolTip1.SetToolTip(this.rtbCompanys, "多个公司代码以“,”形式分隔开");
            this.rtbCompanys.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // cbCreditCode
            // 
            this.cbCreditCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbCreditCode.AutoSize = true;
            this.cbCreditCode.Location = new System.Drawing.Point(32, 432);
            this.cbCreditCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCreditCode.Name = "cbCreditCode";
            this.cbCreditCode.Size = new System.Drawing.Size(51, 21);
            this.cbCreditCode.TabIndex = 26;
            this.cbCreditCode.Text = "排除";
            this.cbCreditCode.UseVisualStyleBackColor = true;
            // 
            // cbMainDataCode
            // 
            this.cbMainDataCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbMainDataCode.AutoSize = true;
            this.cbMainDataCode.Location = new System.Drawing.Point(32, 332);
            this.cbMainDataCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMainDataCode.Name = "cbMainDataCode";
            this.cbMainDataCode.Size = new System.Drawing.Size(51, 21);
            this.cbMainDataCode.TabIndex = 25;
            this.cbMainDataCode.Text = "排除";
            this.cbMainDataCode.UseVisualStyleBackColor = true;
            // 
            // cbSubjectCode
            // 
            this.cbSubjectCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSubjectCode.AutoSize = true;
            this.cbSubjectCode.Location = new System.Drawing.Point(32, 230);
            this.cbSubjectCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSubjectCode.Name = "cbSubjectCode";
            this.cbSubjectCode.Size = new System.Drawing.Size(51, 21);
            this.cbSubjectCode.TabIndex = 24;
            this.cbSubjectCode.Text = "排除";
            this.cbSubjectCode.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(10, 365);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 51);
            this.label5.TabIndex = 14;
            this.label5.Text = "社会信用证\r\n代码/税务登\r\n记证";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(15, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 34);
            this.label4.TabIndex = 12;
            this.label4.Text = "债权债务方\r\n编码";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSubject.Location = new System.Drawing.Point(27, 166);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(56, 17);
            this.lblSubject.TabIndex = 10;
            this.lblSubject.Text = "科目编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(27, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "公司代码";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmPaySubDimension
            // 
            this.AcceptButton = this.btnQuery;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(930, 648);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnQuery);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPaySubDimension";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "查询数据";
            this.Load += new System.EventHandler(this.frmQueryData_Load);
            this.Controls.SetChildIndex(this.btnQuery, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbYear;
        private System.Windows.Forms.ComboBox cbMonth;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox cbCreditCode;
        private System.Windows.Forms.CheckBox cbMainDataCode;
        private System.Windows.Forms.CheckBox cbSubjectCode;
        private System.Windows.Forms.TextBox rtbCreditCode;
        private System.Windows.Forms.TextBox rtbMainDataCode;
        private System.Windows.Forms.TextBox rtbSubjects;
        private System.Windows.Forms.TextBox rtbCompanys;
        private System.Windows.Forms.CheckBox cbComapanys;
        private System.Windows.Forms.TextBox rtbMainDataType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbMainDataType;
    }
}