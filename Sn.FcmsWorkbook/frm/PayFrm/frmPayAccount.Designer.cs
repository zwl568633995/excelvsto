﻿namespace Sn.FcmsWorkbook
{
    partial class frmPayAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbCompanys = new System.Windows.Forms.CheckBox();
            this.rtbMainDataType = new System.Windows.Forms.TextBox();
            this.cbMainDataType = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rtbCreditCode = new System.Windows.Forms.TextBox();
            this.rtbMainDataCode = new System.Windows.Forms.TextBox();
            this.rtbSubjects = new System.Windows.Forms.TextBox();
            this.rtbCompanys = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.cbCreditCode = new System.Windows.Forms.CheckBox();
            this.cbMainDataCode = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbSubjectCode = new System.Windows.Forms.CheckBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbMonth = new System.Windows.Forms.ComboBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbYear = new System.Windows.Forms.ComboBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTitle.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitle
            // 
            this.pnlTitle.Location = new System.Drawing.Point(1, 0);
            this.pnlTitle.Size = new System.Drawing.Size(927, 35);
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(63, 14);
            this.lblTitle.Text = "查询数据";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbCompanys);
            this.groupBox1.Controls.Add(this.rtbMainDataType);
            this.groupBox1.Controls.Add(this.cbMainDataType);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.rtbCreditCode);
            this.groupBox1.Controls.Add(this.rtbMainDataCode);
            this.groupBox1.Controls.Add(this.rtbSubjects);
            this.groupBox1.Controls.Add(this.rtbCompanys);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dateTimeEnd);
            this.groupBox1.Controls.Add(this.dateTimeStart);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbCreditCode);
            this.groupBox1.Controls.Add(this.cbMainDataCode);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbSubjectCode);
            this.groupBox1.Controls.Add(this.lblSubject);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbMonth);
            this.groupBox1.Controls.Add(this.lblYear);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbYear);
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(905, 589);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询维度";
            // 
            // cbCompanys
            // 
            this.cbCompanys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbCompanys.AutoSize = true;
            this.cbCompanys.Location = new System.Drawing.Point(32, 170);
            this.cbCompanys.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCompanys.Name = "cbCompanys";
            this.cbCompanys.Size = new System.Drawing.Size(51, 21);
            this.cbCompanys.TabIndex = 47;
            this.cbCompanys.Text = "排除";
            this.cbCompanys.UseVisualStyleBackColor = true;
            // 
            // rtbMainDataType
            // 
            this.rtbMainDataType.Location = new System.Drawing.Point(89, 494);
            this.rtbMainDataType.Multiline = true;
            this.rtbMainDataType.Name = "rtbMainDataType";
            this.rtbMainDataType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbMainDataType.Size = new System.Drawing.Size(795, 88);
            this.rtbMainDataType.TabIndex = 46;
            this.toolTip1.SetToolTip(this.rtbMainDataType, "多个社会信用证代码/税务登记证以“,”形式分隔开");
            // 
            // cbMainDataType
            // 
            this.cbMainDataType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbMainDataType.AutoSize = true;
            this.cbMainDataType.Location = new System.Drawing.Point(32, 561);
            this.cbMainDataType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMainDataType.Name = "cbMainDataType";
            this.cbMainDataType.Size = new System.Drawing.Size(51, 21);
            this.cbMainDataType.TabIndex = 45;
            this.cbMainDataType.Text = "排除";
            this.cbMainDataType.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(10, 494);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 44;
            this.label7.Text = "债权债务品类";
            // 
            // rtbCreditCode
            // 
            this.rtbCreditCode.Location = new System.Drawing.Point(89, 396);
            this.rtbCreditCode.Multiline = true;
            this.rtbCreditCode.Name = "rtbCreditCode";
            this.rtbCreditCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbCreditCode.Size = new System.Drawing.Size(795, 88);
            this.rtbCreditCode.TabIndex = 43;
            this.toolTip1.SetToolTip(this.rtbCreditCode, "多个社会信用证代码/税务登记证以“,”形式分隔开");
            this.rtbCreditCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbMainDataCode
            // 
            this.rtbMainDataCode.Location = new System.Drawing.Point(89, 298);
            this.rtbMainDataCode.Multiline = true;
            this.rtbMainDataCode.Name = "rtbMainDataCode";
            this.rtbMainDataCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbMainDataCode.Size = new System.Drawing.Size(795, 88);
            this.rtbMainDataCode.TabIndex = 42;
            this.toolTip1.SetToolTip(this.rtbMainDataCode, "多个债权债务方编码以“,”形式分隔开");
            this.rtbMainDataCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbSubjects
            // 
            this.rtbSubjects.Location = new System.Drawing.Point(89, 201);
            this.rtbSubjects.Multiline = true;
            this.rtbSubjects.Name = "rtbSubjects";
            this.rtbSubjects.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbSubjects.Size = new System.Drawing.Size(795, 88);
            this.rtbSubjects.TabIndex = 36;
            this.toolTip1.SetToolTip(this.rtbSubjects, "多个科目编码以“,”形式分隔开");
            this.rtbSubjects.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // rtbCompanys
            // 
            this.rtbCompanys.Location = new System.Drawing.Point(89, 103);
            this.rtbCompanys.Multiline = true;
            this.rtbCompanys.Name = "rtbCompanys";
            this.rtbCompanys.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbCompanys.Size = new System.Drawing.Size(795, 88);
            this.rtbCompanys.TabIndex = 35;
            this.toolTip1.SetToolTip(this.rtbCompanys, "多个公司代码以“,”形式分隔开");
            this.rtbCompanys.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbCompanys_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(475, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 41;
            this.label1.Text = "至";
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Location = new System.Drawing.Point(539, 67);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(345, 23);
            this.dateTimeEnd.TabIndex = 40;
            this.toolTip1.SetToolTip(this.dateTimeEnd, "请输入截止日期");
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.Location = new System.Drawing.Point(89, 67);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(338, 23);
            this.dateTimeStart.TabIndex = 39;
            this.toolTip1.SetToolTip(this.dateTimeStart, "请输入起始日期");
            this.dateTimeStart.Value = new System.DateTime(2004, 1, 1, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(27, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 38;
            this.label6.Text = "记账日期";
            // 
            // cbCreditCode
            // 
            this.cbCreditCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbCreditCode.AutoSize = true;
            this.cbCreditCode.Location = new System.Drawing.Point(32, 463);
            this.cbCreditCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCreditCode.Name = "cbCreditCode";
            this.cbCreditCode.Size = new System.Drawing.Size(51, 21);
            this.cbCreditCode.TabIndex = 36;
            this.cbCreditCode.Text = "排除";
            this.cbCreditCode.UseVisualStyleBackColor = true;
            // 
            // cbMainDataCode
            // 
            this.cbMainDataCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbMainDataCode.AutoSize = true;
            this.cbMainDataCode.Location = new System.Drawing.Point(32, 365);
            this.cbMainDataCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMainDataCode.Name = "cbMainDataCode";
            this.cbMainDataCode.Size = new System.Drawing.Size(51, 21);
            this.cbMainDataCode.TabIndex = 35;
            this.cbMainDataCode.Text = "排除";
            this.cbMainDataCode.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(10, 396);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 51);
            this.label5.TabIndex = 34;
            this.label5.Text = "社会信用证\r\n代码/税务登\r\n记证";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(15, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 34);
            this.label4.TabIndex = 33;
            this.label4.Text = "债权债务方\r\n编码";
            // 
            // cbSubjectCode
            // 
            this.cbSubjectCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSubjectCode.AutoSize = true;
            this.cbSubjectCode.Location = new System.Drawing.Point(32, 268);
            this.cbSubjectCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSubjectCode.Name = "cbSubjectCode";
            this.cbSubjectCode.Size = new System.Drawing.Size(51, 21);
            this.cbSubjectCode.TabIndex = 24;
            this.cbSubjectCode.Text = "排除";
            this.cbSubjectCode.UseVisualStyleBackColor = true;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSubject.Location = new System.Drawing.Point(27, 204);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(56, 17);
            this.lblSubject.TabIndex = 10;
            this.lblSubject.Text = "科目编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(27, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "公司代码";
            // 
            // cbMonth
            // 
            this.cbMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMonth.FormattingEnabled = true;
            this.cbMonth.ItemHeight = 17;
            this.cbMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cbMonth.Location = new System.Drawing.Point(539, 27);
            this.cbMonth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMonth.Name = "cbMonth";
            this.cbMonth.Size = new System.Drawing.Size(345, 25);
            this.cbMonth.TabIndex = 1;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblYear.Location = new System.Drawing.Point(27, 30);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(56, 17);
            this.lblYear.TabIndex = 1;
            this.lblYear.Text = "选择年份";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(477, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "选择月份";
            // 
            // cbYear
            // 
            this.cbYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYear.FormattingEnabled = true;
            this.cbYear.ItemHeight = 17;
            this.cbYear.Location = new System.Drawing.Point(89, 27);
            this.cbYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbYear.Name = "cbYear";
            this.cbYear.Size = new System.Drawing.Size(338, 25);
            this.cbYear.TabIndex = 0;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(716, 637);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(87, 33);
            this.btnQuery.TabIndex = 14;
            this.btnQuery.Text = "查询(&Q)";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(809, 637);
            this.Cancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(87, 33);
            this.Cancel.TabIndex = 15;
            this.Cancel.Text = "取消(&C)";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmPayAccount
            // 
            this.AcceptButton = this.btnQuery;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(929, 676);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.btnQuery);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "frmPayAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountForm";
            this.Load += new System.EventHandler(this.AccountForm_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.btnQuery, 0);
            this.Controls.SetChildIndex(this.Cancel, 0);
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbSubjectCode;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbMonth;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbYear;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbCreditCode;
        private System.Windows.Forms.CheckBox cbMainDataCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.TextBox rtbCreditCode;
        private System.Windows.Forms.TextBox rtbMainDataCode;
        private System.Windows.Forms.TextBox rtbSubjects;
        private System.Windows.Forms.TextBox rtbCompanys;
        private System.Windows.Forms.TextBox rtbMainDataType;
        private System.Windows.Forms.CheckBox cbMainDataType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbCompanys;
    }
}