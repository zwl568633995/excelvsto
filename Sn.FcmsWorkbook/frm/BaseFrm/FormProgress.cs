﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sn.FcmsWorkbook
{
    public partial class FormProgress : BaseForm
    {
        public IProgress<bool> Progress { get; set; }

        public FormProgress()
        {
            InitializeComponent();
        }

        public FormProgress(Progress<bool> progress)
        {
            Progress = progress;
            progress.ProgressChanged += Progress_ProgressChanged;
            InitializeComponent();
        }

        private void Progress_ProgressChanged(object sender, bool e)
        {
        }
    }
}