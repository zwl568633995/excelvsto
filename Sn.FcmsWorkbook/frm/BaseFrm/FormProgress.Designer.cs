﻿namespace Sn.FcmsWorkbook
{
    partial class FormProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProgress));
            this.ibox_progress = new System.Windows.Forms.PictureBox();
            this.pnlTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibox_progress)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitle
            // 
            this.pnlTitle.Location = new System.Drawing.Point(1, 0);
            this.pnlTitle.Size = new System.Drawing.Size(160, 30);
            this.pnlTitle.UseWaitCursor = true;
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTitle.Location = new System.Drawing.Point(11, 9);
            this.lblTitle.Size = new System.Drawing.Size(53, 17);
            this.lblTitle.Text = "加载中...";
            this.lblTitle.UseWaitCursor = true;
            // 
            // ibox_progress
            // 
            this.ibox_progress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ibox_progress.Image = ((System.Drawing.Image)(resources.GetObject("ibox_progress.Image")));
            this.ibox_progress.Location = new System.Drawing.Point(1, 30);
            this.ibox_progress.Name = "ibox_progress";
            this.ibox_progress.Size = new System.Drawing.Size(160, 133);
            this.ibox_progress.TabIndex = 34;
            this.ibox_progress.TabStop = false;
            this.ibox_progress.UseWaitCursor = true;
            // 
            // FormProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(162, 164);
            this.ControlBox = false;
            this.Controls.Add(this.ibox_progress);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProgress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "处理中，请稍后";
            this.UseWaitCursor = true;
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.Controls.SetChildIndex(this.ibox_progress, 0);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibox_progress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ibox_progress;
    }
}