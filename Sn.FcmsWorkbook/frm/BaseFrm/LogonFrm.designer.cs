﻿namespace Sn.FcmsWorkbook
{
    partial class LogonFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogonFrm));
            this.lblUserId = new System.Windows.Forms.Label();
            this.tbUserId = new System.Windows.Forms.TextBox();
            this.lblPassport = new System.Windows.Forms.Label();
            this.tbPassport = new System.Windows.Forms.TextBox();
            this.btnLogOn = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblLogin = new System.Windows.Forms.Label();
            this.pnlTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitle
            // 
            this.pnlTitle.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pnlTitle.Location = new System.Drawing.Point(1, 0);
            this.pnlTitle.Size = new System.Drawing.Size(294, 33);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTitle.Size = new System.Drawing.Size(41, 12);
            this.lblTitle.Text = "请登录";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblUserId.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUserId.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblUserId.Location = new System.Drawing.Point(27, 83);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(61, 19);
            this.lblUserId.TabIndex = 0;
            this.lblUserId.Text = "用户工号";
            // 
            // tbUserId
            // 
            this.tbUserId.Location = new System.Drawing.Point(94, 83);
            this.tbUserId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbUserId.Name = "tbUserId";
            this.tbUserId.Size = new System.Drawing.Size(168, 23);
            this.tbUserId.TabIndex = 1;
            // 
            // lblPassport
            // 
            this.lblPassport.AutoSize = true;
            this.lblPassport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblPassport.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPassport.Location = new System.Drawing.Point(27, 157);
            this.lblPassport.Name = "lblPassport";
            this.lblPassport.Size = new System.Drawing.Size(61, 19);
            this.lblPassport.TabIndex = 2;
            this.lblPassport.Text = "登录密码";
            // 
            // tbPassport
            // 
            this.tbPassport.Location = new System.Drawing.Point(94, 155);
            this.tbPassport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPassport.Name = "tbPassport";
            this.tbPassport.PasswordChar = '*';
            this.tbPassport.Size = new System.Drawing.Size(168, 23);
            this.tbPassport.TabIndex = 3;
            // 
            // btnLogOn
            // 
            this.btnLogOn.Location = new System.Drawing.Point(47, 235);
            this.btnLogOn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLogOn.Name = "btnLogOn";
            this.btnLogOn.Size = new System.Drawing.Size(87, 33);
            this.btnLogOn.TabIndex = 4;
            this.btnLogOn.Text = "登录";
            this.btnLogOn.UseVisualStyleBackColor = true;
            this.btnLogOn.Click += new System.EventHandler(this.btnLogOn_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(161, 235);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblLogin.Location = new System.Drawing.Point(90, 111);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(10, 23);
            this.lblLogin.TabIndex = 35;
            this.lblLogin.Text = "\r\n";
            // 
            // LogonFrm
            // 
            this.AcceptButton = this.btnLogOn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(296, 326);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLogOn);
            this.Controls.Add(this.tbPassport);
            this.Controls.Add(this.lblPassport);
            this.Controls.Add(this.tbUserId);
            this.Controls.Add(this.lblUserId);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "LogonFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "登录菜单";
            this.Load += new System.EventHandler(this.LogonFrm_Load);
            this.Controls.SetChildIndex(this.lblUserId, 0);
            this.Controls.SetChildIndex(this.tbUserId, 0);
            this.Controls.SetChildIndex(this.lblPassport, 0);
            this.Controls.SetChildIndex(this.tbPassport, 0);
            this.Controls.SetChildIndex(this.btnLogOn, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.lblLogin, 0);
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.TextBox tbUserId;
        private System.Windows.Forms.Label lblPassport;
        private System.Windows.Forms.TextBox tbPassport;
        private System.Windows.Forms.Button btnLogOn;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblLogin;
    }
}