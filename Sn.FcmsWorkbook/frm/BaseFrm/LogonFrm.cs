﻿using System;
using System.Windows.Forms;
using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;

namespace Sn.FcmsWorkbook
{
    public partial class LogonFrm : BaseForm
    {
        private readonly string _clientVersion;
        private readonly ClientType _clientType;
        private ProductType queryType;
       

        public LogonFrm()
        {
            InitializeComponent();
        }

        public LogonFrm(string clientVersion, ClientType clientType, ProductType queryType)
        {
            _clientVersion = clientVersion;
            _clientType = clientType;
            this.queryType = queryType;

            InitializeComponent();           
        }

        private async void btnLogOn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.tbUserId.Text) ){
                MessageBox.Show("请输入用户名");
                return;
            }
            if (string.IsNullOrEmpty(this.tbPassport.Text))
            {
                MessageBox.Show("请输入密码");
                return;
            }

            ShowLogin(true);
            var userId = this.tbUserId.Text.Trim();
            var passport = this.tbPassport.Text.Trim();
            var result = await UserServiceFlow.Logon(userId, passport, _clientVersion, _clientType,queryType);
            if (!result.Result)
            {
                MessageBox.Show(result.Message);
                ShowLogin(false);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void ShowLogin(bool bIsOk)
        {
            if (bIsOk)
            {
                lblUserId.Hide();
                lblPassport.Hide();
                tbUserId.Hide();
                tbPassport.Hide();
                btnLogOn.Hide();
                btnCancel.Hide();
                lblLogin.Text = "用户" + this.tbUserId.Text.Trim()+Environment.NewLine+" 正在登陆.....";
            }
            else
            {
                lblUserId.Show();
                lblPassport.Show();
                tbUserId.Show();
                tbPassport.Show();
                btnLogOn.Show();
                btnCancel.Show();
                lblLogin.Text = "";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void LogonFrm_Load(object sender, EventArgs e)
        {

        }
    }
}
