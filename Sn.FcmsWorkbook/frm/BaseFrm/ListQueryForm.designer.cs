﻿namespace Sn.FcmsWorkbook
{
    partial class ListQueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListQueryForm));
            this.lblSelectOrg = new System.Windows.Forms.Label();
            this.cbOrgans = new System.Windows.Forms.ComboBox();
            this.lblSlectYear = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbYear = new System.Windows.Forms.ComboBox();
            this.lblSelectMonth = new System.Windows.Forms.Label();
            this.cbMonth = new System.Windows.Forms.ComboBox();
            this.pnlTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitle
            // 
            this.pnlTitle.Location = new System.Drawing.Point(1, 0);
            this.pnlTitle.Size = new System.Drawing.Size(467, 38);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTitle.Size = new System.Drawing.Size(80, 17);
            this.lblTitle.Text = "清单查询选择";
            // 
            // lblSelectOrg
            // 
            this.lblSelectOrg.AutoSize = true;
            this.lblSelectOrg.Location = new System.Drawing.Point(60, 88);
            this.lblSelectOrg.Name = "lblSelectOrg";
            this.lblSelectOrg.Size = new System.Drawing.Size(56, 17);
            this.lblSelectOrg.TabIndex = 0;
            this.lblSelectOrg.Text = "选择公司";
            // 
            // cbOrgans
            // 
            this.cbOrgans.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbOrgans.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbOrgans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrgans.FormattingEnabled = true;
            this.cbOrgans.Location = new System.Drawing.Point(141, 85);
            this.cbOrgans.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbOrgans.Name = "cbOrgans";
            this.cbOrgans.Size = new System.Drawing.Size(244, 25);
            this.cbOrgans.TabIndex = 1;
            // 
            // lblSlectYear
            // 
            this.lblSlectYear.AutoSize = true;
            this.lblSlectYear.Location = new System.Drawing.Point(36, 145);
            this.lblSlectYear.Name = "lblSlectYear";
            this.lblSlectYear.Size = new System.Drawing.Size(80, 17);
            this.lblSlectYear.TabIndex = 2;
            this.lblSlectYear.Text = "选择会计年度";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(141, 256);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(87, 33);
            this.btnConfirm.TabIndex = 4;
            this.btnConfirm.Text = "开始查询";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(259, 256);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "取消查询";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbYear
            // 
            this.cbYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYear.FormattingEnabled = true;
            this.cbYear.Location = new System.Drawing.Point(141, 142);
            this.cbYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbYear.Name = "cbYear";
            this.cbYear.Size = new System.Drawing.Size(244, 25);
            this.cbYear.TabIndex = 6;
            // 
            // lblSelectMonth
            // 
            this.lblSelectMonth.AutoSize = true;
            this.lblSelectMonth.Location = new System.Drawing.Point(60, 202);
            this.lblSelectMonth.Name = "lblSelectMonth";
            this.lblSelectMonth.Size = new System.Drawing.Size(56, 17);
            this.lblSelectMonth.TabIndex = 7;
            this.lblSelectMonth.Text = "选择月份";
            // 
            // cbMonth
            // 
            this.cbMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMonth.FormattingEnabled = true;
            this.cbMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cbMonth.Location = new System.Drawing.Point(141, 199);
            this.cbMonth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMonth.Name = "cbMonth";
            this.cbMonth.Size = new System.Drawing.Size(244, 25);
            this.cbMonth.TabIndex = 8;
            // 
            // ListQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 344);
            this.Controls.Add(this.cbMonth);
            this.Controls.Add(this.lblSelectMonth);
            this.Controls.Add(this.cbYear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.lblSlectYear);
            this.Controls.Add(this.cbOrgans);
            this.Controls.Add(this.lblSelectOrg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "ListQueryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "清单查询选择";
            this.Controls.SetChildIndex(this.lblSelectOrg, 0);
            this.Controls.SetChildIndex(this.cbOrgans, 0);
            this.Controls.SetChildIndex(this.lblSlectYear, 0);
            this.Controls.SetChildIndex(this.btnConfirm, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.cbYear, 0);
            this.Controls.SetChildIndex(this.lblSelectMonth, 0);
            this.Controls.SetChildIndex(this.cbMonth, 0);
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSelectOrg;
        private System.Windows.Forms.ComboBox cbOrgans;
        private System.Windows.Forms.Label lblSlectYear;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cbYear;
        private System.Windows.Forms.Label lblSelectMonth;
        private System.Windows.Forms.ComboBox cbMonth;
    }
}