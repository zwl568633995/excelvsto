﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Sn.Fcms.Infrastructure;

namespace Sn.FcmsWorkbook
{
    public partial class ListQueryForm : BaseForm
    {

        public ListQueryForm()
        {
            InitializeComponent();
        }

        public ListQueryForm(string sheetId, Worksheet workSheet)
        {
            InitializeComponent();
            InitialControlData();
        }

        private void InitialControlData()
        {
            //cbOrgans.DataSource = new BindingSource();
            //var orgs = UserSession.Organizations.Where(x => !x.OrgCode.StartsWith("R")).ToArray();
            //cbOrgans.DataSource = orgs;
            //cbOrgans.DisplayMember = "CombineName";
            //cbOrgans.ValueMember = "OrgCode";

            //int year = DateTime.Now.Year;

            //for (int i = 0; i < 10; i++)
            //{
            //    cbYear.Items.Add((year - i).ToString());
            //}

            //bool queryed = false;

            //string selectedOrgan = Utils.GetCurrentOrDefaultOrgan(_sheetId, ref queryed);
            //if (!string.IsNullOrEmpty(selectedOrgan))
            //    cbOrgans.SelectedValue = selectedOrgan;

            //string selectedMonth = Utils.GetCurrentOrDefaultMonth(_sheetId, ref queryed);
            //if (!string.IsNullOrEmpty(selectedMonth) && selectedMonth.Length == 6)
            //{
            //    cbYear.SelectedItem = selectedMonth.Substring(0, 4);
            //    cbMonth.SelectedItem = selectedMonth.Substring(4, 2);
            //}
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (cbOrgans.SelectedValue == null)
            {
                MessageBox.Show("请选择组织！");
                return;
            }

            if (cbYear.SelectedItem == null) {

                MessageBox.Show("请选择年度！");
                return;    
            }

            if (cbMonth.SelectedItem == null) {

                MessageBox.Show("请选择月份！");
                return;
            }

            var organCode = cbOrgans.SelectedValue?.ToString();
            string month = cbYear.SelectedItem + cbMonth.SelectedItem.ToString();

            //var result = await UiFuctions.BindListData(_sheetId, organCode, month, _worksheet);

            //if (result)
            //{
            //    DialogResult = DialogResult.OK;
            //    this.Close();
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
