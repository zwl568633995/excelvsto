﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;

namespace Sn.FcmsWorkbook
{
    public partial class frmOtherDataVerSubject : BaseForm
    {
        #region 自定义的成员变量
        private PayDataVerificationRequest m_PayDataVerificationRequest;

        public PayDataVerificationRequest PayDataVerificationRequest
        {
            get
            {
                return m_PayDataVerificationRequest;
            }

            set
            {
                m_PayDataVerificationRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmOtherDataVerSubject()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体事件
        /// <summary>
        /// 窗体加载
        /// </summary>
        private void frmQueryData_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "(" + this.Tag + ")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbeYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        /// <summary>
        /// 设置查询参数
        /// </summary>
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_PayDataVerificationRequest = new PayDataVerificationRequest();
            m_PayDataVerificationRequest.UserId = UserSession.UserId;
            m_PayDataVerificationRequest.TokenId = UserSession.TokenId;
            m_PayDataVerificationRequest.Month = cbeYear.Text + cbeMonth.Text;
            string[] sOrgCode= ToDBC(rtbCompanys.Text,cbCompanys.Checked).Split(',');
            m_PayDataVerificationRequest.OrgCode = (from a in sOrgCode
                                                where !string.IsNullOrEmpty(a)
                                                select a).ToArray();
            m_PayDataVerificationRequest.StartIndex = 1;
            m_PayDataVerificationRequest.ReportId = "NO10";
            string sPageCount =Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_PayDataVerificationRequest.CountPerPage =  Convert.ToInt32(sPageCount);
            SaveFormInfo();
            this.Hide();
            await QueryData.BinderData<PayDataVerSubjectItem, PayDataVerificationRequest, PayDataVerSujectSumItem>(m_PayDataVerificationRequest,true);
            this.Close();
        }

        /// <summary>
        /// 实现全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbeYear.Text))
            {
                ShowError(cbeYear, "所选年份为空。");
                return false;
            }

            if (string.IsNullOrEmpty(cbeMonth.Text))
            {
                ShowError(cbeMonth, "所选月份为空。");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput, bool bRemove)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格
            sInput = bRemove ? ("1," + sInput) : ("0," + sInput);
            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 保存缓存信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo = new OtherDataVerSubjectInfo
            {
                Year = cbeYear.Text.ToString(),
                Month = cbeMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),
                IsRemoveCompanys=cbCompanys.Checked,
                PayDataVerificationRequest = m_PayDataVerificationRequest
            };

        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo != null)
            {
                cbeYear.Text = Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo.Year.ToString();
                cbeMonth.Text = Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo.Month.ToString();
                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo.OrgCode.ToString();
                cbCompanys.Checked = Globals.Ribbons.FcmsRibbon.OtherDataVerSubjectInfo.IsRemoveCompanys;
            }
        }
        #endregion
    }
}

