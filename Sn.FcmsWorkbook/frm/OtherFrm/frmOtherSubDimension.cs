﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace Sn.FcmsWorkbook
{
    public partial class frmOtherSubDimension : BaseForm
    {
        #region 自定义的成员变量
        /// <summary>
        /// 当前请求
        /// </summary>
        private PaySubDimensionRequest m_PaySubDimensionRequest;

        public PaySubDimensionRequest PaySubDimensionRequest
        {
            get
            {
                return m_PaySubDimensionRequest;
            }

            set
            {
                m_PaySubDimensionRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmOtherSubDimension()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体事件
        /// <summary>
        /// 窗体加载
        /// </summary>
        private void frmQueryData_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "(" + this.Tag + ")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        /// <summary>
        /// 设置查询参数
        /// </summary>
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_PaySubDimensionRequest = new PaySubDimensionRequest();
            m_PaySubDimensionRequest.UserId = UserSession.UserId;
            m_PaySubDimensionRequest.TokenId = UserSession.TokenId;
            m_PaySubDimensionRequest.Month = cbYear.Text + cbMonth.Text;
            string[] sOrgCode = ToDBC(rtbCompanys.Text,cbComapanys.Checked).Split(',');
            m_PaySubDimensionRequest.OrgCode = (from a in sOrgCode
                                                 where !string.IsNullOrEmpty(a)
                                                 select a).ToArray();
            m_PaySubDimensionRequest.Subjiect= string.IsNullOrEmpty(rtbSubjects.Text) ? new string[] { } : ToDBC(rtbSubjects.Text,cbSubjectCode.Checked).Split(',');
            m_PaySubDimensionRequest.MainData= string.IsNullOrEmpty(rtbMainDataCode.Text) ? new string[] { } : ToDBC(rtbMainDataCode.Text,cbMainDataCode.Checked).Split(',');
            m_PaySubDimensionRequest.Credit= string.IsNullOrEmpty(rtbCreditCode.Text) ? new string[] { } : ToDBC(rtbCreditCode.Text,cbCreditCode.Checked).Split(',');
            m_PaySubDimensionRequest.MainDataType = string.IsNullOrEmpty(rtbMainDataType.Text) ? new string[] { } : ToDBC(rtbMainDataType.Text, cbMainDataType.Checked).Split(',');
            m_PaySubDimensionRequest.StartIndex = 1;
            m_PaySubDimensionRequest.ReportId = "NO12";
            string PageSize = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_PaySubDimensionRequest.CountPerPage =  Convert.ToInt32(PageSize);
            SaveFormInfo();
            this.Hide();
            await QueryData.BinderData<OtherSubDimensionItem, PaySubDimensionRequest, OtherSubDimensionSumItem>(m_PaySubDimensionRequest, true);
            this.Close();
        }


        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbYear.Text))
            {
                ShowError(cbYear, "所选年份为空");
                return false;
            }

            if (string.IsNullOrEmpty(cbMonth.Text))
            {
                ShowError(cbMonth, "所选月份为空");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput,bool bRemove)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格
            sInput = bRemove ? ("1," + sInput) : ("0," + sInput);
            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 保存查询信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo = new OtherSubDimensionInfo
            {
                Year = cbYear.Text.ToString(),
                Month = cbMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),

                MainDataCode = string.IsNullOrEmpty(rtbMainDataCode.Text) ? string.Empty : rtbMainDataCode.Text.ToString(),
                CreditCode = string.IsNullOrEmpty(rtbCreditCode.Text) ? string.Empty : rtbCreditCode.Text.ToString(),
                Subjects = string.IsNullOrEmpty(rtbSubjects.Text) ? string.Empty : rtbSubjects.Text.ToString(),
                MainDataType= string.IsNullOrEmpty(rtbMainDataType.Text) ? string.Empty : rtbMainDataType.Text.ToString(),

                IsRemoveSubjects = cbSubjectCode.Checked,
                IsRemoveCreditCode = cbCreditCode.Checked,
                IsRemoveMainDataCode = cbMainDataCode.Checked,
                IsRemoveCompanys=cbComapanys.Checked,
                IsRemoveMainDataType=cbMainDataType.Checked,

                PaySubDimensionRequest = m_PaySubDimensionRequest
            };
        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo != null)
            {
                cbYear.Text = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.Year.ToString();
                cbMonth.Text = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.Month.ToString();

                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.OrgCode.ToString();
                rtbMainDataCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.MainDataCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.MainDataCode.ToString();
                rtbCreditCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.CreditCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.CreditCode.ToString();
                rtbSubjects.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.Subjects) ? string.Empty : Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.Subjects.ToString();
                rtbMainDataType.Text= string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.MainDataType) ? string.Empty : Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.MainDataType.ToString();

                cbComapanys.Checked= Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.IsRemoveCompanys;
                cbCreditCode.Checked = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.IsRemoveCreditCode;
                cbMainDataCode.Checked = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.IsRemoveMainDataCode;
                cbSubjectCode.Checked = Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.IsRemoveSubjects;
                cbMainDataType.Checked= Globals.Ribbons.FcmsRibbon.OtherSubDimensionInfo.IsRemoveMainDataType;
            }
        }
        #endregion
    }
}

