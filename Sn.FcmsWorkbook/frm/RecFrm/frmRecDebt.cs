﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;

namespace Sn.FcmsWorkbook
{
    public partial class frmRecDebt : BaseForm
    {
        #region 自定义的成员变量
        private DebtTableRequest m_DebtTableRequest;

        public DebtTableRequest DebtTableRequest
        {
            get
            {
                return m_DebtTableRequest;
            }

            set
            {
                m_DebtTableRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmRecDebt()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体事件
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_DebtTableRequest = new DebtTableRequest();
            m_DebtTableRequest.UserId = UserSession.UserId;
            m_DebtTableRequest.TokenId = UserSession.TokenId;
            m_DebtTableRequest.Month = cbYear.Text + cbMonth.Text;
            string[] sOrgCode = ToDBC(rtbCompanys.Text).Split(',');
            m_DebtTableRequest.OrgCode = (from a in sOrgCode
                                          where !string.IsNullOrEmpty(a)
                                          select a).ToArray();
            m_DebtTableRequest.MainData = string.IsNullOrEmpty(rtbMainDataCode.Text) ? new string[] { } : ToDBC(rtbMainDataCode.Text, cbMainDataCode.Checked).Split(',');
            m_DebtTableRequest.Credit = string.IsNullOrEmpty(rtbCreditCode.Text) ? new string[] { } : ToDBC(rtbCreditCode.Text, cbCreditCode.Checked).Split(',');
            m_DebtTableRequest.StartIndex = 1;
            m_DebtTableRequest.ReportId = "NO3";
            string sPageCount = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_DebtTableRequest.CountPerPage = Convert.ToInt32(sPageCount);
            SaveFormInfo();
            this.Hide();
            await QueryData.BinderData<DebtTableItem, DebtTableRequest, DebtTableSumItem>(m_DebtTableRequest, true);
            this.Close();
        }

        private void DebtForm_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "(" + this.Tag + ")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbYear.Text))
            {
                ShowError(cbYear, "所选年份为空");
                return false;
            }

            if (string.IsNullOrEmpty(cbMonth.Text))
            {
                ShowError(cbMonth, "所选月份为空");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput, bool bRemove)
        {
            sInput = sInput.Replace("\r", ""); //删除\r
            sInput = sInput.Replace("\n", "");//删除\n
            sInput = sInput.Replace("\t", "");//删除\t
            sInput = sInput.Replace(" ", "");//删除空格

            sInput = bRemove ? ("1," + sInput) : ("0," + sInput);
            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 保存缓存信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.DebtInfo = new DebtInfo
            {
                Year = cbYear.Text.ToString(),
                Month = cbMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),
                MainDataCode = rtbMainDataCode.Text.ToString(),
                CreditCode = rtbCreditCode.Text.ToString(),
                IsRemoveCreditCode = cbCreditCode.Checked,
                IsRemoveMainDataCode = cbMainDataCode.Checked,
                DebtTableRequest = m_DebtTableRequest
            };
        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.DebtInfo != null)
            {
                cbYear.Text = Globals.Ribbons.FcmsRibbon.DebtInfo.Year.ToString();
                cbMonth.Text = Globals.Ribbons.FcmsRibbon.DebtInfo.Month.ToString();
                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.DebtInfo.OrgCode.ToString();
                rtbMainDataCode.Text = Globals.Ribbons.FcmsRibbon.DebtInfo.MainDataCode.ToString();
                rtbCreditCode.Text = Globals.Ribbons.FcmsRibbon.DebtInfo.CreditCode.ToString();
                cbCreditCode.Checked = Globals.Ribbons.FcmsRibbon.DebtInfo.IsRemoveCreditCode;
                cbMainDataCode.Checked = Globals.Ribbons.FcmsRibbon.DebtInfo.IsRemoveMainDataCode;
            }
        }
        #endregion
    }
}
