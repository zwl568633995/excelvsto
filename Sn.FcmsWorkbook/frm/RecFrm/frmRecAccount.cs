﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;

namespace Sn.FcmsWorkbook
{

    public partial class frmRecAccount : BaseForm
    {
        #region 自定义的成员变量

        private AccountTableRequest m_AccountTableRequest;

        public AccountTableRequest AccountTableRequest
        {
            get
            {
                return m_AccountTableRequest;
            }

            set
            {
                m_AccountTableRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmRecAccount()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体函数
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_AccountTableRequest = new AccountTableRequest();
            m_AccountTableRequest.UserId = UserSession.UserId;
            m_AccountTableRequest.TokenId = UserSession.TokenId;
            m_AccountTableRequest.Month = cbYear.Text + cbMonth.Text;
            string[] sOrgCode = ToDBC(rtbCompanys.Text).Split(',');
            m_AccountTableRequest.OrgCode = (from a in sOrgCode
                                             where !string.IsNullOrEmpty(a)
                                             select a).ToArray();
            m_AccountTableRequest.Subjiect = string.IsNullOrEmpty(rtbSubjects.Text) ? new string[] { } : ToDBC(rtbSubjects.Text, cbSubjectCode.Checked).Split(',');
            m_AccountTableRequest.MainData = string.IsNullOrEmpty(rtbMainDataCode.Text) ? new string[] { } : ToDBC(rtbMainDataCode.Text, cbMainDataCode.Checked).Split(',');
            m_AccountTableRequest.Credit = string.IsNullOrEmpty(rtbCreditCode.Text) ? new string[] { } : ToDBC(rtbCreditCode.Text, cbCreditCode.Checked).Split(',');
            m_AccountTableRequest.AccountDate = dateTimeStart.Value.ToString("yyyyMMdd") + "," + dateTimeEnd.Value.ToString("yyyyMMdd");
            m_AccountTableRequest.StartIndex = 1;
            m_AccountTableRequest.ReportId = "NO4";
            string sPageCount = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_AccountTableRequest.CountPerPage = Convert.ToInt32(sPageCount);
            SaveFormInfo();
            this.Hide();
            await QueryData.BinderData<AccountTableItem, AccountTableRequest, AccountTableSumItem>(m_AccountTableRequest, true);
            this.Close();
        }

        private void AccountForm_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "(" + this.Tag + ")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbYear.Text))
            {
                ShowError(cbYear, "所选年份为空。");
                return false;
            }

            if (string.IsNullOrEmpty(cbMonth.Text))
            {
                ShowError(cbMonth, "所选月份为空。");
                return false;
            }

            DateTime cbDate = DateTime.Parse(cbYear.Text + "/" + cbMonth.Text);
            DateTime AccoutDateEnd = DateTime.Parse(dateTimeEnd.Value.ToString("yyyy-MM-dd"));
            DateTime AccoutDateStart = DateTime.Parse(dateTimeStart.Value.ToString("yyyy-MM-dd"));
            if (AccoutDateStart > AccoutDateEnd)
            {
                MessageBox.Show("起始日期不能大于终止日期", "警告");
                return false;
            }

            if ((AccoutDateEnd.Year > cbDate.Year) || (AccoutDateEnd.Year <= cbDate.Year && AccoutDateEnd.Month > cbDate.Month))
            {
                MessageBox.Show("记账日期不得大于查询日期", "警告");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput, bool bRemove)
        {
            sInput = sInput.Replace("\r", ""); //删除\r
            sInput = sInput.Replace("\n", "");//删除\n
            sInput = sInput.Replace("\t", "");//删除\t
            sInput = sInput.Replace(" ", "");//删除空格

            sInput = bRemove ? ("1," + sInput) : ("0," + sInput);
            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 保存缓存信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.AccountInfo = new AccountInfo
            {
                Year = cbYear.Text.ToString(),
                Month = cbMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),

                MainDataCode = string.IsNullOrEmpty(rtbMainDataCode.Text) ? string.Empty : rtbMainDataCode.Text.ToString(),
                CreditCode = string.IsNullOrEmpty(rtbCreditCode.Text) ? string.Empty : rtbCreditCode.Text.ToString(),
                Subjects = string.IsNullOrEmpty(rtbSubjects.Text) ? string.Empty : rtbSubjects.Text.ToString(),

                IsRemoveSubjects = cbSubjectCode.Checked,
                IsRemoveCreditCode = cbCreditCode.Checked,
                IsRemoveMainDataCode = cbMainDataCode.Checked,
                AccountTableRequest = m_AccountTableRequest,
                AccountDateFrom = dateTimeStart.Text.ToString(),
                AccountDateTo = dateTimeEnd.Text.ToString()
            };
        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.AccountInfo != null)
            {
                cbYear.Text = Globals.Ribbons.FcmsRibbon.AccountInfo.Year.ToString();
                cbMonth.Text = Globals.Ribbons.FcmsRibbon.AccountInfo.Month.ToString();
                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.AccountInfo.OrgCode.ToString();
                rtbMainDataCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.AccountInfo.MainDataCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.AccountInfo.MainDataCode.ToString();
                rtbCreditCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.AccountInfo.CreditCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.AccountInfo.CreditCode.ToString();
                rtbSubjects.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.AccountInfo.Subjects) ? string.Empty : Globals.Ribbons.FcmsRibbon.AccountInfo.Subjects.ToString();
                cbCreditCode.Checked = Globals.Ribbons.FcmsRibbon.AccountInfo.IsRemoveCreditCode;
                cbMainDataCode.Checked = Globals.Ribbons.FcmsRibbon.AccountInfo.IsRemoveMainDataCode;
                cbSubjectCode.Checked = Globals.Ribbons.FcmsRibbon.AccountInfo.IsRemoveSubjects;
                dateTimeStart.Text = Globals.Ribbons.FcmsRibbon.AccountInfo.AccountDateFrom.ToString();
                dateTimeEnd.Text = Globals.Ribbons.FcmsRibbon.AccountInfo.AccountDateTo.ToString();
            }
        }
        #endregion
    }
}
