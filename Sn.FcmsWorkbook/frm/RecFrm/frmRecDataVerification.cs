﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;

namespace Sn.FcmsWorkbook
{
    public partial class frmRecDataVerification : BaseForm
    {
        #region 自定义的成员变量
        private DataVerificationRequest m_DataVerificationRequest;

        public DataVerificationRequest DataVerificationRequest
        {
            get
            {
                return m_DataVerificationRequest;
            }

            set
            {
                m_DataVerificationRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmRecDataVerification()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体事件
        /// <summary>
        /// 窗体加载
        /// </summary>
        private void frmQueryData_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "("+this.Tag+")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbeYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        /// <summary>
        /// 设置查询参数
        /// </summary>
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_DataVerificationRequest = new DataVerificationRequest();
            m_DataVerificationRequest.UserId = UserSession.UserId;
            m_DataVerificationRequest.TokenId = UserSession.TokenId;
            m_DataVerificationRequest.Month = cbeYear.Text + cbeMonth.Text;
            string[] sOrgCode= ToDBC(rtbCompanys.Text).Split(',');
            m_DataVerificationRequest.OrgCode = (from a in sOrgCode
                                                where !string.IsNullOrEmpty(a)
                                                select a).ToArray();
            m_DataVerificationRequest.StartIndex = 1;
            m_DataVerificationRequest.ReportId = "NO1";
            string sPageCount =Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_DataVerificationRequest.CountPerPage =  Convert.ToInt32(sPageCount);
            SaveFormInfo();
            this.Hide();
            await QueryData.BinderData<DataVerificationItem, DataVerificationRequest, DataVerificationSumItem>(m_DataVerificationRequest,true);
            this.Close();
        }

        /// <summary>
        /// 实现全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbeYear.Text))
            {
                ShowError(cbeYear, "所选年份为空。");
                return false;
            }

            if (string.IsNullOrEmpty(cbeMonth.Text))
            {
                ShowError(cbeMonth, "所选月份为空。");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 保存缓存信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.DataVerificationInfo = new DataVerificationInfo
            {
                Year = cbeYear.Text.ToString(),
                Month = cbeMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),
                DataVerificationRequest = m_DataVerificationRequest
            };

        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.DataVerificationInfo != null)
            {
                cbeYear.Text = Globals.Ribbons.FcmsRibbon.DataVerificationInfo.Year.ToString();
                cbeMonth.Text = Globals.Ribbons.FcmsRibbon.DataVerificationInfo.Month.ToString();
                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.DataVerificationInfo.OrgCode.ToString();
            }
        }
        #endregion
    }
}

