﻿using Sn.Fcms.Infrastructure;
using Sn.Fcms.Service;
using System;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using System.Text;

namespace Sn.FcmsWorkbook
{
    public partial class frmRecSubDimension : BaseForm
    {
        #region 自定义的成员变量
        /// <summary>
        /// 当前请求
        /// </summary>
        private SubDimensionRequest m_SubDimensionRequest;

        public SubDimensionRequest SubDimensionRequest
        {
            get
            {
                return m_SubDimensionRequest;
            }

            set
            {
                m_SubDimensionRequest = value;
            }
        }
        #endregion

        #region 构造函数
        public frmRecSubDimension()
        {
            InitializeComponent();
        }
        #endregion

        #region 窗体事件
        /// <summary>
        /// 窗体加载
        /// </summary>
        private void frmQueryData_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text += "(" + this.Tag + ")";
            errorProvider1.Clear();
            ////绑定年份
            int year = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbYear.Items.Add((year - i).ToString());
            }

            SetFormInfo();
        }

        /// <summary>
        /// 设置查询参数
        /// </summary>
        private async void btnQuery_Click(object sender, EventArgs e)
        {
            if (!CheckParasms())
                return;

            m_SubDimensionRequest = new SubDimensionRequest();
            m_SubDimensionRequest.UserId = UserSession.UserId;
            m_SubDimensionRequest.TokenId = UserSession.TokenId;
            m_SubDimensionRequest.Month = cbYear.Text + cbMonth.Text;
            string[] sOrgCode = ToDBC(rtbCompanys.Text).Split(',');
            m_SubDimensionRequest.OrgCode = (from a in sOrgCode
                                                 where !string.IsNullOrEmpty(a)
                                                 select a).ToArray();
            m_SubDimensionRequest.Subjiect= string.IsNullOrEmpty(rtbSubjects.Text) ? new string[] { } : ToDBC(rtbSubjects.Text,cbSubjectCode.Checked).Split(',');
            m_SubDimensionRequest.MainData= string.IsNullOrEmpty(rtbMainDataCode.Text) ? new string[] { } : ToDBC(rtbMainDataCode.Text,cbMainDataCode.Checked).Split(',');
            m_SubDimensionRequest.Credit= string.IsNullOrEmpty(rtbCreditCode.Text) ? new string[] { } : ToDBC(rtbCreditCode.Text,cbCreditCode.Checked).Split(',');
            m_SubDimensionRequest.StartIndex = 1;
            m_SubDimensionRequest.ReportId = "NO2";
            string PageSize = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
            m_SubDimensionRequest.CountPerPage =  Convert.ToInt32(PageSize);
            SaveFormInfo();
            this.Hide();

            DateTime Time1 = DateTime.Now;
            await QueryData.BinderData<SubDimensionItem, SubDimensionRequest, SubDimensionSumItem>(m_SubDimensionRequest, true);
            DateTime Time2 = DateTime.Now;
            TimeSpan ts = Time2 - Time1;
            int difference = (int)ts.TotalMilliseconds;
            string logPath = @"C:\Users\17093191\Desktop\log.txt";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(logPath, true, Encoding.UTF8);
            sw.WriteLine(difference.ToString());
            sw.Close();

            this.Close();
        }


        private void rtbCompanys_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((System.Windows.Forms.TextBox)sender).SelectAll();
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 私有函数
        /// <summary>
        /// 检查参数
        /// </summary>
        /// <returns></returns>
        private bool CheckParasms()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbYear.Text))
            {
                ShowError(cbYear, "所选年份为空");
                return false;
            }

            if (string.IsNullOrEmpty(cbMonth.Text))
            {
                ShowError(cbMonth, "所选月份为空");
                return false;
            }

            if (string.IsNullOrEmpty(rtbCompanys.Text))
            {
                ShowError(rtbCompanys, "公司代码为空");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 展示错误消息
        /// </summary>
        private void ShowError(Control control, string sError)
        {
            errorProvider1.SetError(control, sError);
            MessageBox.Show(sError);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput)
        {
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格

            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 将中文逗号转换为英文逗号
        /// </summary>
        /// <returns></returns>
        private string ToDBC(string sInput,bool bRemove)
        {
            sInput = bRemove ? ("1," + sInput) : ("0," + sInput);
            sInput = sInput.Trim().Replace("\r", ""); //删除\r
            sInput = sInput.Trim().Replace("\n", "");//删除\n
            sInput = sInput.Trim().Replace("\t", "");//删除\t
            sInput = sInput.Trim().Replace(" ", "");//删除空格
            char[] c = sInput.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 保存查询信息
        /// </summary>
        private void SaveFormInfo()
        {
            Globals.Ribbons.FcmsRibbon.SubDimensionInfo = new SubDimensionInfo
            {
                Year = cbYear.Text.ToString(),
                Month = cbMonth.Text.ToString(),
                OrgCode = rtbCompanys.Text.ToString(),

                MainDataCode = string.IsNullOrEmpty(rtbMainDataCode.Text) ? string.Empty : rtbMainDataCode.Text.ToString(),
                CreditCode = string.IsNullOrEmpty(rtbCreditCode.Text) ? string.Empty : rtbCreditCode.Text.ToString(),
                Subjects = string.IsNullOrEmpty(rtbSubjects.Text) ? string.Empty : rtbSubjects.Text.ToString(),

                IsRemoveSubjects = cbSubjectCode.Checked,
                IsRemoveCreditCode = cbCreditCode.Checked,
                IsRemoveMainDataCode = cbMainDataCode.Checked,

                SubDimensionRequest = m_SubDimensionRequest
            };
        }

        /// <summary>
        /// 设置初始化信息
        /// </summary>
        private void SetFormInfo()
        {
            if (Globals.Ribbons.FcmsRibbon.SubDimensionInfo != null)
            {
                cbYear.Text = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.Year.ToString();
                cbMonth.Text = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.Month.ToString();
                rtbCompanys.Text = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.OrgCode.ToString();
                rtbMainDataCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.SubDimensionInfo.MainDataCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.SubDimensionInfo.MainDataCode.ToString();
                rtbCreditCode.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.SubDimensionInfo.CreditCode) ? string.Empty : Globals.Ribbons.FcmsRibbon.SubDimensionInfo.CreditCode.ToString();
                rtbSubjects.Text = string.IsNullOrEmpty(Globals.Ribbons.FcmsRibbon.SubDimensionInfo.Subjects) ? string.Empty : Globals.Ribbons.FcmsRibbon.SubDimensionInfo.Subjects.ToString();
                cbCreditCode.Checked = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.IsRemoveCreditCode;
                cbMainDataCode.Checked = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.IsRemoveMainDataCode;
                cbSubjectCode.Checked = Globals.Ribbons.FcmsRibbon.SubDimensionInfo.IsRemoveSubjects;
            }
        }
        #endregion
    }
}

