﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Sn.Fcms.Infrastructure;
using System.Diagnostics;

namespace Sn.FcmsWorkbook
{
    public partial class ThisWorkbook
    {
        private Dictionary<Excel.Worksheet, StatusInfo> m_SheetStatus;
        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            Globals.ThisWorkbook.Application.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized;
            ThisWorkbook_Open();
            NLogger.Info.Info("startup....");
            if (Globals.ThisWorkbook.Application.ActiveWindow != null)
                Globals.ThisWorkbook.Application.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized;

            m_SheetStatus = new Dictionary<Excel.Worksheet, StatusInfo>();
        }

        private void ThisWorkbook_Shutdown(object sender, System.EventArgs e)
        {
            GC.Collect();
            this.Dispose();
        }

        #region VSTO 设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisWorkbook_Startup);
            this.Shutdown += new System.EventHandler(ThisWorkbook_Shutdown);
            Globals.ThisWorkbook.Application.WorkbookActivate += Application_WorkbookActivate;
            Globals.ThisWorkbook.Application.SheetActivate += Application_SheetActivate;
            Globals.ThisWorkbook.Application.SheetDeactivate += Application_SheetDeactivate;
        }

        private void Application_SheetDeactivate(object Sh)
        {
            Excel.Worksheet worksheet = Sh as Excel.Worksheet;
            if (!ConstStr.GetAllSheetNames().Contains(worksheet.Name))
                return;

            if (!m_SheetStatus.ContainsKey(worksheet))
            {
                StatusInfo info = new StatusInfo();
                info.BtnLnFirst = Globals.Ribbons.FcmsRibbon.BtnFirst.Enabled;
                info.BtnLnkLast = Globals.Ribbons.FcmsRibbon.BtnLast.Enabled;
                info.BtnNextLink = Globals.Ribbons.FcmsRibbon.BtnNext.Enabled;
                info.BtnPreLink = Globals.Ribbons.FcmsRibbon.BtnPre.Enabled;
                info.GoTo = Globals.Ribbons.FcmsRibbon.ebGoTo.Text;
                info.PageSize = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
                string sPage = Globals.Ribbons.FcmsRibbon.lblPgeInfo.Label.ToString();
                info.PageIndex = sPage.Split('/')[0].ToString();
                info.PageCount = sPage.Split('/')[1].ToString();
                info.RecordCount = Globals.Ribbons.FcmsRibbon.lblRecordCount.Label.ToString();
                info.QueryData = Globals.Ribbons.FcmsRibbon.lblDate.Label.ToString();
                m_SheetStatus.Add(worksheet, info);
            }
            else
            {
                StatusInfo info = m_SheetStatus[worksheet];
                info.BtnLnFirst = Globals.Ribbons.FcmsRibbon.BtnFirst.Enabled;
                info.BtnLnkLast = Globals.Ribbons.FcmsRibbon.BtnLast.Enabled;
                info.BtnNextLink = Globals.Ribbons.FcmsRibbon.BtnNext.Enabled;
                info.GoTo = Globals.Ribbons.FcmsRibbon.ebGoTo.Text;
                info.PageSize = Globals.Ribbons.FcmsRibbon.cbPerCount.Text;
                info.RecordCount = Globals.Ribbons.FcmsRibbon.lblRecordCount.Label.ToString();
                info.QueryData = Globals.Ribbons.FcmsRibbon.lblDate.Label.ToString();
                string sPage = Globals.Ribbons.FcmsRibbon.lblPgeInfo.Label.ToString();
                info.PageIndex = sPage.Split('/')[0].ToString();
                info.PageCount = sPage.Split('/')[1].ToString();
            }
        }

        private void Application_SheetActivate(object Sh)
        {  
            Excel.Worksheet worksheet = Sh as Excel.Worksheet;
            if (!ConstStr.GetAllSheetNames().Contains(worksheet.Name))
                return;

            if (FcmsRibbon.CurrentForm != null && FcmsRibbon.CurrentForm.Tag.ToString() != worksheet.Name)
                FcmsRibbon.CurrentForm.Close();

            if (m_SheetStatus.ContainsKey(worksheet))
            {
                StatusInfo info = m_SheetStatus[worksheet];
                Globals.Ribbons.FcmsRibbon.BtnFirst.Enabled = info.BtnLnFirst;
                Globals.Ribbons.FcmsRibbon.BtnLast.Enabled = info.BtnLnkLast;
                Globals.Ribbons.FcmsRibbon.BtnNext.Enabled = info.BtnNextLink;
                Globals.Ribbons.FcmsRibbon.BtnPre.Enabled = info.BtnPreLink;
                Globals.Ribbons.FcmsRibbon.ebGoTo.Text = info.GoTo;
                Globals.Ribbons.FcmsRibbon.cbPerCount.Text = info.PageSize;
                Globals.Ribbons.FcmsRibbon.lblPgeInfo.Label = info.PageIndex + "/" + info.PageCount;
                Globals.Ribbons.FcmsRibbon.lblRecordCount.Label = info.RecordCount;
                Globals.Ribbons.FcmsRibbon.PageIndex = Convert.ToInt32(info.PageIndex);
                Globals.Ribbons.FcmsRibbon.PageCount = Convert.ToInt32(info.PageCount);
                Globals.Ribbons.FcmsRibbon.RecordCount = Convert.ToInt32(info.RecordCount);
                Globals.Ribbons.FcmsRibbon.PageSize = Convert.ToInt32(info.PageSize);
                Globals.Ribbons.FcmsRibbon.lblDate.Label = info.QueryData;
            }
            else
            {
                Globals.Ribbons.FcmsRibbon.BtnFirst.Enabled = false;
                Globals.Ribbons.FcmsRibbon.BtnLast.Enabled = false;
                Globals.Ribbons.FcmsRibbon.BtnNext.Enabled = false;
                Globals.Ribbons.FcmsRibbon.BtnPre.Enabled = false;
                Globals.Ribbons.FcmsRibbon.ebGoTo.Text = string.Empty;
                Globals.Ribbons.FcmsRibbon.cbPerCount.Text = 2000.ToString();
                Globals.Ribbons.FcmsRibbon.lblPgeInfo.Label = "0/0";
                Globals.Ribbons.FcmsRibbon.lblDate.Label = "未选择";
                Globals.Ribbons.FcmsRibbon.lblRecordCount.Label = 0.ToString(); ;
                Globals.Ribbons.FcmsRibbon.PageIndex = 1;
                Globals.Ribbons.FcmsRibbon.PageCount = 0;
                Globals.Ribbons.FcmsRibbon.RecordCount = 0;
                Globals.Ribbons.FcmsRibbon.PageSize = 2000;
            }
        }

        private void Application_WorkbookActivate(Excel.Workbook Wb)
        {
            ShowAddIn();
        }

        private void ThisWorkbook_Open()
        {
            if (ShowAddIn()) return;

            ProductType prod = ProductType.List;
            UserAddIn.Logon(ClientType.FcmsDocument, prod, Globals.ThisWorkbook.Path);
            if (!UserSession.Auth)
            {
                Globals.Ribbons.FcmsRibbon.tabFcms.Visible = false;
                Globals.Ribbons.FcmsRibbon.tabFinanceList.Visible = false;
            }

            Globals.Ribbons.FcmsRibbon.lblUser.Label = UserSession.Auth == true ? UserSession.UserName + UserSession.UserId : "未选择";
        }

        private bool ShowAddIn()
        {
            bool result = false;
            if (Globals.ThisWorkbook.Name.Equals("fcms.xlsx"))
            {
                Globals.Ribbons.FcmsRibbon.tabFcms.Visible = true;
                Globals.Ribbons.FcmsRibbon.tabFinanceList.Visible = false;
            }
            else if (Globals.ThisWorkbook.Name.Equals("FinList.xlsx"))
            {
                Globals.Ribbons.FcmsRibbon.tabFcms.Visible = false;
                Globals.Ribbons.FcmsRibbon.tabFinanceList.Visible = true;
            }
            else
            {
                Globals.Ribbons.FcmsRibbon.tabFcms.Visible = false;
                Globals.Ribbons.FcmsRibbon.tabFinanceList.Visible = false;
                result = true;
            }

            return result;
        }

        #endregion
    }
}
