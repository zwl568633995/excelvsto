﻿using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Service
{
    public class HKResult
    {
        public HKResult()
        {
            Data = new List<BillInfo>();
            XData = new List<BillInfo>();
            YData = new List<BillInfo>();
        }

        public bool Ok { get; set; }

        public string Message { get; set; }

        public List<BillInfo> Data { get; set; }

        public List<BillInfo> XData { get; set; }

        public List<BillInfo> YData { get; set; }
    }
}
