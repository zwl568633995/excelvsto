﻿using System.Collections.Generic;

namespace Sn.Fcms.Service
{
    public class FlowResult
    {
        public bool Result { get; set; }

        public string Message { get; set; }

        public string DataVersion { get; set; }

        public string LevelStatus { get; set; }

        public string ReportId { get; set; }     
    }

    public class DataResult<T> : FlowResult
    {
        public List<T> Data { get; set; }
    }

    public class FormResult:FlowResult
    {
        public Dictionary<string, string> FormData { get; set; }
    }

    public class DataFormResult<T>:DataResult<T>
    {
        public Dictionary<string, string> FormData { get; set; }
    }

    public class IncomeResult<H,T>: FlowResult
    {
        public List<H> FloatCols { get; set; }

        public List<T> FlowRows { get; set; }
    }  
}