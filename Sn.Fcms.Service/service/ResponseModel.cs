﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Service
{
    public class DataItem
    {
        /// <summary>
        /// 公司代码
        /// </summary>
        public string CompanyCode { get; set; }
    }
    /// <summary>
    /// No.1数据核对数据模型
    /// </summary>
    public class DataVerificationItem : DataItem
    {
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 科目
        /// </summary>
        public string SubjectCode { get; set; }

        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// SAP金额合计
        /// </summary>
        public string SapBalance { get; set; }

        /// <summary>
        /// 报表数据金额合计
        /// </summary>
        public string Balance { get; set; }

        /// <summary>
        /// 差异
        /// </summary>
        public string Difference { get; set; }
    }

    /// <summary>
    /// No.2应收预收余额监控表-科目维度
    /// </summary>
    public class SubDimensionItem : DataItem
    {
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string SubjectCode { get; set; }

        /// <summary>
        /// 预收账款余额(B)
        /// </summary>
        public string PrAmmount { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public string Balance { get; set; }

        /// <summary>
        /// 应收账款(C>0)
        /// </summary>
        public string ArAccount { get; set; }

        /// <summary>
        /// 预收账款(C<0)
        /// </summary>
        public string PrAccount { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string Month1 { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string Month2 { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string Month3 { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string Month4 { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string Month5 { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string Month6 { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string Month7 { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string Month8 { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string Month9 { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string Month10 { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string Month11 { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string Month12 { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string Month13 { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string Month14 { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string Month15 { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string Month16 { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string Month17 { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string Month18 { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string Month19 { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string Month20 { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string Month21 { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string Month22 { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string Month23 { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string Month24 { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string Age23 { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string Age34 { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string Age45 { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string Ageg5 { get; set; }

        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 合并字段
        /// </summary>
        public string MergeField { get; set; }

        /// <summary>
        /// 债权债务方名称
        /// </summary>
        public string MainDataName { get; set; }

        /// <summary>
        /// 科目属性
        /// </summary>
        public string SubjectProperty { get; set; }

        /// <summary>
        /// 应收账款余额(A)
        /// </summary>
        public string ArAmmout { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }
    }

    /// <summary>
    /// No.3应收预收余额监控表-债务债券
    /// </summary>
    public class DebtTableItem : DataItem
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 预收账款余额(B)
        /// </summary>
        public string PrAmmount { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public string Balance { get; set; }

        /// <summary>
        /// 应收账款(C>0)
        /// </summary>
        public string ArAccount { get; set; }

        /// <summary>
        /// 预收账款(C<0)
        /// </summary>
        public string PrAccount { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string Month1 { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string Month2 { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string Month3 { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string Month4 { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string Month5 { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string Month6 { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string Month7 { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string Month8 { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string Month9 { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string Month10 { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string Month11 { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string Month12 { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string Month13 { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string Month14 { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string Month15 { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string Month16 { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string Month17 { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string Month18 { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string Month19 { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string Month20 { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string Month21 { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string Month22 { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string Month23 { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string Month24 { get; set; }
        /// <summary>
        /// 2-3年
        /// </summary>
        public string Age23 { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string Age34 { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string Age45 { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string Ageg5 { get; set; }

        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 合并字段
        /// </summary>
        public string MergeField { get; set; }

        /// <summary>
        /// 债权债务方名称
        /// </summary>
        public string MainDataName { get; set; }

        /// <summary>
        /// 应收账款余额(A)
        /// </summary>
        public string ArAmmout { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }
    }

    /// <summary>
    /// No.4应收预收账款明细报表
    /// </summary>
    public class AccountTableItem : DataItem
    {
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }
        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }
        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 债权债务方编码代码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 债权债务方编码名称
        /// </summary>
        public string MainDataName { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string SubjectCode { get; set; }
        private string _AccountDate;
        /// <summary>
        /// 记账日期
        /// </summary>
        public string AccountDate
        {
            get { return _AccountDate; }
            set
            {
                DateTime date = DateTime.Parse(value.ToString().Substring(0, 4) + "/" + value.ToString().Substring(4, 2) + "/" + value.ToString().Substring(6, 2));
                _AccountDate = date.ToString("yyyy/MM/dd");
            }
        }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertiDate { get; set; }
        /// <summary>
        /// 凭证编码
        /// </summary>
        public string CertiCode { get; set; }
        /// <summary>
        /// 凭证类型
        /// </summary>
        public string CertiType { get; set; }
        /// <summary>
        ///   项（行项目编号）
        /// </summary>
        public string LineCode { get; set; }
        /// <summary>
        /// 采购凭证
        /// </summary>
        public string Purchase { get; set; }
        /// <summary>
        /// 参照
        /// </summary>
        public string ReferTo { get; set; }
        /// <summary>
        /// 分配
        /// </summary>
        public string Distribute { get; set; }
        /// <summary>
        /// 文本
        /// </summary>
        public string Text { get; set; }
        private string _ClearAccountDate;
        /// <summary>
        /// 清帐日期
        /// </summary>
        public string ClearAccountDate
        {
            get { return _ClearAccountDate; }
            set
            {
                _ClearAccountDate = value.ToString() == "00000000" ? "" : value;
            }
        }
        /// <summary>
        /// 清帐凭证
        /// </summary>
        public string ClearCertificate { get; set; }
        /// <summary>
        /// 净值到期日
        /// </summary>
        public string ProupDate { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public string Amount { get; set; }
        /// <summary>
        /// 合并字段
        /// </summary>
        public string MergeField { get; set; }
        /// <summary>
        /// 供应商总金额
        /// </summary>
        public string SupAccount { get; set; }
        /// <summary>
        /// 账面账龄
        /// </summary>
        public string AccountAge
        {
          get
            {
                TimeSpan ts = DateTime.Now.Date - Convert.ToDateTime(_AccountDate);
                if ((ts.Days+1)<=30)
                {
                    return "1个月";
                }
                else if ((ts.Days + 1) <= 60)
                {
                    return "2个月";
                }
                else if ((ts.Days + 1) <= 90)
                {
                    return "3个月";
                }
                else if ((ts.Days + 1) <= 180)
                {
                    return "3-6个月";
                }
                else if ((ts.Days + 1) <= 365)
                {
                    return "6-12个月";
                }
                else if ((ts.Days + 1) <= 730)
                {
                    return "1-2年";
                }
                else if ((ts.Days + 1) <= 1095)
                {
                    return "2-3年";
                }
                else if ((ts.Days + 1) <= 1460)
                {
                    return "3-4年";
                }
                else if ((ts.Days + 1) <= 1825)
                {
                    return "4-5年";
                }

                return "5年以上";
            }
        }
    }

    /// <summary>
    /// No.5应付预付账款-科目明细表
    /// </summary>
    public class PayDataVerSubjectItem : DataItem
    {
        public string DistrictCode { get; set; }
        public string SubjectCode { get; set; }
        public string Balance { get; set; }
        public string CompanyName { get; set; }
        public string SapBalance { get; set; }
        public string SubjectName { get; set; }
        public string DistrictName { get; set; }
        public string MergeField { get; set; }
    }

    /// <summary>
    /// No.9应付预付账款-债权债务明细表
    /// </summary>
    public class PayDataVerMainDataItem : DataItem
    {
        public string DistrictCode { get; set; }
        public string MainDataCode { get; set; }
        public string Balance { get; set; }

        public string CompanyName { get; set; }
        public string MainDataBalance { get; set; }

        public string DistrictName { get; set; }
        public string MergeField { get; set; }
        public string MainDataName { get; set; }
    }

    /// <summary>
    /// No.6应付预付余额监控表-科目维度
    /// </summary>
    public class PaySubDimensionItem : DataItem
    {
        public string MergeField { get; set; }
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string SubjectCode { get; set; }

        /// <summary>
        /// 预付账款余额(B)
        /// </summary>
        public string PrAmmount { get; set; }

        /// <summary>
        /// 应付暂估余额（D）
        /// </summary>
        public string TemArmount { get; set; }

        /// <summary>
        /// 应付暂估-期初导入（C）
        /// </summary>
        public string PortArmount { get; set; }
        
        /// <summary>
        /// 债权债务品类
        /// </summary>
        public string MainDataType { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public string Balance { get; set; }

        /// <summary>
        /// 应收账款(C>0)
        /// </summary>
        public string ArAccount { get; set; }

        /// <summary>
        /// 预收账款(C<0)
        /// </summary>
        public string PrAccount { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string Month1 { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string Month2 { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string Month3 { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string Month4 { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string Month5 { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string Month6 { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string Month7 { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string Month8 { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string Month9 { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string Month10 { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string Month11 { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string Month12 { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string Month13 { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string Month14 { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string Month15 { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string Month16 { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string Month17 { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string Month18 { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string Month19 { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string Month20 { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string Month21 { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string Month22 { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string Month23 { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string Month24 { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string Age23 { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string Age34 { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string Age45 { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string Ageg5 { get; set; }

        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 债权债务方名称
        /// </summary>
        public string MainDataName { get; set; }

        /// <summary>
        /// 科目属性
        /// </summary>
        public string SubjectProperty { get; set; }

        /// <summary>
        /// 应付账款余额(A)
        /// </summary>
        public string ArAmmout { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }
    }

    /// <summary>
    /// No.7应收预收余额监控表-债务债券
    /// </summary>
    public class PayDebtTableItem : DataItem
    {
        public string DistrictCode { get; set; }
        public string MainDataCode { get; set; }
        public string Age23 { get; set; }
        public string Age34 { get; set; }
        public string Age45 { get; set; }
        public string Ageg5 { get; set; }
        public string PrAmmount { get; set; }
        public string Balance { get; set; }
        public string ArAccount { get; set; }
        public string PrAccount { get; set; }
        public string CompanyName { get; set; }
        public string MainDataType { get; set; }
        public string DistrictName { get; set; }
        public string MergeField { get; set; }
        public string CreditCode { get; set; }
        public string MainDataName { get; set; }
        public string ArAmmout { get; set; }
        public string TemArmount { get; set; }
        public string PortArmount { get; set; }
        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        public string Month4 { get; set; }
        public string Month5 { get; set; }
        public string Month6 { get; set; }
        public string Month7 { get; set; }
        public string Month8 { get; set; }
        public string Month9 { get; set; }
        public string Month10 { get; set; }
        public string Month11 { get; set; }
        public string Month12 { get; set; }
        public string Month13 { get; set; }
        public string Month14 { get; set; }
        public string Month15 { get; set; }
        public string Month16 { get; set; }
        public string Month17 { get; set; }
        public string Month18 { get; set; }
        public string Month19 { get; set; }
        public string Month20 { get; set; }
        public string Month21 { get; set; }
        public string Month22 { get; set; }
        public string Month23 { get; set; }
        public string Month24 { get; set; }
    }

    /// <summary>
    /// No.8应付预付账款明细报表
    /// </summary>
    public class PayAccountTableItem : DataItem
    {
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }
        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }
        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 债权债务方编码代码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 债权债务方编码名称
        /// </summary>
        public string MainDataName { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string SubjectCode { get; set; }
        private string _AccountDate;
        /// <summary>
        /// 记账日期
        /// </summary>
        public string AccountDate
        {
            get { return _AccountDate; }
            set
            {
                DateTime date = DateTime.Parse(value.ToString().Substring(0, 4) + "/" + value.ToString().Substring(4, 2) + "/" + value.ToString().Substring(6, 2));
                _AccountDate = date.ToString("yyyy/MM/dd");
            }
        }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertiDate { get; set; }
        /// <summary>
        /// 凭证编码
        /// </summary>
        public string CertiCode { get; set; }
        /// <summary>
        /// 凭证类型
        /// </summary>
        public string CertiType { get; set; }
        /// <summary>
        ///   项（行项目编号）
        /// </summary>
        public string LineCode { get; set; }
        /// <summary>
        /// 采购凭证
        /// </summary>
        public string Purchase { get; set; }
        /// <summary>
        /// 参照
        /// </summary>
        public string ReferTo { get; set; }
        /// <summary>
        /// 分配
        /// </summary>
        public string Distribute { get; set; }
        /// <summary>
        /// 文本
        /// </summary>
        public string Text { get; set; }
        private string _ClearAccountDate;
        /// <summary>
        /// 清帐日期
        /// </summary>
        public string ClearAccountDate
        {
            get { return _ClearAccountDate; }
            set
            {
                _ClearAccountDate = value.ToString() == "00000000" ? "" : value;
            }
        }
        /// <summary>
        /// 清帐凭证
        /// </summary>
        public string ClearCertificate { get; set; }
        /// <summary>
        /// 净值到期日
        /// </summary>
        public string ProupDate { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public string Amount { get; set; }
        /// <summary>
        /// 合并字段
        /// </summary>
        public string MergeField { get; set; }
        /// <summary>
        /// 供应商总金额
        /// </summary>
        public string SupAccount { get; set; }
        /// <summary>
        /// 账面账龄
        /// </summary>
        public string AccountAge
        {
            get
            {
                TimeSpan ts = DateTime.Now.Date - Convert.ToDateTime(_AccountDate);
                if ((ts.Days + 1) <= 30)
                {
                    return "1个月";
                }
                else if ((ts.Days + 1) <= 60)
                {
                    return "2个月";
                }
                else if ((ts.Days + 1) <= 90)
                {
                    return "3个月";
                }
                else if ((ts.Days + 1) <= 180)
                {
                    return "3-6个月";
                }
                else if ((ts.Days + 1) <= 365)
                {
                    return "6-12个月";
                }
                else if ((ts.Days + 1) <= 730)
                {
                    return "1-2年";
                }
                else if ((ts.Days + 1) <= 1095)
                {
                    return "2-3年";
                }
                else if ((ts.Days + 1) <= 1460)
                {
                    return "3-4年";
                }
                else if ((ts.Days + 1) <= 1825)
                {
                    return "4-5年";
                }

                return "5年以上";
            }
        }

        public string MainDataType { get; set; }
    }

    /// <summary>
    /// No.6应付预付余额监控表-科目维度
    /// </summary>
    public class OtherSubDimensionItem : DataItem
    {
        public string OtherPay { get; set; }

        public string OtherRec { get; set; }

        public string Balance { get; set; }

        public string PreOtherPay { get; set; }

        public string PreOtherPayTem { get; set; }

        public string PreOtherRec { get; set; }

        public string MergeField { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 大区代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string SubjectCode { get; set; }

        /// <summary>
        /// 大区名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 债权债务方名称
        /// </summary>
        public string MainDataName { get; set; }

        /// <summary>
        /// 科目属性
        /// </summary>
        public string SubjectProperty { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string CreditCode { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string Month1 { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string Month2 { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string Month3 { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string Month4 { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string Month5 { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string Month6 { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string Month7 { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string Month8 { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string Month9 { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string Month10 { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string Month11 { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string Month12 { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string Month13 { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string Month14 { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string Month15 { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string Month16 { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string Month17 { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string Month18 { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string Month19 { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string Month20 { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string Month21 { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string Month22 { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string Month23 { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string Month24 { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string Age23 { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string Age34 { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string Age45 { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string Ageg5 { get; set; }

        /// <summary>
        /// 债权债务品类
        /// </summary>
        public string MainDataType { get; set; }
    }

    /// <summary>
    /// No.7应收预收余额监控表-债务债券
    /// </summary>
    public class OtherDebtTableItem : DataItem
    {
        public string OtherPay { get; set; }
        public string OtherRec { get; set; }
        public string Balance { get; set; }
        public string PreOtherPay { get; set; }
        public string PreOtherPayTem { get; set; }
        public string PreOtherRec { get; set; }
        public string DistrictCode { get; set; }
        public string MainDataCode { get; set; }
        public string CompanyName { get; set; }
        public string DistrictName { get; set; }
        public string MergeField { get; set; }
        public string CreditCode { get; set; }
        public string MainDataName { get; set; }

        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        public string Month4 { get; set; }
        public string Month5 { get; set; }
        public string Month6 { get; set; }
        public string Month7 { get; set; }
        public string Month8 { get; set; }
        public string Month9 { get; set; }
        public string Month10 { get; set; }
        public string Month11 { get; set; }
        public string Month12 { get; set; }
        public string Month13 { get; set; }
        public string Month14 { get; set; }
        public string Month15 { get; set; }
        public string Month16 { get; set; }
        public string Month17 { get; set; }
        public string Month18 { get; set; }
        public string Month19 { get; set; }
        public string Month20 { get; set; }
        public string Month21 { get; set; }
        public string Month22 { get; set; }
        public string Month23 { get; set; }
        public string Month24 { get; set; }
        public string Age23 { get; set; }
        public string Age34 { get; set; }
        public string Age45 { get; set; }
        public string Ageg5 { get; set; }
        public string MainDataType { get; set; }
    }

    #region 合计值
    public class DataVerificationSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string SAPBALANCESUM { get; set; }
        public string BALANCESUM { get; set; }
    }

    public class SubDimensionSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        /// <summary>
        /// 预收账款余额(B)合计
        /// </summary>
        public string PRAMMOUNTSUM { get; set; }

        /// <summary>
        /// 余额合计
        /// </summary>
        public string BALANCESUM { get; set; }

        /// <summary>
        /// 应收账款合计(C>0)
        /// </summary>
        public string ARACCOUNTSUM { get; set; }

        /// <summary>
        /// 预收账款合计(C<0)
        /// </summary>
        public string PRACCOUNTSUM { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string MONTH1SUM { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string MONTH2SUM { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string MONTH3SUM { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string MONTH4SUM { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string MONTH5SUM { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string MONTH6SUM { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string MONTH7SUM { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string MONTH8SUM { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string MONTH9SUM { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string MONTH10SUM { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string MONTH11SUM { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string MONTH12SUM { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string MONTH13SUM { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string MONTH14SUM { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string MONTH15SUM { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string MONTH16SUM { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string MONTH17SUM { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string MONTH18SUM { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string MONTH19SUM { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string MONTH20SUM { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string MONTH21SUM { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string MONTH22SUM { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string MONTH23SUM { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string MONTH24SUM { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string AGE23SUM { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string AGE34SUM { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string AGE45SUM { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string AGEG5SUM { get; set; }

        /// <summary>
        /// 应收账款余额(A)
        /// </summary>
        public string ARAMMOUTSUM { get; set; }
    }

    public class DebtTableSumItem
    {
        public object GetValue(string propertyName)
        { 
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        /// <summary>
        /// 预收账款余额(B)合计
        /// </summary>
        public string PRAMMOUNTSUM { get; set; }

        /// <summary>
        /// 余额合计
        /// </summary>
        public string BALANCESUM { get; set; }

        /// <summary>
        /// 应收账款合计(C>0)
        /// </summary>
        public string ARACCOUNTSUM { get; set; }

        /// <summary>
        /// 预收账款合计(C<0)
        /// </summary>
        public string PRACCOUNTSUM { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string MONTH1SUM { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string MONTH2SUM { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string MONTH3SUM { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string MONTH4SUM { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string MONTH5SUM { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string MONTH6SUM { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string MONTH7SUM { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string MONTH8SUM { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string MONTH9SUM { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string MONTH10SUM { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string MONTH11SUM { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string MONTH12SUM { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string MONTH13SUM { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string MONTH14SUM { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string MONTH15SUM { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string MONTH16SUM { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string MONTH17SUM { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string MONTH18SUM { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string MONTH19SUM { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string MONTH20SUM { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string MONTH21SUM { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string MONTH22SUM { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string MONTH23SUM { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string MONTH24SUM { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string AGE23SUM { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string AGE34SUM { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string AGE45SUM { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string AGEG5SUM { get; set; }

        /// <summary>
        /// 应收账款余额(A)
        /// </summary>
        public string ARAMMOUTSUM { get; set; }

    }

    public class AccountTableSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        /// <summary>
        /// 金额合计
        /// </summary>
        public string AMOUNTSUM { get; set; }

        /// <summary>
        /// 供应商总金额合计
        /// </summary>
        public string SUPACCOUNTSUM { get; set; }
    }

    public class PayDataVerSujectSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string SAPBALANCESUM { get; set; }
        public string BALANCESUM { get; set; }
    }

    public class PayDataVerMainDataSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string BALANCESUM { get; set; }

        public string MAINDATABALANCESUM { get; set; }
    }

    public class PaySubDimensionSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string TEMARMOUNTSUM { get; set; }

        public string PORTARMOUNTSUM { get; set; }
        /// <summary>
        /// 预收账款余额(B)合计
        /// </summary>
        public string PRAMMOUNTSUM { get; set; }

        /// <summary>
        /// 余额合计
        /// </summary>
        public string BALANCESUM { get; set; }

        /// <summary>
        /// 应付账款合计(C>0)
        /// </summary>
        public string ARACCOUNTSUM { get; set; }

        /// <summary>
        /// 预收账款合计(C<0)
        /// </summary>
        public string PRACCOUNTSUM { get; set; }

        /// <summary>
        /// 应付账款余额(A)
        /// </summary>
        public string ARAMMOUTSUM { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string MONTH1SUM { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string MONTH2SUM { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string MONTH3SUM { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string MONTH4SUM { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string MONTH5SUM { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string MONTH6SUM { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string MONTH7SUM { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string MONTH8SUM { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string MONTH9SUM { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string MONTH10SUM { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string MONTH11SUM { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string MONTH12SUM { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string MONTH13SUM { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string MONTH14SUM { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string MONTH15SUM { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string MONTH16SUM { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string MONTH17SUM { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string MONTH18SUM { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string MONTH19SUM { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string MONTH20SUM { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string MONTH21SUM { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string MONTH22SUM { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string MONTH23SUM { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string MONTH24SUM { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string AGE23SUM { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string AGE34SUM { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string AGE45SUM { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string AGEG5SUM { get; set; }
    }

    public class PayDebtTableSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string PRAMMOUNTSUM { get; set; }
        public string TEMARMOUNTSUM { get; set; }
        public string ARAMMOUTSUM { get; set; }
        public string PORTARMOUNTSUM { get; set; }
        public string BALANCESUM { get; set; }
        public string PRACCOUNTSUM { get; set; }
        public string ARACCOUNTSUM { get; set; }
        public string MONTH1SUM { get; set; }
        public string MONTH2SUM { get; set; }
        public string MONTH3SUM { get; set; }
        public string MONTH4SUM { get; set; }
        public string MONTH5SUM { get; set; }
        public string MONTH6SUM { get; set; }
        public string MONTH7SUM { get; set; }
        public string MONTH8SUM { get; set; }
        public string MONTH9SUM { get; set; }
        public string MONTH10SUM { get; set; }
        public string MONTH11SUM { get; set; }
        public string MONTH12SUM { get; set; }
        public string MONTH13SUM { get; set; }
        public string MONTH14SUM { get; set; }
        public string MONTH15SUM { get; set; }
        public string MONTH16SUM { get; set; }
        public string MONTH17SUM { get; set; }
        public string MONTH18SUM { get; set; }
        public string MONTH19SUM { get; set; }
        public string MONTH20SUM { get; set; }
        public string MONTH21SUM { get; set; }
        public string MONTH22SUM { get; set; }
        public string MONTH23SUM { get; set; }
        public string MONTH24SUM { get; set; }
        public string AGE23SUM { get; set; }
        public string AGE34SUM { get; set; }
        public string AGE45SUM { get; set; }
        public string AGEG5SUM { get; set; }
    }

    public class PayAccountTableSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        /// <summary>
        /// 金额合计
        /// </summary>
        public string AMOUNTSUM { get; set; }

        /// <summary>
        /// 供应商总金额合计
        /// </summary>
        public string SUPACCOUNTSUM { get; set; }
    }

    public class OtherSubDimensionSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string PREOTHERRECSUM { get; set; }
        public string PREOTHERPAYTEMSUM { get; set; }
        public string PREOTHERPAYSUM { get; set; }
        public string BALANCESUM { get; set; }
        public string OTHERRECSUM { get; set; }
        public string OTHERPAYSUM { get; set; }

        /// <summary>
        /// 1个月
        /// </summary>
        public string MONTH1SUM { get; set; }

        /// <summary>
        /// 2个月
        /// </summary>
        public string MONTH2SUM { get; set; }

        /// <summary>
        /// 3个月
        /// </summary>
        public string MONTH3SUM { get; set; }

        /// <summary>
        /// 4个月
        /// </summary>
        public string MONTH4SUM { get; set; }

        /// <summary>
        /// 5个月
        /// </summary>
        public string MONTH5SUM { get; set; }

        /// <summary>
        /// 6个月
        /// </summary>
        public string MONTH6SUM { get; set; }

        /// <summary>
        /// 7个月
        /// </summary>
        public string MONTH7SUM { get; set; }

        /// <summary>
        /// 8个月
        /// </summary>
        public string MONTH8SUM { get; set; }

        /// <summary>
        /// 9个月
        /// </summary>
        public string MONTH9SUM { get; set; }

        /// <summary>
        /// 10个月
        /// </summary>
        public string MONTH10SUM { get; set; }

        /// <summary>
        /// 11个月
        /// </summary>
        public string MONTH11SUM { get; set; }

        /// <summary>
        /// 12个月
        /// </summary>
        public string MONTH12SUM { get; set; }

        /// <summary>
        /// 13个月
        /// </summary>
        public string MONTH13SUM { get; set; }

        /// <summary>
        /// 14个月
        /// </summary>
        public string MONTH14SUM { get; set; }

        /// <summary>
        /// 15个月
        /// </summary>
        public string MONTH15SUM { get; set; }

        /// <summary>
        /// 16个月
        /// </summary>
        public string MONTH16SUM { get; set; }

        /// <summary>
        /// 17个月
        /// </summary>
        public string MONTH17SUM { get; set; }

        /// <summary>
        /// 18个月
        /// </summary>
        public string MONTH18SUM { get; set; }

        /// <summary>
        /// 19个月
        /// </summary>
        public string MONTH19SUM { get; set; }

        /// <summary>
        /// 20个月
        /// </summary>
        public string MONTH20SUM { get; set; }

        /// <summary>
        /// 21个月
        /// </summary>
        public string MONTH21SUM { get; set; }

        /// <summary>
        /// 22个月
        /// </summary>
        public string MONTH22SUM { get; set; }

        /// <summary>
        /// 23个月
        /// </summary>
        public string MONTH23SUM { get; set; }

        /// <summary>
        /// 24个月
        /// </summary>
        public string MONTH24SUM { get; set; }

        /// <summary>
        /// 2-3年
        /// </summary>
        public string AGE23SUM { get; set; }

        /// <summary>
        /// 3-4年
        /// </summary>
        public string AGE34SUM { get; set; }

        /// <summary>
        /// 4-5年
        /// </summary>
        public string AGE45SUM { get; set; }

        /// <summary>
        /// 5年以上
        /// </summary>
        public string AGEG5SUM { get; set; }
    }

    public class OtherDebtTableSumItem
    {
        public object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName.ToUpper()).GetValue(this, null);
        }

        public string PREOTHERRECSUM { get; set; }
        public string PREOTHERPAYTEMSUM { get; set; }
        public string PREOTHERPAYSUM { get; set; }
        public string BALANCESUM { get; set; }
        public string OTHERRECSUM { get; set; }
        public string OTHERPAYSUM { get; set; }
        public string MONTH1SUM { get; set; }
        public string MONTH2SUM { get; set; }
        public string MONTH3SUM { get; set; }
        public string MONTH4SUM { get; set; }
        public string MONTH5SUM { get; set; }
        public string MONTH6SUM { get; set; }
        public string MONTH7SUM { get; set; }
        public string MONTH8SUM { get; set; }
        public string MONTH9SUM { get; set; }
        public string MONTH10SUM { get; set; }
        public string MONTH11SUM { get; set; }
        public string MONTH12SUM { get; set; }
        public string MONTH13SUM { get; set; }
        public string MONTH14SUM { get; set; }
        public string MONTH15SUM { get; set; }
        public string MONTH16SUM { get; set; }
        public string MONTH17SUM { get; set; }
        public string MONTH18SUM { get; set; }
        public string MONTH19SUM { get; set; }
        public string MONTH20SUM { get; set; }
        public string MONTH21SUM { get; set; }
        public string MONTH22SUM { get; set; }
        public string MONTH23SUM { get; set; }
        public string MONTH24SUM { get; set; }
        public string AGE23SUM { get; set; }
        public string AGE34SUM { get; set; }
        public string AGE45SUM { get; set; }
        public string AGEG5SUM { get; set; }
    }
    #endregion

    /// <summary>
    /// 数据模型
    /// </summary>
    public class ResponseModel<T, S>
    {
        /// <summary>
        /// 数据集合
        /// </summary>
        public T[] FloatCols { get; set; }

        public S TotalMap { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string MsgDesc { get; set; }

        /// <summary>
        /// 消息代码
        /// </summary>
        public string MsgCode { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        public string TotalCount { get; set; }
    }
}
