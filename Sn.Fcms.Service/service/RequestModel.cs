﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Service
{
    #region 请求
    /// <summary>
    /// No.1数据核对请求模型
    /// </summary>
    public class DataVerificationRequest
    {
        /// <summary>
        /// 表序号
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// TokenId
        /// </summary>
        public string TokenId { get; set; }

        /// <summary>
        /// 年月
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// 公司代码
        /// </summary>
        public string[] OrgCode { get; set; }

        /// <summary>
        /// 开始的Index
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CountPerPage { get; set; }
    }

    /// <summary>
    /// No.2应收预收余额监控表-科目维度
    /// </summary>
    public class SubDimensionRequest : DataVerificationRequest
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string[] Subjiect { get; set; }

        /// <summary>
        /// 债券债务方编码
        /// </summary>
        public string[] MainData { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string[] Credit { get; set; }
    }
    /// <summary>
    /// No.3应收预收余额监控表-债务债权维度
    /// </summary>
    public class DebtTableRequest : DataVerificationRequest
    {
        /// <summary>
        /// 债券债务方编码
        /// </summary>
        public string[] MainData { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string[] Credit { get; set; }
    }
    /// <summary>
    /// No.4应收预收账款明细报表
    /// </summary>
    public class AccountTableRequest : DataVerificationRequest
    {

        /// <summary>
        /// 科目编码
        /// </summary>
        public string[] Subjiect { get; set; }
        /// <summary>
        /// 债券债务方编码
        /// </summary>
        public string[] MainData { get; set; }

        /// <summary>
        /// 社会信用证代码/税务登记证
        /// </summary>
        public string[] Credit { get; set; }
        /// <summary>
        /// 记账日期
        /// </summary>
        public string AccountDate { get; set; }
    }
    /// <summary>
    /// No.5应付预付数据核对请求模型
    /// </summary>
    public class PayDataVerificationRequest:DataVerificationRequest
    {
       
    }
    /// <summary>
    /// No.6应付预付余额监控表-科目维度
    /// </summary>
    public class PaySubDimensionRequest : SubDimensionRequest
    {
       public string[] MainDataType { get; set; }
    }
    /// <summary>
    /// No.7应收预收余额监控表-债务债权维度
    /// </summary>
    public class PayDebtTableRequest : DebtTableRequest
    {
        public string[] MainDataType { get; set; }
    }
    /// <summary>
    /// No.8应收预收账款明细报表
    /// </summary>
    public class PayAccountTableRequest : AccountTableRequest
    {
        public string[] MainDataType { get; set; }
    }
    #endregion

    #region 记录窗体的信息
    public class BaseInfo
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string OrgCode { get; set; }
    }

    public class DataVerificationInfo : BaseInfo
    {
        public DataVerificationRequest DataVerificationRequest { get; set; }
    }

    public class PayDataVerSubjectInfo : BaseInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public PayDataVerificationRequest PayDataVerificationRequest { get; set; }
    }

    public class OtherDataVerSubjectInfo : BaseInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public PayDataVerificationRequest PayDataVerificationRequest { get; set; }
    }

    public class PayDataVerMainDataInfo : BaseInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public PayDataVerificationRequest PayDataVerificationRequest { get; set; }
    }

    public class OtherDataVerMainDataInfo : BaseInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public PayDataVerificationRequest PayDataVerificationRequest { get; set; }
    }

    public class SubDimensionInfo : BaseInfo
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string Subjects { get; set; }
        /// <summary>
        /// 是否排除科目编码
        /// </summary>
        public bool IsRemoveSubjects { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }

        /// <summary>
        /// 是否排除债权债务方编码
        /// </summary>
        public bool IsRemoveMainDataCode { get; set; }

        /// <summary>
        /// 社会信用编码
        /// </summary>
        public string CreditCode { get; set; }
        /// <summary>
        /// 是否排除社会信用编码
        /// </summary>
        public bool IsRemoveCreditCode { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public SubDimensionRequest SubDimensionRequest { get; set; }
    }

    public class PaySubDimensionInfo : SubDimensionInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PaySubDimensionRequest PaySubDimensionRequest { get; set; }
    }

    public class OtherSubDimensionInfo : SubDimensionInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PaySubDimensionRequest PaySubDimensionRequest { get; set; }
    }

    public class DebtInfo : BaseInfo
    {
        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }

        /// <summary>
        /// 是否排除债权债务方编码
        /// </summary>
        public bool IsRemoveMainDataCode { get; set; }

        /// <summary>
        /// 社会信用编码
        /// </summary>
        public string CreditCode { get; set; }
        /// <summary>
        /// 是否排除社会信用编码
        /// </summary>
        public bool IsRemoveCreditCode { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public DebtTableRequest DebtTableRequest { get; set; }
    }

    public class PayDebtInfo : DebtInfo
    {

        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PayDebtTableRequest PayDebtTableRequest { get; set; }
    }

    public class OtherDebtInfo : DebtInfo
    {

        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PayDebtTableRequest PayDebtTableRequest { get; set; }
    }

    public class AccountInfo : BaseInfo
    {
        /// <summary>
        /// 记账日期From
        /// </summary>
        public string AccountDateFrom { get; set; }
        /// <summary>
        /// 记账日期To
        /// </summary>
        public string AccountDateTo { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public string Subjects { get; set; }
        /// <summary>
        /// 是否排除科目编码
        /// </summary>
        public bool IsRemoveSubjects { get; set; }

        /// <summary>
        /// 债权债务方编码
        /// </summary>
        public string MainDataCode { get; set; }

        /// <summary>
        /// 是否排除债权债务方编码
        /// </summary>
        public bool IsRemoveMainDataCode { get; set; }

        /// <summary>
        /// 社会信用编码
        /// </summary>
        public string CreditCode { get; set; }
        /// <summary>
        /// 是否排除社会信用编码
        /// </summary>
        public bool IsRemoveCreditCode { get; set; }
        /// <summary>
        /// 请求信息
        /// </summary>
        public AccountTableRequest AccountTableRequest { get; set; }
    }

    public class PayAccountInfo : AccountInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PayAccountTableRequest PayAccountTableRequest { get; set; }
    }

    public class OtherAccountInfo : AccountInfo
    {
        public bool IsRemoveCompanys { get; set; }
        public string MainDataType { get; set; }
        public bool IsRemoveMainDataType { get; set; }

        /// <summary>
        /// 请求信息
        /// </summary>
        public PayAccountTableRequest PayAccountTableRequest { get; set; }
    }
    #endregion
}
