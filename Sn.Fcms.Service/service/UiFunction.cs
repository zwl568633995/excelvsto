﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sn.Fcms.Service
{
    #region 对查询表的内容进行缓存
    public class UIFunction<T> where T:new()
    {
        private static T m_Frm;
        static UIFunction()
        {
        }
        public static T GetInstance()
        {
            if (m_Frm==null)
            {
                m_Frm = new T();
            }

            return m_Frm;
        }
    }
    #endregion
}
