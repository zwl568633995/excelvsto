﻿using Sn.Fcms.Core;
using Sn.Fcms.Infrastructure;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Service
{
    public class UserServiceFlow
    {
        public static async Task<FlowResult> Logon(string userId, string password,string clientVersion, ClientType clientType, ProductType prd)
        {
            NLogger.Info.Info("user: {0} try to log on with clientVerType: {1}, clientVersion: {2}"
                , userId, clientType, clientVersion);

            FlowResult result = new FlowResult();

            try
            {
                UserService userService = new UserService();

                var authenResult = await userService.AuthUser(new AuthUserRequest()
                {
                    ClientId = clientType,
                    ClientVersion = clientVersion,
                    UserId = userId,
                    Password = password
                });

                if (!authenResult.Success())
                {
                    NLogger.Info.Info("user: {0} log on with clientVerType: {1}, " +
                                       "clientVersion: {2} failed!, reson:{3}"
                        , userId, clientType, clientVersion, "Authenization Failed");

                    result.Message = authenResult.MsgDesc;
                    return result;
                }

                UserSession.UserName = authenResult.UserName;
                UserSession.ClientId = clientType;
                UserSession.TokenId = authenResult.TokenId;
                UserSession.UserId = userId;
                UserSession.ProcedureAuth = authenResult.ProcedureAuth;

                var organsResult = await userService.QueryOrganizations(new BaseRequest()
                {
                    UserId = userId,
                    TokenId = authenResult.TokenId
                });

                var reportsResult = await userService.QueryReports(new AuthReportRequest()
                {
                    UserId = userId,
                    TokenId = authenResult.TokenId,
                    ProductType = prd,
                    ParentId = ""
                });

                if (!reportsResult.Success())
                {
                    NLogger.Info.Info("user: {0} log on with clientVerType: {1}, " +
                                   "clientVersion: {2} failed!, reson:{3}"
                    , userId, clientType, clientVersion, "No Reports Authorized");

                    result.Message = "没有获取到清单权限，请确认是否有清单权限";
                    return result;
                }

                UserSession.Reports = reportsResult.Data?.ToList();
              
                if (!organsResult.Success())
                {
                    NLogger.Info.Info("user: {0} log on with clientVerType: {1}, " +
                                       "clientVersion: {2} failed!, reson:{3}"
                        , userId, clientType, clientVersion, "No Oragnizations Authorized");

                    result.Message = "没有获取到组织权限，请确认";
                    return result;
                }

                result.Result = true;
                UserSession.Organizations = organsResult.Data?.ToList();
            }
            catch (Exception exception)
            {
                result.Message = "登录流程出错，请联系开发人员！";
                NLogger.Error.Error("logon flow exception:{0}", exception.StackTrace);
            }

            NLogger.Info.Info("user: {0} log on with clientVerType: {1}, clientVersion: {2} successed!", userId, clientType, clientVersion);

            return result;
        }


        public static async Task<FlowResult> Logon(string userId, string password, string clientVersion, ClientType clientType)
        {
            NLogger.Info.Info("user: {0} try to log on with clientVerType: {1}, clientVersion: {2}"
                , userId, clientType, clientVersion);

            FlowResult result = new FlowResult();

            try
            {
                UserService userService = new UserService();

                var authenResult = await userService.AuthHkUser(new AuthUserRequest()
                {
                    ClientId = clientType,
                    ClientVersion = clientVersion,
                    UserId = userId,
                    Password = password
                });

                if (!authenResult.Success())
                {
                    NLogger.Info.Info("user: {0} log on with clientVerType: {1},clientVersion: {2} failed!, Authenization Failed"
                        , userId, clientType, clientVersion);

                    result.Message = authenResult.MsgDesc;
                    return result;
                }

                UserSession.UserName = authenResult.UserName;
                UserSession.ClientId = clientType;
                UserSession.TokenId = authenResult.TokenId;
                UserSession.UserId = userId;
                UserSession.ProcedureAuth = authenResult.ProcedureAuth;                

                result.Result = true;
            }
            catch (Exception exception)
            {
                result.Message = "登录流程出错，请联系开发人员！";
                NLogger.Error.Error("logon flow exception:{0}", exception.StackTrace);
            }

            NLogger.Info.Info("user: {0} log on with clientVerType: {1}, clientVersion: {2} successed!", userId, clientType, clientVersion);

            return result;
        }
    }
}
