﻿using Sn.Fcms.Core;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Sn.Fcms.Infrastructure;

namespace Sn.Fcms.Service
{
    public class HKServiceFlow
    {
        private static IHKDataService service;

        public static async Task<HKResult> Query(QueryParam req)
        {
            if (service == null)
                service = new HKDataService();

            Dictionary<string, BillInfo> xkeys = new Dictionary<string, BillInfo>();
            Dictionary<string, BillInfo> ykeys = new Dictionary<string, BillInfo>();

            HKResult hk = new HKResult();
            int index = 1;

            NLogger.Info.Info("------------begin query ----------------");

            while (true)
            {
                req.StartIndex = index;
                var response = await service.Query<BillInfo>(req);
                hk.Data.AddRange(response.Data);
                GetData(response.Data?.ToList(), xkeys, ykeys);

                if (hk.Data.Count >= response.Amount)
                    break;

                index += req.CountPerPage;
            }

            hk.XData = xkeys.Values.ToList();
            hk.YData = ykeys.Values.ToList();

            NLogger.Info.Info("------------end query ----------------");

            return hk;
        }

        private static bool GetData(List<BillInfo> data,Dictionary<string,BillInfo> xkeys,Dictionary<string,BillInfo> ykeys)
        {
            if (data == null || data.Count == 0)
                return true;

            foreach (var bill in data)
            {
                string date = bill.BillType == -1 ? bill.RefundTime : bill.EffectiveTime;
                string xkey = string.Format("{0}_{1}_{2}_{3}", bill.PayOrg, bill.PayStore, bill.PayCode, date);
                if (!xkeys.ContainsKey(xkey))
                    xkeys.Add(xkey, bill);

                string ykey = string.Format("{0}_{1}_{2}", bill.PayOrg, bill.PayCode, date);
                if (!ykeys.ContainsKey(ykey))
                    ykeys.Add(ykey, bill);
            }

            return true;
        }       
    }
}
