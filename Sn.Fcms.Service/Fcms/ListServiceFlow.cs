﻿using Sn.Fcms.Core;
using Sn.Fcms.Infrastructure;
using Sn.Fcms.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sn.Fcms.Service
{
    public class ListServiceFlow
    {
        public static async Task<DataResult<T>> GetListData<T>(ReqParam req)
        {
            DataResult<T> result = new DataResult<T>();

            try
            {
                string dataVersion = string.Empty;
                var listDataService = ContainerConfigurer.CurrentManager.Resolve<IListDataService>();
                List<T> listData = new List<T>();

                int index = 1;
                int bk = 0;
                int pageCount = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageCount"]);

                QueryListDataRequest param = new QueryListDataRequest()
                {
                    UserId = req.UserId,
                    TokenId = req.TokenId,
                    DataVersion = req.DataVersion,
                    ReportId = req.ReportId,
                    OrgCode = req.OrgCode,
                    Month = req.Month,
                    CountPerPage = pageCount
                };

                while (true)
                {
                    param.StartIndex = index;
                    var response = await listDataService.Query<T>(param);

                    if (!response.Success())
                    {
                        NLogger.Info.Info("user: {0} try to query workSheet: {1} data error,tokenId: {2}, organCode: {3}, month: {4}", req.UserId, req.TokenId, req.ReportId, req.OrgCode, req.Month);

                        result.Message = response.MsgDesc;
                        result.Data = listData;
                        return result;
                    }

                    listData.AddRange(response.Data);
                    index += pageCount;

                    if (string.IsNullOrEmpty(dataVersion))
                        dataVersion = response.DataVersion;

                    if (response.Data.Count() < pageCount)
                        break;

                    bk++;

                    // 防止死循环
                    if (bk >= 100)
                    {
                        NLogger.Error.Error("response.Data.Count:{0},index:{1},break:{2},请求超过100次，可能存在死循环", response.Data.Count(), bk);
                        break;
                    }
                }

                result.Data = listData;
                result.Result = true;
                result.Message = string.Format("sheetId:{0}查询成功", req.ReportId);
                result.ReportId = req.ReportId;
                result.DataVersion = dataVersion;
            }
            catch (Exception exception)
            {
                result.Data = new List<T>();
                result.Message = string.Format("获取清单数据异常 [{0}]", exception.Message);
                NLogger.Error.Error("GetListData Data flow error sheetId:{0},orgcode:{1},token:{2},message:{3} \r\n {4}", req.ReportId, req.OrgCode, req.TokenId, exception.Message, exception.StackTrace);
            }

            return result;
        }
    }
}
